#pragma once

#include "arity_node/arity_incl.hpp"
#include "declarations/decl_fwd.hpp"
#include "expressions/expr_fwd.hpp"
#include "lexer/Name.hpp"
#include "statements/stmt_fwd.hpp"
#include "types/type_fwd.hpp"

class Fun_type;


namespace Builder {
	// Declareations
	Declaration* make_Variable_decl(Name const& name, Type const* type);
	Declaration* make_Function_decl(Name const& name, Fun_type const* type);
	Declaration* make_Program_decl(Decl_seq& decls);

	// Expressions
	Expression* make_Identifier_expr(Declaration const* id);
	Expression* make_Bool_expr(bool const b = false);
	Expression* make_Char_expr(char const c = 0);
	Expression* make_Int_expr(int const i = 0);
	Expression* make_UInt_expr(unsigned int const u = 0);
	Expression* make_Float_expr(float const f = 0.0);
	Expression* make_Double_expr(double const d = 0.0);
	Expression* make_Convert_Value_expr(Expression* operand);
	Expression* make_pre_Increment_expr(Expression* operand);
	Expression* make_post_Increment_expr(Expression* operand);
	Expression* make_pre_Decrement_expr(Expression* operand);
	Expression* make_post_Decrement_expr(Expression* operand);
	Expression* make_Minus_expr(Expression* operand);
	Expression* make_B_NOT_expr(Expression* operand);
	Expression* make_Reciprocal_expr(Expression* operand);
	Expression* make_L_NOT_expr(Expression* operand);
	Expression* make_Assignment_expr(Expression* lhs, Expression* rhs);
	Expression* make_Addition_Assignment_expr(Expression* lhs, Expression* rhs);
	Expression* make_Subtraction_Assignment_expr(Expression* lhs, Expression* rhs);
	Expression* make_Multiplication_Assignment_expr(Expression* lhs, Expression* rhs);
	Expression* make_Quotient_Assignment_expr(Expression* lhs, Expression* rhs);
	Expression* make_Modulo_Assignment_expr(Expression* lhs, Expression* rhs);
	Expression* make_B_AND_Assignment_expr(Expression* lhs, Expression* rhs);
	Expression* make_B_OR_Assignment_expr(Expression* lhs, Expression* rhs);
	Expression* make_B_XOR_Assignment_expr(Expression* lhs, Expression* rhs);
	Expression* make_B_LSHIFT_Assignment_expr(Expression* lhs, Expression* rhs);
	Expression* make_B_RSHIFT_Assignment_expr(Expression* lhs, Expression* rhs);
	Expression* make_B_AND_expr(Expression* lhs, Expression* rhs);
	Expression* make_B_OR_expr(Expression* lhs, Expression* rhs);
	Expression* make_B_XOR_expr(Expression* lhs, Expression* rhs);
	Expression* make_B_LSHIFT_expr(Expression* lhs, Expression* rhs);
	Expression* make_B_RSHIFT_expr(Expression* lhs, Expression* rhs);
	Expression* make_Addition_expr(Expression* lhs, Expression* rhs);
	Expression* make_Subtraction_expr(Expression* lhs, Expression* rhs);
	Expression* make_Multiplication_expr(Expression* lhs, Expression* rhs);
	Expression* make_Quotient_expr(Expression* lhs, Expression* rhs);
	Expression* make_Modulo_expr(Expression* lhs, Expression* rhs);
	Expression* make_L_AND_expr(Expression* lhs, Expression* rhs);
	Expression* make_L_OR_expr(Expression* lhs, Expression* rhs);
	Expression* make_EQUAL_expr(Expression* lhs, Expression* rhs);
	Expression* make_NEQUAL_expr(Expression* lhs, Expression* rhs);
	Expression* make_LT_expr(Expression* lhs, Expression* rhs);
	Expression* make_GT_expr(Expression* lhs, Expression* rhs);
	Expression* make_LTOE_expr(Expression* lhs, Expression* rhs);
	Expression* make_GTOE_expr(Expression* lhs, Expression* rhs);
	Expression* make_Conditional_expr(Expression* condition, Expression* true_expr, Expression* false_expr);
	Expression* make_Call_expr(Expr_seq& exprs);

	// Statements
	Statement* get_Skip_stmt();
	Statement* get_Break_stmt();
	Statement* get_Continue_stmt();
	Statement* make_Block_stmt(Stmt_seq& stmts);
	Statement* make_When_stmt(Expression* condition, Statement* stmt);
	Statement* make_While_stmt(Expression* condition, Statement* stmt);
	Statement* make_If_stmt(Expression* condition, Statement* true_stmt, Statement* false_stmt);
	Statement* make_Return_stmt(Expression* expr);
	Statement* make_Expression_stmt(Expression* expr);
	Statement* make_Declaration_stmt(Declaration* decl);

	// Types
	Type const* get_Bool_type();
	Type const* get_Char_type();
	Type const* get_Int_type();
	Type const* get_UInt_type();
	Type const* get_Float_type();
	Type const* get_Double_type();
	Type const* get_Ref_type(Type const* type);
	Fun_type const* get_Fun_type(Type_seq const operands);

	// Check Helpers
	Expression* require_bool(Expression* expr);
	Expression* require_arithmetic(Expression* expr);
	Expression* require_function(Expression* expr);
	Expression* require_type(Expression* expr, Type const* type);
	Expression* require_value_of(Expression* expr, Type const* type);
	Expression* require_reference_to(Expression* expr, Type const* type);
	std::pair<Expression*, Expression*> require_same(Expression* e1, Expression* e2);
	std::pair<Expression*, Expression*> require_same_value(Expression* e1, Expression* e2);
	std::pair<Expression*, Expression*> require_same_arithmetic(Expression* e1, Expression* e2);
	std::pair<Expression*, Expression*> require_common(Expression* e1, Expression* e2);

	// Convert Helpers
	Expression* convert_to_value(Expression* expr);
	Expression* convert_to_reference(Expression* expr);

	// Init Helpers
	void copy_initialize(Declaration* decl, Expression* expr);
	void reference_initialize(Declaration* decl, Expression* expr);
};	// namespace Builder
