#pragma once

#include "declarations/decl_fwd.hpp"
#include "lexer/Symbol.hpp"

#include <unordered_map>
#include <vector>


struct Scope : public std::unordered_map<Symbol, Declaration*> {
  public:
	Declaration* lookup(Symbol const sym);
	Declaration const* lookup(Symbol const sym) const;
	Declaration* declare(Symbol const sym, Declaration* decl);
};


class Scope_stack : private std::vector<Scope> {
  public:
	Declaration* lookup(Symbol const sym);
	Declaration const* lookup(Symbol const sym) const;
	Declaration* declare(Symbol constsym, Declaration* decl);

	void push();
	void pop();
	Scope& top();
	Scope const& top() const;
};
