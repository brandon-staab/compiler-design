#pragma once

#include "declarations/decl_fwd.hpp"
#include "expressions/expr_fwd.hpp"
#include "statements/stmt_fwd.hpp"
#include "types/type_fwd.hpp"

#include "lexer/Token.hpp"
#include "semantics/Scope.hpp"


class Function_decl;


class SemanticActions {
  public:
	void enter_scope();
	void leave_scope();
	Declaration* declare(Symbol sym, Declaration* decl);
	Declaration* declare(Token token, Declaration* decl);
	Declaration* lookup(Symbol sym);
	Declaration* lookup(Token token);
	Declaration* must_exist(Token token);
	void must_not_exist(Token token) const;
	Declaration* declare_unique(Token token, Declaration* decl);

	// Declaration
	Declaration* on_program(Decl_seq& decls);
	Declaration* on_object_declaration(Token id, Type const* type, Token var_tok, Token colon);
	Declaration* on_reference_definintion(Token id, Type const* type, Token let, Token colon, Token ref, Token equal, Token semicolon);
	Declaration* on_function_declaration(Token id, Decl_seq& parms, Type const* type);
	Declaration* on_parameter(Token id, Type const* type, Token colon);
	Declaration* finish_object_definition(Declaration* var, Expression* init, Token semicolon);
	void start_function_definition(Declaration* fn);
	void finish_function_definition(Declaration* fn, Statement* body);

	// Expression
	Expression* on_assignment_expression(Expression* lhs, Expression* rhs, Token const& op);
	Expression* on_conditional_expression(Expression* e1, Expression* e2, Expression* e3, Token const& tok1, Token const& tok2);
	Expression* on_logical_expression(Expression* lhs, Expression* rhs, Token const& op);
	Expression* on_bitwise_expression(Expression* lhs, Expression* rhs, Token const& op);
	Expression* on_equality_expression(Expression* lhs, Expression* rhs, Token const& op);
	Expression* on_relational_expression(Expression* lhs, Expression* rhs, Token const& op);
	Expression* on_shift_expression(Expression* lhs, Expression* rhs, Token const& op);
	Expression* on_additive_expression(Expression* lhs, Expression* rhs, Token const& op);
	Expression* on_multiplicative_expression(Expression* lhs, Expression* rhs, Token const& op);
	Expression* on_access_expression(Expression* expr, Token const& op);
	Expression* on_prefix_expression(Expression* expr, Token const& op);
	Expression* on_postfix_expression(Expression* expr, Token const& op);
	Expression* on_call_expression(Expression* id, Expr_seq args, Token const& lparen, Token const& rparen);
	Expression* on_boolean_literal(Token const& bool_lit);
	Expression* on_integer_literal(Token const& int_lit);
	Expression* on_float_literal(Token const& float_lit);
	Expression* on_paren_expression(Expression* expr, Token const& lparen, Token const& rparen);
	Expression* on_identifier_expression(Token const& id_tok);

	// Statement
	Statement* on_empty_statement(Token semicolon);
	Statement* on_block_statement(Stmt_seq& stmts, Token lbrace, Token rbrace);
	Statement* on_if_statement(Expression* condition, Statement* true_stmt, Statement* false_stmt, Token if_kw, Token lparen, Token rparen, Token else_kw);
	Statement* on_when_statement(Expression* condition, Statement* true_stmt, Token if_kw, Token lparen, Token rparen);
	Statement* on_while_statement(Expression* condition, Statement* body, Token keyword, Token lparen, Token rparen);
	Statement* on_break_statement(Token keyword, Token semicolon);
	Statement* on_continue_statement(Token keyword, Token semicolon);
	Statement* on_return_statement(Expression* ret, Token keyword, Token semicolon);
	Statement* on_declaration_statement(Declaration* decl);
	Statement* on_expression_statement(Expression* expr, Token semicolon);

	// Type
	Type const* get_bool_type(Token tok);
	Type const* get_char_type(Token tok);
	Type const* get_int_type(Token tok);
	Type const* get_unsigned_type(Token tok);
	Type const* get_float_type(Token tok);
	Type const* get_double_type(Token tok);
	Type const* on_ref_type(Type const* type, Token ref);
	Type const* on_function_type(Type const* type);
	Expression* on_type_get_zero(Type const* type);

  private:
	Function_decl* m_func;
	Scope_stack m_stack;
};
