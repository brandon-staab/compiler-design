#pragma once

#include <iosfwd>


void insertIndent(std::ostream& os, std::size_t const amount);
