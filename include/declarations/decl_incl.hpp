#pragma once

#include "declarations/Declaration.hpp"

// clang-format off
#include "declarations/Nullary_decl.hpp"
#include "declarations/Multiary_decl.hpp"
