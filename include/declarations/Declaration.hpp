#pragma once

#include "arity_node/arity_incl.hpp"

#include "lexer/Name.hpp"
#include "statements/stmt_incl.hpp"
#include "types/type_incl.hpp"


class Declaration {
  public:
	enum class Kind : char {
		// Nullary
		variable_decl,

		// Multiary
		function_decl,
		program_decl
	};

	Kind get_kind() const;
	virtual Name const& get_name() const = 0;
	virtual Type const* const get_type() const = 0;

	bool is_object() const;
	bool is_variable() const;
	bool is_reference() const;
	bool is_function() const;

	virtual ~Declaration() = default;

  protected:
	Declaration(Kind const kind);

  private:
	Kind m_kind;
};

/*****************************/

template <std::size_t N>
class Fixed_Arity_decl : public Declaration, public Static_arity_node<Declaration, N> {
  protected:
	// clang-format off
	template <typename ...Args>	 // clang-format on
	Fixed_Arity_decl(Kind const kind, Args&&... args) : Declaration(kind), Static_arity_node<Declaration, N>(args...) {}
};

/*************************************************************/

class Value_decl {
  protected:
	Value_decl(Name const& name, Type const* const type);

	Name const& get_name() const;
	Type const* const get_type() const;

  private:
	Name m_name;
	Type const* m_type;
};

/*************************************************************/

std::ostream& operator<<(std::ostream& os, Declaration const& decl);
std::ostream& operator<<(std::ostream& os, Declaration::Kind const kind);

/*****************************/

char const* const kind_to_string(Declaration::Kind const kind);
void dump_debug(std::ostream& os, Declaration const* const decl, std::size_t depth = 0);
void print(std::ostream& os, Declaration const* const decl, std::size_t depth = 0);
void to_sexpr(std::ostream& os, Declaration const* const decl);
