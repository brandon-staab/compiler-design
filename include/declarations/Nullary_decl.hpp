#pragma once

#include "declarations/Declaration.hpp"


class Nullary_decl : public Fixed_Arity_decl<0> {
  protected:
	Nullary_decl(Kind const kind);
};

/*************************************************************/

class Variable_decl : public Nullary_decl, public Value_decl {
  public:
	Variable_decl(Name const& name, Type const* const type);

	Name const& get_name() const;
	Type const* const get_type() const;

	bool has_initializer() const;
	void set_initializer(Expression* const expr);
	Expression* const get_initializer();
	Expression const* const get_initializer() const;

  private:
	Expression* m_init;
};
