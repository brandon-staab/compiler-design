#pragma once

#include "declarations/Declaration.hpp"


class Multiary_decl : public Declaration, public Dynamic_arity_node<Declaration> {
  protected:
	Multiary_decl(Kind const kind, Decl_seq operands);
};

/*************************************************************/

class Function_decl : public Multiary_decl, public Value_decl {
  public:
	Function_decl(Name const& name, Fun_type const* const type);

	Name const& get_name() const final;
	Type const* const get_type() const final;
	Fun_type const* const get_fun_type() const;

	void set_parameters(Decl_seq& parms);
	void add_parameter(Declaration* const decl);
	void set_body(Statement const* const stmt);
	Statement const* const get_stmt() const;

	std::size_t get_num_parameters() const;
	Type const* const get_parm_type(std::size_t const i);
	Type const* const get_ret_type() const;

  private:
	Statement const* m_stmt;
};


class Program_decl : public Multiary_decl {
  public:
	Program_decl(std::vector<Declaration*>& decls);

	Name const& get_name() const final;
	Type const* const get_type() const final;
};
