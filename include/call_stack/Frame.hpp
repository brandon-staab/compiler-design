#pragma once

#include "store/Monotonic_store.hpp"


class Frame {
  public:
	Frame(Frame* const prev, Declaration const* const func);

	Frame* const get_caller() const;
	Declaration const* get_function() const;
	std::size_t get_index() const;

	Monotonic_store& get_locals();
	Object* const allocate_local(Declaration const* const decl);
	Object* const locate_local(Declaration const* const decl);
	void alias_local(Declaration const* const decl, Object* const obj);

  private:
	Frame* const m_prev;
	Declaration const* const m_func;
	std::size_t const m_index;
	Monotonic_store m_locals;
};
