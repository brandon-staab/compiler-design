#pragma once

#include "call_stack/Frame.hpp"


class Call_stack {
  public:
	Call_stack();

	std::size_t empty() const;
	std::size_t size() const;
	Frame* const push(Declaration const* const func);
	void pop();
	Frame* const get_top() const;
	Frame* const get_frame(std::size_t n) const;

  private:
	Frame* m_top;
	std::vector<Frame*> m_frames;
};
