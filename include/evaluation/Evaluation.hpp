#pragma once

#include "call_stack/Call_stack.hpp"
#include "expressions/expr_fwd.hpp"
#include "statements/stmt_fwd.hpp"


enum class ControlFlow {
	next_fc,
	break_fc,
	continue_fc,
	return_fc,
};


class Evaluation {
  public:
	Evaluation() = default;
	Evaluation(Evaluation const& other) = delete;
	Evaluation(Evaluation&& other) = delete;

	Monotonic_store& get_globals();
	Object* allocate_static(Declaration const* decl);
	Object* allocate_automatic(Declaration const* decl);
	Object* locate_object(Value const& val);
	Call_stack& get_stack();
	Declaration const* get_current_function() const;
	Frame* get_current_frame() const;
	Frame* push_frame(Declaration const* func);
	void pop_frame();

  private:
	Monotonic_store m_globals;
	Call_stack m_stack;
};

/*************************************************************/

void evaluate_decl(Evaluation& eval, Declaration const* decl);
Value evaluate_expr(Evaluation& eval, Expression const* expr);
ControlFlow evaluate_stmt(Evaluation& eval, Statement const* stmt);
