#pragma once

#include "components/Value.hpp"
#include "types/type_incl.hpp"


class Object {
  public:
	Object(Type const* const type);
	Object(Type const* const type, Value const& val);
	Object(Type const* const type, Value&& val);

	Type const* get_type() const;
	bool is_initialized() const;
	bool is_uninitialized() const;
	Value const& load() const;
	void initialize(Value const& val);
	void initialize(Value&& val);
	void store(Value const& val);
	void store(Value&& val);

  private:
	Type const* const m_type;
	Value m_value;
};
