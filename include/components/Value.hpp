#pragma once

#include <iosfwd>


class Variable_decl;


struct Address {
	int store;
	Variable_decl const* def;
};


class Value {
  public:
	enum class Kind : char {
		non_val,
		bool_val,
		char_val,
		int_val,
		uint_val,
		float_val,
		double_val,
		address_val,
	};

	Value();
	explicit Value(bool const b);
	explicit Value(char const c);
	explicit Value(int const i);
	explicit Value(unsigned int const u);
	explicit Value(float const f);
	explicit Value(double const d);
	explicit Value(Address const& a);

	Kind get_kind() const;

	bool is_indeterminate() const;
	bool is_bool() const;
	bool is_char() const;
	bool is_int() const;
	bool is_uint() const;
	bool is_float() const;
	bool is_double() const;
	bool is_address() const;

	void set_bool(bool const b);
	void set_char(char const c);
	void set_int(int const i);
	void set_uint(unsigned int const u);
	void set_float(float const f);
	void set_double(double const d);
	void set_address(Address const& a);

	bool get_bool() const;
	char get_char() const;
	int get_int() const;
	unsigned int get_uint() const;
	float get_float() const;
	double get_double() const;
	Address get_address() const;


  private:
	Kind m_kind;
	union Data {
		Data(bool const b);
		Data(char const c);
		Data(int const i);
		Data(unsigned int const u);
		Data(float const f);
		Data(double const d);
		Data(Address const& a);

		bool b;
		char c;
		int i;
		unsigned int u;
		float f;
		double d;
		Address a;
	} m_data;
};

/*************************************************************/

std::ostream& operator<<(std::ostream& os, Value const& val);
