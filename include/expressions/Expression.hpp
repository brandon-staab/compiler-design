#pragma once

#include "expressions/expr_fwd.hpp"

#include "arity_node/arity_incl.hpp"
#include "types/type_fwd.hpp"

#include <iosfwd>


class Expression {
  public:
	enum class Kind : char {
		// Nullary
		identifier_expr,
		bool_expr,
		char_expr,
		int_expr,
		uint_expr,
		float_expr,
		double_expr,

		// Unary
		convert_value_expr,
		pre_increment_expr,
		post_increment_expr,
		pre_decrement_expr,
		post_decrement_expr,
		minus_expr,
		b_not_expr,
		reciprocal_expr,
		l_not_expr,

		// Binary
		assignment_expr,
		addition_assignment_expr,
		subtraction_assignment_expr,
		multiplication_assignment_expr,
		quotient_assignment_expr,
		modulo_assignment_expr,
		b_and_assignment_expr,
		b_or_assignment_expr,
		b_xor_assignment_expr,
		b_lshift_assignment_expr,
		b_rshift_assignment_expr,
		b_and_expr,
		b_or_expr,
		b_xor_expr,
		b_lshift_expr,
		b_rshift_expr,
		addition_expr,
		subtraction_expr,
		multiplication_expr,
		quotient_expr,
		modulo_expr,
		l_and_expr,
		l_or_expr,
		equal_expr,
		nequal_expr,
		lt_expr,
		gt_expr,
		ltoe_expr,
		gtoe_expr,

		// Ternary
		conditional_expr,

		// Multiary
		call_expr
	};

	virtual ~Expression() = default;

	Kind get_kind() const;
	Type const* const get_type() const;

  protected:
	Expression(Kind const kind, Type const* const type);

  private:
	Kind m_kind;
	Type const* m_type;
};

/*****************************/

template <std::size_t N>
class Fixed_Arity_expr : public Expression, public Static_arity_node<Expression, N> {
  protected:
	// clang-format off
	template <typename ...Args>	 // clang-format on
	Fixed_Arity_expr(Kind const kind, Type const* type, Args&&... args) : Expression(kind, type), Static_arity_node<Expression, N>(args...) {}
};

/*************************************************************/

std::ostream& operator<<(std::ostream& os, Expression const& expr);
std::ostream& operator<<(std::ostream& os, Expression::Kind const kind);

/*****************************/

char const* const kind_to_string(Expression::Kind const kind);
char const* const kind_to_symbol(Expression::Kind const kind);
void dump_debug(std::ostream& os, Expression const* const expr, std::size_t depth = 0);
void print(std::ostream& os, Expression const* const expr, std::size_t depth = 0);
void to_sexpr(std::ostream& os, Expression const* const expr);
