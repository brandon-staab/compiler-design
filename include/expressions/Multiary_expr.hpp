#pragma once

#include "expressions/Expression.hpp"


class Multiary_expr : public Expression, public Dynamic_arity_node<Expression> {
  protected:
	Multiary_expr(Kind const kind, Type const* const type, Expr_seq&& operands);
};

/*************************************************************/

class Identifier_expr;

class Call_expr : public Multiary_expr {
  public:
	Call_expr(Type const* const type, Expr_seq&& operands);

	Identifier_expr const& get_calle() const;
	Expression const& get_parameter(std::size_t const i) const;
};
