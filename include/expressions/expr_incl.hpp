#pragma once

#include "expressions/Expression.hpp"

// clang-format off
#include "expressions/Nullary_expr.hpp"
#include "expressions/Unary_expr.hpp"
#include "expressions/Binary_expr.hpp"
#include "expressions/Ternary_expr.hpp"
#include "expressions/Multiary_expr.hpp"
