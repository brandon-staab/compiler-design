#pragma once

#include <vector>

class Expression;

using Expr_seq = std::vector<Expression*>;
