#pragma once

#include "expressions/Expression.hpp"


class Ternary_expr : public Fixed_Arity_expr<3> {
  protected:
	Ternary_expr(Kind const kind, Type const* const type, Expression* const first, Expression* const second, Expression* const third);
};

/*************************************************************/

class Conditional_expr : public Ternary_expr {
  public:
	Conditional_expr(Type const* const type, Expression* const condition, Expression* const true_expr, Expression* const false_expr);

	Expression const* const get_condition() const;
	Expression const* const get_true_expr() const;
	Expression const* const get_false_expr() const;
};
