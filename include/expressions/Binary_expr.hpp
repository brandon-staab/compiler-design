#pragma once

#include "expressions/Expression.hpp"


class Binary_expr : public Fixed_Arity_expr<2> {
  protected:
	Binary_expr(Kind const kind, Type const* const type, Expression* const lhs, Expression* const rhs);
};


class Assignment_binaryexpr : public Binary_expr {
  protected:
	Assignment_binaryexpr(Kind const kind, Type const* const type, Expression* const lhs, Expression* const rhs);
};


class Arithmetic_binaryexpr : public Binary_expr {
  protected:
	Arithmetic_binaryexpr(Kind const kind, Type const* const type, Expression* const lhs, Expression* const rhs);
};


class Logical_binaryexpr : public Binary_expr {
  protected:
	Logical_binaryexpr(Kind const kind, Type const* const type, Expression* const lhs, Expression* const rhs);
};


class Comparison_binaryexpr : public Binary_expr {
  protected:
	Comparison_binaryexpr(Kind const kind, Type const* const type, Expression* const lhs, Expression* const rhs);
};

/*************************************************************/

class Assignment_expr : public Assignment_binaryexpr {
  public:
	Assignment_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};


class Addition_Assignment_expr : public Assignment_binaryexpr {
  public:
	Addition_Assignment_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};


class Subtraction_Assignment_expr : public Assignment_binaryexpr {
  public:
	Subtraction_Assignment_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};


class Multiplication_Assignment_expr : public Assignment_binaryexpr {
  public:
	Multiplication_Assignment_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};


class Quotient_Assignment_expr : public Assignment_binaryexpr {
  public:
	Quotient_Assignment_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};


class Modulo_Assignment_expr : public Assignment_binaryexpr {
  public:
	Modulo_Assignment_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};


class B_AND_Assignment_expr : public Assignment_binaryexpr {
  public:
	B_AND_Assignment_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};


class B_OR_Assignment_expr : public Assignment_binaryexpr {
  public:
	B_OR_Assignment_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};


class B_XOR_Assignment_expr : public Assignment_binaryexpr {
  public:
	B_XOR_Assignment_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};


class B_LSHIFT_Assignment_expr : public Assignment_binaryexpr {
  public:
	B_LSHIFT_Assignment_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};


class B_RSHIFT_Assignment_expr : public Assignment_binaryexpr {
  public:
	B_RSHIFT_Assignment_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};

/*****************************/

class B_AND_expr : public Arithmetic_binaryexpr {
  public:
	B_AND_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};


class B_OR_expr : public Arithmetic_binaryexpr {
  public:
	B_OR_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};


class B_XOR_expr : public Arithmetic_binaryexpr {
  public:
	B_XOR_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};


class B_LSHIFT_expr : public Arithmetic_binaryexpr {
  public:
	B_LSHIFT_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};


class B_RSHIFT_expr : public Arithmetic_binaryexpr {
  public:
	B_RSHIFT_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};


class Addition_expr : public Arithmetic_binaryexpr {
  public:
	Addition_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};


class Subtraction_expr : public Arithmetic_binaryexpr {
  public:
	Subtraction_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};


class Multiplication_expr : public Arithmetic_binaryexpr {
  public:
	Multiplication_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};


class Quotient_expr : public Arithmetic_binaryexpr {
  public:
	Quotient_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};


class Modulo_expr : public Arithmetic_binaryexpr {
  public:
	Modulo_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};

/*****************************/

class L_AND_expr : public Logical_binaryexpr {
  public:
	L_AND_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};


class L_OR_expr : public Logical_binaryexpr {
  public:
	L_OR_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};

/*****************************/

class EQUAL_expr : public Comparison_binaryexpr {
  public:
	EQUAL_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};


class NEQUAL_expr : public Comparison_binaryexpr {
  public:
	NEQUAL_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};


class LT_expr : public Comparison_binaryexpr {
  public:
	LT_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};


class GT_expr : public Comparison_binaryexpr {
  public:
	GT_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};


class LTOE_expr : public Comparison_binaryexpr {
  public:
	LTOE_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};


class GTOE_expr : public Comparison_binaryexpr {
  public:
	GTOE_expr(Type const* const type, Expression* const lhs, Expression* const rhs);
};
