#pragma once

#include "expressions/Expression.hpp"

#include "components/Value.hpp"
#include "declarations/decl_incl.hpp"


class Nullary_expr : public Fixed_Arity_expr<0> {
  public:
	virtual std::string to_string() const = 0;

  protected:
	Nullary_expr(Kind const kind, Type const* const type);
};


class Literal_expr : public Nullary_expr {
  protected:
	Literal_expr(Kind const kind, Type const* const type, Value&& value);

	Value m_value;
};

/*************************************************************/

class Declaration;

class Identifier_expr : public Nullary_expr {
  public:
	Identifier_expr(Type const* const type, Declaration const* const decl);

	void set_decl(Declaration const* const decl);
	std::string to_string() const final;

  private:
	Declaration const* m_decl;
};

/*****************************/

class Bool_expr : public Literal_expr {
  public:
	Bool_expr(Type const* const type, bool const val);

	std::string to_string() const final;
};


class Char_expr : public Literal_expr {
  public:
	Char_expr(Type const* const type, char const val);

	std::string to_string() const final;
};


class Int_expr : public Literal_expr {
  public:
	Int_expr(Type const* const type, int const val);

	std::string to_string() const final;
};


class UInt_expr : public Literal_expr {
  public:
	UInt_expr(Type const* const type, unsigned int const val);

	std::string to_string() const final;
};


class Float_expr : public Literal_expr {
  public:
	Float_expr(Type const* const type, float const val);

	std::string to_string() const final;
};


class Double_expr : public Literal_expr {
  public:
	Double_expr(Type const* const type, double const val);

	std::string to_string() const final;
};
