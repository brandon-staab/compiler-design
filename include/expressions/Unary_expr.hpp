#pragma once

#include "expressions/Expression.hpp"


class Unary_expr : public Fixed_Arity_expr<1> {
  protected:
	Unary_expr(Kind const kind, Type const* const type, Expression* const operand);
};

class Incdec_unaryexpr : public Unary_expr {
  protected:
	Incdec_unaryexpr(Kind const kind, Type const* const type, Expression* const operand);
};


class Arithmetic_unaryexpr : public Unary_expr {
  protected:
	Arithmetic_unaryexpr(Kind const kind, Type const* const type, Expression* const operand);
};


class Logical_unaryexpr : public Unary_expr {
  protected:
	Logical_unaryexpr(Kind const kind, Type const* const type, Expression* const operand);
};

/*************************************************************/

class Convert_Value_expr : public Unary_expr {
  public:
	Convert_Value_expr(Type const* const type, Expression* const operand);

	Expression* const get_source();
	Expression const* const get_source() const;
};

/*****************************/

class pre_Increment_expr : public Incdec_unaryexpr {
  public:
	pre_Increment_expr(Type const* const type, Expression* const operand);
};


class post_Increment_expr : public Incdec_unaryexpr {
  public:
	post_Increment_expr(Type const* const type, Expression* const operand);
};


class pre_Decrement_expr : public Incdec_unaryexpr {
  public:
	pre_Decrement_expr(Type const* const type, Expression* const operand);
};


class post_Decrement_expr : public Incdec_unaryexpr {
  public:
	post_Decrement_expr(Type const* const type, Expression* const operand);
};

/*****************************/

class Minus_expr : public Arithmetic_unaryexpr {
  public:
	Minus_expr(Type const* const type, Expression* const operand);
};


class B_NOT_expr : public Arithmetic_unaryexpr {
  public:
	B_NOT_expr(Type const* const type, Expression* const operand);
};


class Reciprocal_expr : public Arithmetic_unaryexpr {
  public:
	Reciprocal_expr(Type const* const type, Expression* const operand);
};

/*****************************/

class L_NOT_expr : public Logical_unaryexpr {
  public:
	L_NOT_expr(Type const* const type, Expression* const operand);
};
