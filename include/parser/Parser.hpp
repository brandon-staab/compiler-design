#pragma once

#include "lexer/Lexer.hpp"
#include "semantics/SemanticActions.hpp"


class Parser {
  public:
	Parser(SymbolTable& syms, std::string const& input);

	// Lookahead
	Token const& peek() const;
	Token::Kind lookahead() const;

	bool is_eof() const;
	bool next_token_is(Token::Kind const kind) const;
	bool next_token_is_not(Token::Kind const kind) const;

	// Consumption
	Token consume();
	Token match(Token::Kind kind);
	Token expect(Token::Kind kind);
	Token require(Token::Kind kind);

	Token match_equality_operator();
	Token match_relational_operator();
	Token match_shift_operator();
	Token match_additive_operator();
	Token match_multiplicative_operator();
	Token match_access_operator();
	Token match_prefix_operator();
	Token match_postfix_operator();

	// Scope
	void enter_scope();
	void leave_scope();
	Declaration* declare(Symbol sym, Declaration* decl);
	Declaration* declare(Token token, Declaration* decl);
	Declaration* lookup(Symbol sym);
	Declaration* lookup(Token token);

	// Declaration
	Declaration* parse_program();
	Decl_seq parse_declaration_seq();
	Declaration* parse_declaration();
	Declaration* parse_local_declaration();
	Declaration* parse_variable_definition();
	Declaration* parse_object_definition();
	Declaration* parse_reference_definition();
	Declaration* parse_function_definition();
	Decl_seq parse_parameter_list();
	Declaration* parse_parameter();

	// Expression
	Expression* parse_expression();
	Expression* parse_assignment_expression();
	Expression* parse_conditional_expression();
	Expression* parse_logical_or_expression();
	Expression* parse_logical_and_expression();
	Expression* parse_bitwise_or_expression();
	Expression* parse_bitwise_xor_expression();
	Expression* parse_bitwise_and_expression();
	Expression* parse_equality_expression();
	Expression* parse_relational_expression();
	Expression* parse_shift_expression();
	Expression* parse_additive_expression();
	Expression* parse_multiplicative_expression();
	Expression* parse_access_expression();
	Expression* parse_prefix_expression();
	Expression* parse_postfix_expression();
	Expression* parse_call_expression(Expression* expr, Token lparen);
	Expr_seq parse_argument_list();
	Expression* parse_argument();
	Expression* parse_primary_expression();
	Expression* parse_identifier_expression();

	// Statement
	Statement* parse_statement();
	Statement* parse_empty_statement();
	Statement* parse_block_statement();
	Statement* parse_if_statement();
	Statement* parse_while_statement();
	Statement* parse_break_statement();
	Statement* parse_continue_statement();
	Statement* parse_return_statement();
	Statement* parse_declaration_statement();
	Statement* parse_expression_statement();

	// Type
	Type const* parse_type_specifier();
	Type const* parse_ref_type();

  private:
	Lexer m_lex;
	std::vector<Token> m_toks;
	Token* m_lookahead;
	Token* m_last;
	SemanticActions m_act;
};
