#pragma once

#include <vector>

class Type;

using Type_seq = std::vector<Type const*>;
