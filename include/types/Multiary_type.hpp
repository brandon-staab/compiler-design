#pragma once

#include "types/Type.hpp"


class Multiary_type : public Type, public Dynamic_arity_node<Type const> {
  protected:
	Multiary_type(Kind const kind, Type_seq const& operands);
};

/*************************************************************/

class Fun_type : public Multiary_type {
  public:
	Fun_type(Type_seq const& operands);

	std::size_t get_num_parameters() const { return get_arity() - 1; }

	Type const* const get_parm_type(std::size_t const i) const;
	Type const* const get_ret_type() const;
};
