#pragma once

#include "types/type_fwd.hpp"

#include "arity_node/arity_incl.hpp"

#include <iosfwd>


class Type {
  public:
	enum class Kind : char {
		// Nullary
		bool_type,
		char_type,
		int_type,
		uint_type,
		float_type,
		double_type,

		// Unary
		ref_type,

		// Multiary
		fun_type
	};

	Kind get_kind() const;

	bool is_bool() const;
	bool is_char() const;
	bool is_int() const;
	bool is_uint() const;
	bool is_float() const;
	bool is_double() const;
	bool is_reference() const;
	bool is_function() const;

	bool is_primitive() const;
	bool is_object() const;
	bool is_variable() const;
	bool is_integral() const;
	bool is_arithmetic() const;
	bool is_reference_to(Type const* const that) const;

	virtual ~Type() = default;

  protected:
	Type(Kind const kind);

  private:
	Kind m_kind;
};

/*****************************/

template <std::size_t N>
class Fixed_Arity_type : public Type, public Static_arity_node<Type const, N> {
  protected:
	// clang-format off
	template <typename ...Args>	 // clang-format on
	Fixed_Arity_type(Kind const kind, Args&&... args) : Type(kind), Static_arity_node<Type const, N>(args...) {}
};


/*************************************************************/

std::ostream& operator<<(std::ostream& os, Type const& type);
std::ostream& operator<<(std::ostream& os, Type::Kind const kind);
bool operator==(Type const& lhs, Type const& rhs);
bool operator!=(Type const& lhs, Type const& rhs);

/*****************************/

bool is_equal(Type const* const a, Type const* const b);
char const* const kind_to_string(Type::Kind const kind);
void dump_debug(std::ostream& os, Type const* const type, std::size_t depth = 0);
void print(std::ostream& os, Type const* const type);
void to_sexpr(std::ostream& os, Type const* const type);
