#pragma once

#include "types/Type.hpp"

// clang-format off
#include "types/Nullary_type.hpp"
#include "types/Unary_type.hpp"
#include "types/Multiary_type.hpp"
