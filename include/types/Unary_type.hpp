#pragma once

#include "types/Type.hpp"


class Unary_type : public Fixed_Arity_type<1> {
  protected:
	Unary_type(Kind const kind, Type const* const type);
};

/*************************************************************/

class Ref_type : public Unary_type {
  public:
	Ref_type(Type const* const type);

	Type const* get_referent_type() const;
};
