#pragma once

#include "types/Type.hpp"


class Nullary_type : public Fixed_Arity_type<0> {
  protected:
	Nullary_type(Kind const kind);
};

/*************************************************************/

class Bool_type : public Nullary_type {
  public:
	Bool_type();
};


class Char_type : public Nullary_type {
  public:
	Char_type();
};


class Int_type : public Nullary_type {
  public:
	Int_type();
};


class UInt_type : public Nullary_type {
  public:
	UInt_type();
};


class Float_type : public Nullary_type {
  public:
	Float_type();
};


class Double_type : public Nullary_type {
  public:
	Double_type();
};
