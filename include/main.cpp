#include "parser/parser_fwd.hpp"
#include "utilities/load_file.hpp"

#include <iostream>


int main(int argc, char const* argv[]) {
	if (argc != 2) {
		std::cerr << argv[0] << " <filename>\n";
		return EXIT_FAILURE;
	}

	auto input = load_file(argv[1]);
	std::cout << input << " - - - -\n";

	auto syms = SymbolTable{};
	auto parser = Parser{syms, input};
	auto program = parser.parse_program();
	std::cout << " - - - -" << std::endl;

	dump_debug(std::cout, program);
	std::cout << "\n - - - -" << std::endl;
	print(std::cout, program);
	std::cout << "\n - - - -" << std::endl;
	to_sexpr(std::cout, program);
	std::cout << std::endl;
}
