#pragma once

#include "components/Object.hpp"
#include "declarations/decl_fwd.hpp"

#include <unordered_map>


class Store {
  public:
	virtual Object* const allocate(Declaration const* const decl) = 0;
	virtual Object* const locate(Declaration const* const decl) = 0;
	virtual void alias(Declaration const* const decl, Object const* obj) = 0;

  protected:
	std::vector<Object> m_storage;
	std::unordered_map<Declaration const*, std::size_t> m_lookup;
};
