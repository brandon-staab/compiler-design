#pragma once

#include "store/Store.hpp"


class Monotonic_store : public Store {
  public:
	Object* const allocate(Declaration const* const decl) final;
	Object* const locate(Declaration const* const decl) final;
	void alias(Declaration const* const decl, Object const* obj) final;
};
