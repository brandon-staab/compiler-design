#pragma once

#include <array>
#include <vector>

class Statement;

using Stmt_seq = std::vector<Statement*>;
