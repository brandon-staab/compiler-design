#pragma once

#include "statements/Statement.hpp"


class Nullary_stmt : public Fixed_Arity_stmt<0> {
  protected:
	Nullary_stmt(Kind const kind);
};

/*************************************************************/

class Skip_stmt : public Nullary_stmt {
  public:
	Skip_stmt();
};

class Break_stmt : public Nullary_stmt {
  public:
	Break_stmt();
};


class Continue_stmt : public Nullary_stmt {
  public:
	Continue_stmt();
};


class Return_stmt : public Nullary_stmt {
  public:
	Return_stmt(Expression const* const expr);

	Expression const* get_expr() const;

  private:
	Expression const* m_expr;
};


class Expression_stmt : public Nullary_stmt {
  public:
	Expression_stmt(Expression const* const expr);

	Expression const* get_expr() const;

  private:
	Expression const* m_expr;
};


class Declaration_stmt : public Nullary_stmt {
  public:
	Declaration_stmt(Declaration const* const decl);

	Declaration const* get_decl() const;

  private:
	Declaration const* m_decl;
};
