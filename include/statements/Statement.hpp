#pragma once

#include "statements/stmt_fwd.hpp"

#include "arity_node/arity_incl.hpp"
#include "declarations/decl_fwd.hpp"
#include "expressions/expr_fwd.hpp"

#include <iosfwd>


class Statement {
  public:
	enum class Kind : char {
		// Nullary
		skip_stmt,
		break_stmt,
		continue_stmt,
		declaration_stmt,
		expression_stmt,
		return_stmt,

		// Unary
		when_stmt,
		while_stmt,

		// Binary
		if_stmt,

		// Multiary
		block_stmt
	};

	virtual ~Statement() = default;

	Kind get_kind() const;

  protected:
	Statement(Kind const kind);

  private:
	Kind m_kind;
};

/*****************************/

template <std::size_t N>
class Fixed_Arity_stmt : public Statement, public Static_arity_node<Statement, N> {
  protected:
	// clang-format off
	template <typename ...Args>	 // clang-format on
	Fixed_Arity_stmt(Kind const kind, Args&&... args) : Statement(kind), Static_arity_node<Statement, N>(args...) {}
};

/*************************************************************/

std::ostream& operator<<(std::ostream& os, Statement const& stmt);
std::ostream& operator<<(std::ostream& os, Statement::Kind const kind);

/*****************************/

char const* const kind_to_string(Statement::Kind const kind);
void dump_debug(std::ostream& os, Statement const* const stmt, std::size_t depth = 0);
void print(std::ostream& os, Statement const* const stmt, std::size_t depth = 0);
void to_sexpr(std::ostream& os, Statement const* const stmt);
