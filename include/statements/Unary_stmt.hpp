#pragma once

#include "statements/Statement.hpp"


class Unary_stmt : public Fixed_Arity_stmt<1> {
  protected:
	Unary_stmt(Kind const kind, Statement* const stmt);
};

/*************************************************************/

class When_stmt : public Unary_stmt {
  public:
	When_stmt(Expression const* condition, Statement* const stmt);

	Expression const* const get_condition() const;
	Statement const* const get_stmt() const;

  private:
	Expression const* const m_condition;
};

class While_stmt : public Unary_stmt {
  public:
	While_stmt(Expression const* condition, Statement* const stmt);

	Expression const* const get_condition() const;
	Statement const* const get_stmt() const;

  private:
	Expression const* const m_condition;
};
