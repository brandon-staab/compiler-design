#pragma once

#include "statements/Statement.hpp"


class Multiary_stmt : public Statement, public Dynamic_arity_node<Statement> {
  public:
	Statement const* get_stmt(std::size_t const i) const;

  protected:
	Multiary_stmt(Kind const kind, Stmt_seq& operands);
};

/*************************************************************/

class Block_stmt : public Multiary_stmt {
  public:
	Block_stmt(Stmt_seq& operands);
};
