#pragma once

#include "statements/Statement.hpp"

// clang-format off
#include "statements/Nullary_stmt.hpp"
#include "statements/Unary_stmt.hpp"
#include "statements/Binary_stmt.hpp"
#include "statements/Multiary_stmt.hpp"
