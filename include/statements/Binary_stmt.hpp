#pragma once

#include "statements/Statement.hpp"


class Binary_stmt : public Fixed_Arity_stmt<2> {
  protected:
	Binary_stmt(Kind const kind, Statement* const s1, Statement* const s2);
};

/*************************************************************/

class If_stmt : public Binary_stmt {
  public:
	If_stmt(Expression* const condition, Statement* const true_stmt, Statement* const false_stmt);

	Expression const* const get_condition() const;
	Statement const* const get_true_stmt() const;
	Statement const* const get_false_stmt() const;

  private:
	Expression const* const m_condition;
};
