#pragma once

#include <iosfwd>


class Location {
  public:
	Location();
	Location(std::size_t const line_number, std::size_t const column_number);

	std::size_t get_line_num() const;
	std::size_t get_column_num() const;

  private:  // TODO: use bitset
	std::size_t m_line_number;
	std::size_t m_column_number;
};

/*************************************************************/

std::ostream& operator<<(std::ostream& os, Location const& location);
