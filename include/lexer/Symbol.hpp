#pragma once

#include "lexer/Name.hpp"

#include <iosfwd>
#include <string>


class Symbol {
	friend class SymbolTable;
	friend bool operator==(Symbol const& lhs, Symbol const& rhs);
	friend bool operator!=(Symbol const& lhs, Symbol const& rhs);

  public:
	Symbol();
	Symbol(Name const* name);

	Name const& name() const;

  private:
	Name const* m_name;
};

/*************************************************************/

namespace std {
	template <>
	struct hash<Symbol> {
		std::size_t operator()(Symbol const& sym) const noexcept {
			std::hash<std::string const*> h;
			return h(&sym.name());
		}
	};
};	// namespace std

/*****************************/

bool operator==(Symbol const& lhs, Symbol const& rhs);
bool operator!=(Symbol const& lhs, Symbol const& rhs);

/*****************************/

std::ostream& operator<<(std::ostream& os, Symbol const& symbol);
