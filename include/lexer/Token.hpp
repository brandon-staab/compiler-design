#pragma once

#include "lexer/Location.hpp"
#include "lexer/Symbol.hpp"

#include <iosfwd>


class Token {
  public:
	enum class Kind : char {
		// special
		eof_tok,

		// Identifiers
		identifier_tok,

		// Keywords
		bool_tok,
		break_tok,
		case_tok,
		catch_tok,
		char_tok,
		class_tok,
		const_tok,
		continue_tok,
		delete_tok,
		do_tok,
		double_tok,
		else_tok,
		enum_tok,
		explicit_tok,
		false_tok,
		float_tok,
		for_tok,
		fun_tok,
		goto_tok,
		if_tok,
		int_tok,
		let_tok,
		long_tok,
		namespace_tok,
		new_tok,
		nullptr_tok,
		operator_tok,
		private_tok,
		protected_tok,
		public_tok,
		reinterpret_cast_tok,
		ref_tok,
		return_tok,
		short_tok,
		signed_tok,
		sizeof_tok,
		static_assert_tok,
		static_cast_tok,
		struct_tok,
		switch_tok,
		throw_tok,
		true_tok,
		try_tok,
		union_tok,
		unsigned_tok,
		using_tok,
		var_tok,
		void_tok,
		while_tok,

		// Literals
		integer_literal_tok,
		float_literal_tok,


		// Punctuation
		lbrace_tok,
		rbrace_tok,
		lbracket_tok,
		rbracket_tok,
		lparen_tok,
		rparen_tok,
		semicolon_tok,
		colon_tok,
		ellipses_tok,
		question_tok,
		scope_tok,
		dot_tok,
		dot_star_tok,
		rarrow_tok,
		rarrow_star_tok,
		comma_tok,

		// Operators
		tilde_tok,
		bang_tok,
		plus_tok,
		minus_tok,
		star_tok,
		slash_tok,
		percent_tok,
		caret_tok,
		ampersand_tok,
		bar_tok,
		eq_tok,
		plus_eq_tok,
		minus_eq_tok,
		star_eq_tok,
		slash_eq_tok,
		percent_eq_tok,
		caret_eq_tok,
		ampersand_eq_tok,
		bar_eq_tok,
		eq_eq_tok,
		bang_eq_tok,
		less_tok,
		greater_tok,
		less_eq_tok,
		greater_eq_tok,
		spaceship_tok,
		ampersand_ampersand_tok,
		bar_bar_tok,
		less_less_tok,
		greater_greater_tok,
		less_less_eq_tok,
		greater_greater_eq_tok,
		plus_plus_tok,
		minus_minus_tok,
		and_tok,
		or_tok,
		xor_tok,
		not_tok,
		bitand_tok,
		bitor_tok,
		and_eq_tok,
		or_eq_tok,
		xor_eq_tok,
		not_eq_tok,
	};

	Token();
	Token(Kind const kind, Symbol const& sym, Location const& loc);

	explicit operator bool() const;

	bool is(Kind const kind) const;
	bool is_identifier() const;
	bool is_keyword() const;
	bool is_literal() const;
	bool is_punctuator() const;
	bool is_operator() const;

	Kind get_kind() const;
	Symbol get_lexeme() const;
	Location get_location() const;


  private:
	Kind m_kind;
	Symbol m_lex;
	Location m_loc;
};

/*************************************************************/

std::ostream& operator<<(std::ostream& os, Token const& token);
std::ostream& operator<<(std::ostream& os, Token::Kind const kind);

/*****************************/

char const* const kind_to_string(Token::Kind const kind);
