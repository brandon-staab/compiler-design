#pragma once

#include "lexer/Name.hpp"
#include "lexer/Symbol.hpp"

#include <unordered_set>


class SymbolTable : private std::unordered_set<Name> {
  public:
	Symbol get(Name const& name);
};
