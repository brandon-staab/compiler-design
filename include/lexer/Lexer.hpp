#pragma once

#include "lexer/SymbolTable.hpp"
#include "lexer/Token.hpp"

#include <sstream>
#include <unordered_map>


class Lexer {
  public:
	Lexer(SymbolTable& syms, char const* first, char const* limit);
	Lexer(SymbolTable& syms, std::string const& str);

	Token operator()();
	Token get_next_token();

  private:
	bool is_eof() const;
	bool is_eof(char const* ptr) const;
	char peek() const;
	char peek(std::size_t const n) const;
	char consume();
	void consume_comment();
	Token match(Token::Kind const kind, std::size_t const len);
	Token match_number();
	Token match_word();
	void error();

	void number(char const*& cursor, Token::Kind& kind);
	void binary_digits(char const*& cursor);
	void digits(char const*& cursor);
	void hexadecimal_digits(char const*& cursor);
	void float_literal_rest(char const*& cursor, Token::Kind& kind);
	void exponent_rest(char const*& cursor);

  private:
	SymbolTable* const m_syms;
	char const* m_cursor;
	char const* m_limit;
	std::size_t m_line_number;
	unsigned short m_column_number;
	std::unordered_map<std::string, Token::Kind> m_keywords;
	std::stringstream m_ss;
};
