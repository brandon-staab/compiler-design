#pragma once

#include <memory>


template <typename T>
class Arity_node {
  public:
	virtual T* get_operand(std::size_t const i) = 0;
	virtual T const* get_operand(std::size_t const i) const = 0;

	virtual T** begin() = 0;
	virtual T** end() = 0;
	virtual T* const* begin() const = 0;
	virtual T* const* end() const = 0;
};
