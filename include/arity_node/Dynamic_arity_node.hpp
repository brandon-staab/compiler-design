#pragma once

#include "arity_node/Arity_node.hpp"

#include <vector>


template <typename T>
class Dynamic_arity_node : public Arity_node<T> {
  public:
	std::size_t get_arity() const;
	T* get_operand(std::size_t const i) final;
	T const* get_operand(std::size_t const i) const final;
	std::vector<T*>& get_operands();
	std::vector<T*> const& get_operands() const;

	T** begin() final;
	T** end() final;
	T* const* begin() const final;
	T* const* end() const final;

  protected:
	Dynamic_arity_node(std::vector<T*> const& node_seq);
	Dynamic_arity_node(std::vector<T*>&& other);

  private:
	std::vector<T*> m_operands;
};


#include "arity_node/Dynamic_arity_node.tpp"
