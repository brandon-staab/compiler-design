#pragma once

#include "arity_node/Arity_node.hpp"

#include <array>


template <typename T, std::size_t N>
class Static_arity_node : public Arity_node<T> {
  public:
	static constexpr std::size_t get_arity();
	T* get_operand(std::size_t const i) final;
	T const* get_operand(std::size_t const i) const final;

	T** begin() final;
	T** end() final;
	T* const* begin() const final;
	T* const* end() const final;

  protected:
	// clang-format off
  	template <typename ...Args>	 // clang-format on
	Static_arity_node(Args&&... args);


  private:
	std::array<T*, N> m_operands;
};

#include "arity_node/Static_arity_node.tpp"
