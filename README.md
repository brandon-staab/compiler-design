compiler-design [![pipeline status](https://gitlab.com/brandon-staab/compiler-design/badges/master/pipeline.svg)](https://gitlab.com/brandon-staab/compiler-design/commits/master)
===============================

What is this?
-------------------------------
An implementation of MiniC from the compiler design course.
[MiniC Language Specification](https://gitlab.com/brandon-staab/compiler-design/blob/master/docs/spec/project.pdf)

Requirements
-------------------------------
```shell
./dependencies.sh
```

* C++ 20

Setting up build environment
-------------------------------
```shell
mkdir -p build
cd build
cmake ..
```

Running
-------------------------------
```shell
make run args="arg1 arg2 arg3"
```

Documentation
-------------------------------
### [README.md](https://gitlab.com/brandon-staab/compiler-design/-/blob/master/README.md)

### Doxygen
```shell
make docs
```

Testing
-------------------------------
```shell
make check
```

Debugging
-------------------------------
```shell
make debug
```
