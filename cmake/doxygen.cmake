set("enlarge_lex_buffers" 1048576)

find_package("Doxygen"
	REQUIRED
		"dot"
	OPTIONAL_COMPONENTS
		"mscgen"
		"dia"
)

doxygen_add_docs("doxygen" "${PROJECT_SOURCE_DIR}")
