include("src/sources.cmake")

add_library("core.lib" "${CORE_SOURCE}")

target_compile_features("core.lib"
	PUBLIC
		"cxx_std_20"
)

target_link_libraries("core.lib"
	PUBLIC
		"project_warnings"
		"project_packages"
)

target_include_directories(core.lib
	PUBLIC
		"${CMAKE_SOURCE_DIR}/src"
		"${CMAKE_SOURCE_DIR}/include"
)
