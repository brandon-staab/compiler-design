enable_testing()
option("BUILD_TESTS" "Build tests" ON)

add_library("test.lib" ALIAS "core.lib")
add_custom_target("check" COMMAND "${CMAKE_CTEST_COMMAND}")

function(add_unit_test TEST)
	string(REPLACE "./" "" "TEST_FILENAME" "${TEST}")
	string(REPLACE "/" "-" "TEST_FILENAME" "${TEST_FILENAME}")
	string(REPLACE "-" "." "TEST_NAME" "${TEST_FILENAME}")

	add_executable("${TEST_FILENAME}" "${TEST}")
	set_target_properties("${TEST_FILENAME}" PROPERTIES
		RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}/bin/tests"
	)
	target_link_libraries(${TEST_FILENAME} "test.lib")

	add_test(NAME "${TEST_NAME}" COMMAND "${TEST_FILENAME}")
	add_dependencies("check" "${TEST_FILENAME}")
endfunction()

include("test/sources.cmake")
foreach(TEST IN LISTS TESTS_SOURCES)
	add_unit_test("${TEST}")
endforeach()
