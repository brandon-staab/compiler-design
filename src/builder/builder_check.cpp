#include "builder/builder.hpp"

#include "expressions/expr_incl.hpp"

#include <iostream>


namespace Builder {
	Expression* require_bool(Expression* expr) {
		assert(expr != nullptr);

		Expression* convert = convert_to_value(expr);
		if (convert->get_type()->is_bool()) {
			return convert;
		} else {
			throw std::runtime_error("operand not boolean");
		}
	}


	Expression* require_arithmetic(Expression* expr) {
		assert(expr != nullptr);

		Expression* convert = convert_to_value(expr);
		if (convert->get_type()->is_arithmetic()) {
			return convert;
		} else {
			throw std::runtime_error("operand not arithmetic");
		}
	}


	Expression* require_function(Expression* expr) {
		assert(expr != nullptr);

		Expression* convert = convert_to_value(expr);
		if (convert->get_type()->is_function()) {
			return convert;
		} else {
			throw std::runtime_error("operand not function");
		}
	}


	Expression* require_type(Expression* expr, Type const* type) {
		assert(expr != nullptr);
		assert(type != nullptr);

		if (type->is_reference()) {
			return require_reference_to(expr, type);
		} else {
			return require_value_of(expr, type);
		}
	}

	Expression* require_value_of(Expression* expr, Type const* type) {
		assert(expr != nullptr);
		assert(type != nullptr);
		assert(type->is_object());

		Expression* expr_val = convert_to_value(expr);
		if (*expr_val->get_type() == *type) {
			return expr_val;
		} else {
			print(std::cerr, expr_val->get_type());
			print(std::cerr, type);
			throw std::runtime_error("invalid operand: required val");
		}
	}


	Expression* require_reference_to(Expression* expr, Type const* type) {
		assert(expr != nullptr);
		assert(type != nullptr);

		if (expr->get_type()->is_reference_to(type)) {
			return expr;
		} else {
			throw std::runtime_error("invalid operand: required ref");
		}
	}


	std::pair<Expression*, Expression*> require_same(Expression* e1, Expression* e2) {
		assert(e1 != nullptr);
		assert(e2 != nullptr);

		if (*e1->get_type() == *e2->get_type()) {
			return {e1, e2};
		} else {
			throw std::runtime_error("operands have different type");
		}
	}


	std::pair<Expression*, Expression*> require_same_value(Expression* e1, Expression* e2) {
		assert(e1 != nullptr);
		assert(e2 != nullptr);

		return {convert_to_value(e1), convert_to_value(e2)};
	}


	std::pair<Expression*, Expression*> require_same_arithmetic(Expression* e1, Expression* e2) {
		assert(e1 != nullptr);
		assert(e2 != nullptr);

		auto e_pair = std::pair<Expression*, Expression*>{convert_to_value(e1), convert_to_value(e2)};
		e_pair = require_same(e_pair.first, e_pair.second);
		if (e_pair.first->get_type()->is_arithmetic()) {
			return e_pair;
		} else {
			throw std::runtime_error("operands not arithmetic");
		}
	}


	std::pair<Expression*, Expression*> require_common(Expression* e1, Expression* e2) {
		assert(e1 != nullptr);
		assert(e2 != nullptr);

		if (e1->get_type()->is_reference() && e2->get_type()->is_reference()) {
			return require_same(e1, e2);
		} else {
			return require_same_value(e1, e2);
		}
	}
}  // namespace Builder
