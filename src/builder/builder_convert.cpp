#include "builder/builder.hpp"

#include "expressions/expr_incl.hpp"

#include <iostream>


namespace Builder {
	Expression* convert_to_value(Expression* expr) {
		assert(expr != nullptr);

		std::cerr << "convert_to_value\n";
		if (expr->get_type()->is_reference()) {
			return make_Convert_Value_expr(expr);
		}

		return expr;
	}


	Expression* convert_to_reference(Expression* expr) {
		assert(expr != nullptr);

		throw std::runtime_error("FIXME: convert_to_reference");

		return expr;
	}
}  // namespace Builder
