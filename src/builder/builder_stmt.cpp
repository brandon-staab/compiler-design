#include "builder/builder.hpp"

#include "expressions/expr_incl.hpp"
#include "statements/stmt_incl.hpp"

#include <iostream>


namespace {
	auto skip_stmt = Skip_stmt();
	auto break_stmt = Break_stmt();
	auto continue_stmt = Continue_stmt();
}  // namespace


namespace Builder {
	Statement* get_Skip_stmt() { return &skip_stmt; }


	Statement* get_Break_stmt() { return &break_stmt; }


	Statement* get_Continue_stmt() { return &continue_stmt; }


	Statement* make_Block_stmt(Stmt_seq& stmts) {
		for (auto stmt : stmts) {
			assert(stmt != nullptr);
		}

		return new Block_stmt(stmts);
	}


	Statement* make_When_stmt(Expression* condition, Statement* stmt) {
		assert(condition != nullptr);
		assert(stmt != nullptr);

		return new When_stmt(condition, stmt);
	}


	Statement* make_While_stmt(Expression* condition, Statement* stmt) {
		assert(condition != nullptr);
		assert(stmt != nullptr);

		return new While_stmt(condition, stmt);
	}


	Statement* make_If_stmt(Expression* condition, Statement* true_stmt, Statement* false_stmt) {
		assert(condition != nullptr);
		assert(true_stmt != nullptr);
		assert(false_stmt != nullptr);

		return new If_stmt(condition, true_stmt, false_stmt);
	}


	Statement* make_Return_stmt(Expression* expr) {
		assert(expr != nullptr);

		return new Return_stmt(expr);
	}


	Statement* make_Expression_stmt(Expression* expr) {
		assert(expr != nullptr);

		return new Expression_stmt(expr);
	}


	Statement* make_Declaration_stmt(Declaration* decl) {
		assert(decl != nullptr);

		return new Declaration_stmt(decl);
	}
}  // namespace Builder
