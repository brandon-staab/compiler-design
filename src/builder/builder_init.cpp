#include "builder/builder.hpp"

#include "declarations/decl_incl.hpp"
#include "expressions/expr_incl.hpp"

#include <iostream>


namespace Builder {
	void copy_initialize(Declaration* decl, Expression* expr) {
		assert(decl != nullptr);
		assert(expr != nullptr);
		assert(decl->is_variable());

		if (decl->is_reference()) {
			return reference_initialize(decl, expr);
		}

		auto var = static_cast<Variable_decl*>(decl);
		expr = require_type(expr, decl->get_type());
		var->set_initializer(expr);
	}

	void reference_initialize(Declaration* decl, Expression* expr) {
		assert(decl != nullptr);
		assert(expr != nullptr);
		assert(decl->is_variable());

		auto var = static_cast<Variable_decl*>(decl);
		std::cerr << "reff init" << '\n';
		expr = require_type(expr, decl->get_type());
		// expr = convert_to_reference(expr); // FIXME
		var->set_initializer(expr);

		throw std::runtime_error("FIXME: reference_initialize");
	}

}  // namespace Builder
