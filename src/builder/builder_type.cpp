#include "builder/builder.hpp"

#include "types/type_incl.hpp"

#include <unordered_map>


namespace std {
	template <>
	struct hash<Type_seq> {
		std::size_t operator()(Type_seq const& types) const {
			std::size_t seed = types.size();

			for (auto const& i : types) {
				seed ^= reinterpret_cast<std::size_t>(i) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
			}

			return seed;
		}
	};
}  // namespace std


namespace {
	auto const bool_type = Bool_type();
	auto const char_type = Char_type();
	auto const int_type = Int_type();
	auto const uint_type = UInt_type();
	auto const float_type = Float_type();
	auto const double_type = Double_type();
	auto ref_types = std::unordered_map<Type const*, Ref_type const*>{
		// clang-format off
		{&bool_type,   new Ref_type(&bool_type)},
		{&char_type,   new Ref_type(&char_type)},
		{&int_type,    new Ref_type(&int_type)},
		{&uint_type,   new Ref_type(&uint_type)},
		{&float_type,  new Ref_type(&float_type)},
		{&double_type, new Ref_type(&double_type)}
		// clang-format on
	};
	auto fun_types = std::unordered_map<Type_seq, Fun_type const*>{};
}  // namespace


namespace Builder {
	Type const* get_Bool_type() { return &bool_type; }


	Type const* get_Char_type() { return &char_type; }


	Type const* get_Int_type() { return &int_type; }


	Type const* get_UInt_type() { return &uint_type; }


	Type const* get_Float_type() { return &float_type; }


	Type const* get_Double_type() { return &double_type; }


	Type const* get_Ref_type(Type const* type) {
		assert(type != nullptr);

		if (ref_types.count(type) == 0) {
			ref_types.emplace(std::make_pair(type, new Ref_type(type)));
		}

		return ref_types[type];
	}


	Fun_type const* get_Fun_type(Type_seq const types) {
		assert(!types.empty());
		for (auto op : types) {
			assert(op != nullptr);
		}

		if (fun_types.count(types) == 0) {
			fun_types.emplace(std::make_pair(types, new Fun_type(types)));
		}

		return fun_types[types];
	}
}  // namespace Builder
