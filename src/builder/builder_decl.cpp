#include "builder/builder.hpp"

#include "declarations/decl_incl.hpp"
#include "expressions/expr_incl.hpp"


namespace Builder {
	Declaration* make_Variable_decl(Name const& name, Type const* type) {
		assert(type != nullptr);

		return new Variable_decl(name, type);
	}


	Declaration* make_Function_decl(Name const& name, Fun_type const* type) {
		assert(type != nullptr);

		return new Function_decl(name, type);
	}


	Declaration* make_Program_decl(Decl_seq& decls) {
		for (auto decl : decls) {
			assert(decl != nullptr);
		}

		return new Program_decl(decls);
	}
}  // namespace Builder
