#include "builder/builder.hpp"

#include "expressions/expr_incl.hpp"
#include "types/type_incl.hpp"

#include <iostream>


namespace Builder {
	/**** Nullary ****************/
	Expression* make_Identifier_expr(Declaration const* id) {
		Type const* type;

		if (id->is_variable()) {
			type = get_Ref_type(id->get_type());
		} else if (id->is_function()) {
			type = id->get_type();
		} else {
			throw std::logic_error("invalid id-expression");
		}

		return new Identifier_expr(type, id);
	}


	Expression* make_Bool_expr(bool const b) { return new Bool_expr(get_Bool_type(), b); }


	Expression* make_Char_expr(char const c) { return new Char_expr(get_Char_type(), c); }


	Expression* make_Int_expr(int const i) { return new Int_expr(get_Int_type(), i); }


	Expression* make_UInt_expr(unsigned int const u) { return new UInt_expr(get_UInt_type(), u); }


	Expression* make_Float_expr(float const f) { return new Float_expr(get_Float_type(), f); }


	Expression* make_Double_expr(double const d) { return new Double_expr(get_Double_type(), d); }

	/**** Unary ******************/

	Expression* make_Convert_Value_expr(Expression* operand) {
		return new Convert_Value_expr(static_cast<Ref_type const*>(operand->get_type())->get_referent_type(), operand);
	}


	Expression* make_pre_Increment_expr(Expression* operand) {
		assert(operand != nullptr);

		operand = require_arithmetic(operand);
		return new pre_Increment_expr(operand->get_type(), operand);
	}


	Expression* make_post_Increment_expr(Expression* operand) {
		assert(operand != nullptr);

		operand = require_arithmetic(operand);
		return new post_Increment_expr(operand->get_type(), operand);
	}


	Expression* make_pre_Decrement_expr(Expression* operand) {
		assert(operand != nullptr);

		operand = require_arithmetic(operand);
		return new pre_Decrement_expr(operand->get_type(), operand);
	}


	Expression* make_post_Decrement_expr(Expression* operand) {
		assert(operand != nullptr);

		operand = require_arithmetic(operand);
		return new post_Decrement_expr(operand->get_type(), operand);
	}


	Expression* make_Minus_expr(Expression* operand) {
		assert(operand != nullptr);

		operand = require_arithmetic(operand);
		return new Minus_expr(operand->get_type(), operand);
	}


	Expression* make_B_NOT_expr(Expression* operand) {
		assert(operand != nullptr);

		operand = require_bool(operand);
		return new B_NOT_expr(operand->get_type(), operand);
	}


	Expression* make_Reciprocal_expr(Expression* operand) {
		assert(operand != nullptr);

		operand = require_arithmetic(operand);
		return new Reciprocal_expr(operand->get_type(), operand);
	}


	Expression* make_L_NOT_expr(Expression* operand) {
		assert(operand != nullptr);

		operand = require_bool(operand);
		return new L_NOT_expr(operand->get_type(), operand);
	}

	/**** Binary *****************/
	Expression* make_Assignment_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		rhs = convert_to_value(rhs);
		lhs = require_reference_to(lhs, rhs->get_type());
		return new Assignment_expr(lhs->get_type(), lhs, rhs);
	}


	Expression* make_Addition_Assignment_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		rhs = convert_to_value(rhs);
		lhs = require_reference_to(lhs, rhs->get_type());
		return new Addition_Assignment_expr(lhs->get_type(), lhs, rhs);
	}


	Expression* make_Subtraction_Assignment_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		rhs = convert_to_value(rhs);
		lhs = require_reference_to(lhs, rhs->get_type());
		return new Subtraction_Assignment_expr(lhs->get_type(), lhs, rhs);
	}


	Expression* make_Multiplication_Assignment_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		rhs = convert_to_value(rhs);
		lhs = require_reference_to(lhs, rhs->get_type());
		return new Multiplication_Assignment_expr(lhs->get_type(), lhs, rhs);
	}


	Expression* make_Quotient_Assignment_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		rhs = convert_to_value(rhs);
		lhs = require_reference_to(lhs, rhs->get_type());
		return new Quotient_Assignment_expr(lhs->get_type(), lhs, rhs);
	}


	Expression* make_Modulo_Assignment_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		rhs = convert_to_value(rhs);
		lhs = require_reference_to(lhs, rhs->get_type());
		return new Modulo_Assignment_expr(lhs->get_type(), lhs, rhs);
	}


	Expression* make_B_AND_Assignment_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		rhs = convert_to_value(rhs);
		lhs = require_reference_to(lhs, rhs->get_type());
		return new B_AND_Assignment_expr(lhs->get_type(), lhs, rhs);
	}


	Expression* make_B_OR_Assignment_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		rhs = convert_to_value(rhs);
		lhs = require_reference_to(lhs, rhs->get_type());
		return new B_OR_Assignment_expr(lhs->get_type(), lhs, rhs);
	}


	Expression* make_B_XOR_Assignment_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		rhs = convert_to_value(rhs);
		lhs = require_reference_to(lhs, rhs->get_type());
		return new B_XOR_Assignment_expr(lhs->get_type(), lhs, rhs);
	}


	Expression* make_B_LSHIFT_Assignment_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		rhs = convert_to_value(rhs);
		lhs = require_reference_to(lhs, rhs->get_type());
		return new B_LSHIFT_Assignment_expr(lhs->get_type(), lhs, rhs);
	}


	Expression* make_B_RSHIFT_Assignment_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		rhs = convert_to_value(rhs);
		lhs = require_reference_to(lhs, rhs->get_type());
		return new B_RSHIFT_Assignment_expr(lhs->get_type(), lhs, rhs);
	}


	Expression* make_B_AND_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		auto val_pair = require_same_value(lhs, rhs);
		return new B_AND_expr(get_Bool_type(), val_pair.first, val_pair.second);
	}


	Expression* make_B_OR_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		auto val_pair = require_same_value(lhs, rhs);
		return new B_OR_expr(get_Bool_type(), val_pair.first, val_pair.second);
	}


	Expression* make_B_XOR_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		auto val_pair = require_same_value(lhs, rhs);
		return new B_XOR_expr(get_Bool_type(), val_pair.first, val_pair.second);
	}


	Expression* make_B_LSHIFT_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		auto lhs_val = convert_to_value(lhs);
		auto rhs_val = require_arithmetic(rhs);
		return new B_LSHIFT_expr(lhs->get_type(), lhs_val, rhs_val);
	}


	Expression* make_B_RSHIFT_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		auto lhs_val = convert_to_value(lhs);
		auto rhs_val = require_arithmetic(rhs);
		return new B_RSHIFT_expr(lhs->get_type(), lhs_val, rhs_val);
	}


	Expression* make_Addition_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		auto val_pair = require_same_arithmetic(lhs, rhs);
		return new Addition_expr(val_pair.first->get_type(), val_pair.first, val_pair.second);
	}


	Expression* make_Subtraction_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		auto val_pair = require_same_arithmetic(lhs, rhs);
		return new Subtraction_expr(val_pair.first->get_type(), val_pair.first, val_pair.second);
	}


	Expression* make_Multiplication_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		auto val_pair = require_same_arithmetic(lhs, rhs);
		return new Multiplication_expr(val_pair.first->get_type(), val_pair.first, val_pair.second);
	}


	Expression* make_Quotient_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		auto val_pair = require_same_arithmetic(lhs, rhs);
		return new Quotient_expr(val_pair.first->get_type(), val_pair.first, val_pair.second);
	}


	Expression* make_Modulo_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		auto val_pair = require_same_arithmetic(lhs, rhs);
		return new Modulo_expr(val_pair.first->get_type(), val_pair.first, val_pair.second);
	}


	Expression* make_L_AND_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		auto val_pair = require_same_value(lhs, rhs);
		return new L_AND_expr(get_Bool_type(), val_pair.first, val_pair.second);
	}


	Expression* make_L_OR_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		auto val_pair = require_same_value(lhs, rhs);
		return new L_OR_expr(get_Bool_type(), val_pair.first, val_pair.second);
	}


	Expression* make_EQUAL_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		auto val_pair = require_same_value(lhs, rhs);
		return new EQUAL_expr(get_Bool_type(), val_pair.first, val_pair.second);
	}


	Expression* make_NEQUAL_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		auto val_pair = require_same_value(lhs, rhs);
		return new NEQUAL_expr(get_Bool_type(), val_pair.first, val_pair.second);
	}


	Expression* make_LT_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		auto val_pair = require_same_value(lhs, rhs);
		return new LT_expr(get_Bool_type(), val_pair.first, val_pair.second);
	}


	Expression* make_GT_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		auto val_pair = require_same_value(lhs, rhs);
		return new GT_expr(get_Bool_type(), val_pair.first, val_pair.second);
	}


	Expression* make_LTOE_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		auto val_pair = require_same_value(lhs, rhs);
		return new LTOE_expr(get_Bool_type(), val_pair.first, val_pair.second);
	}


	Expression* make_GTOE_expr(Expression* lhs, Expression* rhs) {
		assert(lhs != nullptr);
		assert(rhs != nullptr);

		auto val_pair = require_same_value(lhs, rhs);
		return new GTOE_expr(get_Bool_type(), val_pair.first, val_pair.second);
	}

	/**** Ternary ****************/

	Expression* make_Conditional_expr(Expression* condition, Expression* true_expr, Expression* false_expr) {
		assert(condition != nullptr);
		assert(true_expr != nullptr);
		assert(false_expr != nullptr);

		auto condition_val = require_bool(condition);
		auto common_pair = require_common(true_expr, false_expr);
		return new Conditional_expr(condition_val->get_type(), condition_val, common_pair.first, common_pair.second);
	}

	/**** Multiary ***************/

	Expression* make_Call_expr(Expr_seq& exprs) {
		std::cout << "\n\nmake_Call_expr start\n";

		auto func_id_expr = require_function(exprs.front());
		auto fn_type = static_cast<Fun_type const*>(func_id_expr->get_type());

		// Quick reject for parameter/argument mismatch.
		if (fn_type->get_num_parameters() < exprs.size() - 1) {
			std::cerr << "function type parms: " << fn_type->get_num_parameters() << '\n';
			std::cerr << "call arguments: " << exprs.size() << '\n';
			throw std::runtime_error("too many arguments");
		}

		if (fn_type->get_num_parameters() > exprs.size() - 1) {
			std::cerr << "function type parms: " << fn_type->get_num_parameters() << '\n';
			std::cerr << "call arguments: " << exprs.size() << '\n';
			throw std::runtime_error("too few arguments");
		}


		for (size_t i = 1; i < exprs.size(); i++) {
			auto dummy = Variable_decl(Name{"__dummy"}, fn_type->get_parm_type(i - 1));
			std::cout << "dummy rdy" << '\n';
			copy_initialize(&dummy, exprs[i]);
			std::cerr << "cp init" << '\n';
			exprs[i] = dummy.get_initializer();
		}

		std::cout << "\n\nmake_Call_expr end\n";

		return new Call_expr(fn_type->get_ret_type(), std::move(exprs));
	}
}  // namespace Builder
