#pragma once

#include <algorithm>
#include <cassert>
#include <utility>


template <typename T, std::size_t N>
// clang-format off
template <typename... Args>  // clang-format on
Static_arity_node<T, N>::Static_arity_node(Args&&... args) : m_operands{std::move<Args>(args)...} {
	static_assert(sizeof...(args) == N);
	for (auto& op : m_operands) {
		assert(op != nullptr);
	}
}


template <typename T, std::size_t N>
constexpr std::size_t Static_arity_node<T, N>::get_arity() {
	return N;
}


template <typename T, std::size_t N>
T* Static_arity_node<T, N>::get_operand(std::size_t const i) {
	assert(0 <= i);
	assert(i < m_operands.size());


	return m_operands[i];
}


template <typename T, std::size_t N>
T const* Static_arity_node<T, N>::get_operand(std::size_t const i) const {
	assert(0 <= i);
	assert(i < m_operands.size());

	return m_operands[i];
}


template <typename T, std::size_t N>
T** Static_arity_node<T, N>::begin() {
	return m_operands.data();
}


template <typename T, std::size_t N>
T** Static_arity_node<T, N>::end() {
	return m_operands.data() + m_operands.size();
}


template <typename T, std::size_t N>
T* const* Static_arity_node<T, N>::begin() const {
	return m_operands.data();
}


template <typename T, std::size_t N>
T* const* Static_arity_node<T, N>::end() const {
	return m_operands.data() + m_operands.size();
}
