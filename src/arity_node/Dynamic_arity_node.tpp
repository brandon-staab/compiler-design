#pragma once

#include <cassert>


template <typename T>
Dynamic_arity_node<T>::Dynamic_arity_node(std::vector<T*> const& node_seq) : m_operands(node_seq) {
	for (auto& op : m_operands) {
		assert(op != nullptr);
	}
}


template <typename T>
Dynamic_arity_node<T>::Dynamic_arity_node(std::vector<T*>&& other) : m_operands(std::move(other)) {
	for (auto& op : m_operands) {
		assert(op != nullptr);
	}
}

template <typename T>
std::size_t Dynamic_arity_node<T>::get_arity() const {
	return m_operands.size();
}


template <typename T>
T* Dynamic_arity_node<T>::get_operand(std::size_t const i) {
	assert(0 <= i);
	assert(i < m_operands.size());

	return m_operands[i];
}


template <typename T>
T const* Dynamic_arity_node<T>::get_operand(std::size_t const i) const {
	assert(0 <= i);
	assert(i < m_operands.size());

	return m_operands[i];
}


template <typename T>
std::vector<T*>& Dynamic_arity_node<T>::get_operands() {
	return m_operands;
}


template <typename T>
std::vector<T*> const& Dynamic_arity_node<T>::get_operands() const {
	return m_operands;
}


template <typename T>
T** Dynamic_arity_node<T>::begin() {
	return m_operands.data();
}


template <typename T>
T** Dynamic_arity_node<T>::end() {
	return m_operands.data() + m_operands.size();
}


template <typename T>
T* const* Dynamic_arity_node<T>::begin() const {
	return m_operands.data();
}


template <typename T>
T* const* Dynamic_arity_node<T>::end() const {
	return m_operands.data() + m_operands.size();
}
