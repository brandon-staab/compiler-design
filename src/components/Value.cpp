#include "components/Value.hpp"

#include "declarations/decl_incl.hpp"

#include <cassert>
#include <iostream>


Value::Value(bool const b) : m_kind(Value::Kind::bool_val), m_data(b) {}


Value::Value(char const c) : m_kind(Value::Kind::char_val), m_data(c) {}


Value::Value(int const i) : m_kind(Value::Kind::int_val), m_data(i) {}


Value::Value(unsigned int const u) : m_kind(Value::Kind::uint_val), m_data(u) {}


Value::Value(float const f) : m_kind(Value::Kind::float_val), m_data(f) {}


Value::Value(double const d) : m_kind(Value::Kind::double_val), m_data(d) {}


Value::Value(Address const& a) : m_kind(Value::Kind::address_val), m_data(a) {}

/*****************************/

Value::Kind Value::get_kind() const {
	return m_kind;
}

/*****************************/

bool Value::is_indeterminate() const {
	return m_kind == Kind::non_val;
}


bool Value::is_bool() const {
	return get_kind() == Value::Kind::bool_val;
}


bool Value::is_char() const {
	return get_kind() == Value::Kind::char_val;
}


bool Value::is_int() const {
	return get_kind() == Value::Kind::int_val;
}


bool Value::is_uint() const {
	return get_kind() == Value::Kind::uint_val;
}


bool Value::is_float() const {
	return get_kind() == Value::Kind::float_val;
}


bool Value::is_double() const {
	return get_kind() == Value::Kind::double_val;
}


bool Value::is_address() const {
	return get_kind() == Value::Kind::address_val;
}

/*****************************/

void Value::set_bool(const bool b) {
	m_kind = Kind::bool_val;
	m_data.b = b;
}


void Value::set_char(const char c) {
	m_kind = Kind::char_val;
	m_data.c = c;
}


void Value::set_int(const int i) {
	m_kind = Kind::int_val;
	m_data.i = i;
}


void Value::set_uint(const unsigned int u) {
	m_kind = Kind::int_val;
	m_data.u = u;
}


void Value::set_float(const float f) {
	m_kind = Kind::float_val;
	m_data.f = f;
}


void Value::set_double(const double d) {
	m_kind = Kind::double_val;
	m_data.d = d;
}


void Value::set_address(Address const& a) {
	m_kind = Kind::address_val;
	m_data.a = a;
}

/*****************************/

bool Value::get_bool() const {
	assert(is_bool());

	return m_data.b;
}


char Value::get_char() const {
	assert(is_char());

	return m_data.c;
}


int Value::get_int() const {
	assert(is_int());

	return m_data.i;
}


unsigned int Value::get_uint() const {
	assert(is_uint());

	return m_data.u;
}


float Value::get_float() const {
	assert(is_float());

	return m_data.f;
}


double Value::get_double() const {
	assert(is_double());

	return m_data.d;
}


Address Value::get_address() const {
	assert(is_address());

	return m_data.a;
}

/*****************************/

Value::Data::Data(bool const b) : b(b) {}


Value::Data::Data(char const c) : c(c) {}


Value::Data::Data(int const i) : i(i) {}


Value::Data::Data(unsigned int const u) : u(u) {}


Value::Data::Data(float const f) : f(f) {}


Value::Data::Data(double const d) : d(d) {}


Value::Data::Data(Address const& a) : a(a) {}

/*************************************************************/

std::ostream& operator<<(std::ostream& os, Value const& val) {
	switch (val.get_kind()) {
		case Value::Kind::non_val:
			return os << "<indeterminate>";
		case Value::Kind::bool_val:
			return os << val.get_bool();
		case Value::Kind::char_val:
			return os << val.get_char();
		case Value::Kind::int_val:
			return os << val.get_int();
		case Value::Kind::uint_val:
			return os << val.get_uint();
		case Value::Kind::float_val:
			return os << val.get_float();
		case Value::Kind::double_val:
			return os << val.get_double();
		case Value::Kind::address_val:
			Address const& addr = val.get_address();
			os << '<';
			if (addr.store == -1) {
				os << "static";
			} else {
				os << "auto(" << addr.store << ')';
			}
			os << ' ' << addr.def->get_name() << ' ' << addr.def << '>';
			return os << val.get_double();
	}

	assert(false);	// All value kinds must be defined
}
