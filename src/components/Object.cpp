#include "components/Object.hpp"


Object::Object(Type const* const type) : m_type(type), m_value() {
	assert(m_type != nullptr);
	assert(!m_type->is_void());
}


Object::Object(Type const* const type, Value const& val) : m_type(type), m_value(val) {
	assert(m_type != nullptr);
	assert(!m_type->is_void());
}


Object::Object(Type const* const type, Value&& val) : m_type(type), m_value(std::move(val)) {
	assert(m_type != nullptr);
	assert(!m_type->is_void());
}


Type const* get_type() const {
	assert(m_type != nullptr);

	return m_type;
}


bool is_initialized() const {
	return !m_value.is_indeterminate();
}


bool is_uninitialized() const {
	return m_value.is_indeterminate();
}


Value const& load() const {
	assert(is_initialized());

	return m_value;
}

void initialize(Value const& val) {
	assert(val.is_initialized());
	assert(is_uninitialized());

	m_value = val;
}


void initialize(Value&& val) {
	assert(val.is_initialized());
	assert(is_uninitialized());

	m_value = std::move(val);
}


void store(Value const& val) {
	assert(is_initialized());

	m_value = val;
}


void store(Value&& val) {
	assert(is_initialized());

	m_value = std::move(val);
}
