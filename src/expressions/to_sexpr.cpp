#include "expressions/expr_incl.hpp"

#include <iostream>


namespace {
	template <std::size_t N>
	void to_sexpr_arity_operands(std::ostream& os, Fixed_Arity_expr<N> const* const expr) {
		for (auto op = expr->begin(); op != expr->end();) {
			to_sexpr(os, *op);

			if (++op != expr->end()) {
				os << ' ';
			}
		}
	}


	void to_sexpr_Call_expr(std::ostream& os, Call_expr const* const expr) {
		os << expr->get_calle().to_string() << " (";
		for (auto op = expr->begin(); op != expr->end();) {
			print(os, *op);

			if (++op != expr->end()) {
				os << ", ";
			}
		}
		os << ')';
	}
}  // namespace


void to_sexpr(std::ostream& os, Expression const* const expr) {
	assert(expr != nullptr);

	switch (expr->get_kind()) {
		case Expression::Kind::identifier_expr:
		case Expression::Kind::bool_expr:
		case Expression::Kind::char_expr:
		case Expression::Kind::int_expr:
		case Expression::Kind::uint_expr:
		case Expression::Kind::float_expr:
		case Expression::Kind::double_expr:
			os << '(' << static_cast<Nullary_expr const* const>(expr)->to_string() << ')';
			return;

		case Expression::Kind::convert_value_expr:
		case Expression::Kind::pre_decrement_expr:
		case Expression::Kind::pre_increment_expr:
		case Expression::Kind::minus_expr:
		case Expression::Kind::b_not_expr:
		case Expression::Kind::reciprocal_expr:
		case Expression::Kind::l_not_expr:
			os << '(' << kind_to_symbol(expr->get_kind()) << ' ';
			to_sexpr(os, static_cast<Fixed_Arity_expr<1> const* const>(expr)->get_operand(0));
			os << ")";
			return;

		case Expression::Kind::post_increment_expr:
		case Expression::Kind::post_decrement_expr:
			os << "(";
			to_sexpr(os, static_cast<Fixed_Arity_expr<1> const* const>(expr)->get_operand(0));
			os << ' ' << kind_to_symbol(expr->get_kind()) << ')';
			return;

		case Expression::Kind::assignment_expr:
		case Expression::Kind::addition_assignment_expr:
		case Expression::Kind::subtraction_assignment_expr:
		case Expression::Kind::multiplication_assignment_expr:
		case Expression::Kind::quotient_assignment_expr:
		case Expression::Kind::modulo_assignment_expr:
		case Expression::Kind::b_and_assignment_expr:
		case Expression::Kind::b_or_assignment_expr:
		case Expression::Kind::b_xor_assignment_expr:
		case Expression::Kind::b_lshift_assignment_expr:
		case Expression::Kind::b_rshift_assignment_expr:
		case Expression::Kind::b_and_expr:
		case Expression::Kind::b_or_expr:
		case Expression::Kind::b_xor_expr:
		case Expression::Kind::b_lshift_expr:
		case Expression::Kind::b_rshift_expr:
		case Expression::Kind::addition_expr:
		case Expression::Kind::subtraction_expr:
		case Expression::Kind::multiplication_expr:
		case Expression::Kind::quotient_expr:
		case Expression::Kind::modulo_expr:
		case Expression::Kind::l_and_expr:
		case Expression::Kind::l_or_expr:
		case Expression::Kind::equal_expr:
		case Expression::Kind::nequal_expr:
		case Expression::Kind::lt_expr:
		case Expression::Kind::gt_expr:
		case Expression::Kind::ltoe_expr:
		case Expression::Kind::gtoe_expr:
			os << '(' << kind_to_symbol(expr->get_kind()) << ' ';
			to_sexpr_arity_operands(os, static_cast<Fixed_Arity_expr<2> const* const>(expr));
			os << ")";
			return;

		case Expression::Kind::conditional_expr:
			os << '(' << kind_to_symbol(expr->get_kind()) << ' ';
			to_sexpr_arity_operands(os, static_cast<Fixed_Arity_expr<3> const* const>(expr));
			os << ")";
			return;

		case Expression::Kind::call_expr:
			os << '(';
			to_sexpr_Call_expr(os, static_cast<Call_expr const* const>(expr));
			os << ")";
			return;
	}

	assert(false);	// All expressions must be defined
}
