#include "expressions/Binary_expr.hpp"


Binary_expr::Binary_expr(Kind const kind, Type const* const type, Expression* const lhs, Expression* const rhs) : Fixed_Arity_expr(kind, type, lhs, rhs) {}

/*************************************************************/

Assignment_binaryexpr::Assignment_binaryexpr(Kind const kind, Type const* const type, Expression* const lhs, Expression* const rhs)
	: Binary_expr(kind, type, lhs, rhs) {}


Arithmetic_binaryexpr::Arithmetic_binaryexpr(Kind const kind, Type const* const type, Expression* const lhs, Expression* const rhs)
	: Binary_expr(kind, type, lhs, rhs) {}


Logical_binaryexpr::Logical_binaryexpr(Kind const kind, Type const* const type, Expression* const lhs, Expression* const rhs)
	: Binary_expr(kind, type, lhs, rhs) {}


Comparison_binaryexpr::Comparison_binaryexpr(Kind const kind, Type const* const type, Expression* const lhs, Expression* const rhs)
	: Binary_expr(kind, type, lhs, rhs) {}

/*************************************************************/

Assignment_expr::Assignment_expr(Type const* const type, Expression* const lhs, Expression* const rhs)
	: Assignment_binaryexpr(Kind::assignment_expr, type, lhs, rhs) {}


Addition_Assignment_expr::Addition_Assignment_expr(Type const* const type, Expression* const lhs, Expression* const rhs)
	: Assignment_binaryexpr(Kind::addition_assignment_expr, type, lhs, rhs) {}


Subtraction_Assignment_expr::Subtraction_Assignment_expr(Type const* const type, Expression* const lhs, Expression* const rhs)
	: Assignment_binaryexpr(Kind::subtraction_assignment_expr, type, lhs, rhs) {}


Multiplication_Assignment_expr::Multiplication_Assignment_expr(Type const* const type, Expression* const lhs, Expression* const rhs)
	: Assignment_binaryexpr(Kind::multiplication_assignment_expr, type, lhs, rhs) {}


Quotient_Assignment_expr::Quotient_Assignment_expr(Type const* const type, Expression* const lhs, Expression* const rhs)
	: Assignment_binaryexpr(Kind::quotient_assignment_expr, type, lhs, rhs) {}


Modulo_Assignment_expr::Modulo_Assignment_expr(Type const* const type, Expression* const lhs, Expression* const rhs)
	: Assignment_binaryexpr(Kind::modulo_assignment_expr, type, lhs, rhs) {}


B_AND_Assignment_expr::B_AND_Assignment_expr(Type const* const type, Expression* const lhs, Expression* const rhs)
	: Assignment_binaryexpr(Kind::b_and_assignment_expr, type, lhs, rhs) {}


B_OR_Assignment_expr::B_OR_Assignment_expr(Type const* const type, Expression* const lhs, Expression* const rhs)
	: Assignment_binaryexpr(Kind::b_or_assignment_expr, type, lhs, rhs) {}


B_XOR_Assignment_expr::B_XOR_Assignment_expr(Type const* const type, Expression* const lhs, Expression* const rhs)
	: Assignment_binaryexpr(Kind::b_xor_assignment_expr, type, lhs, rhs) {}


B_LSHIFT_Assignment_expr::B_LSHIFT_Assignment_expr(Type const* const type, Expression* const lhs, Expression* const rhs)
	: Assignment_binaryexpr(Kind::b_lshift_assignment_expr, type, lhs, rhs) {}


B_RSHIFT_Assignment_expr::B_RSHIFT_Assignment_expr(Type const* const type, Expression* const lhs, Expression* const rhs)
	: Assignment_binaryexpr(Kind::b_rshift_assignment_expr, type, lhs, rhs) {}

/*****************************/

B_AND_expr::B_AND_expr(Type const* const type, Expression* const lhs, Expression* const rhs) : Arithmetic_binaryexpr(Kind::b_and_expr, type, lhs, rhs) {}


B_OR_expr::B_OR_expr(Type const* const type, Expression* const lhs, Expression* const rhs) : Arithmetic_binaryexpr(Kind::b_or_expr, type, lhs, rhs) {}


B_XOR_expr::B_XOR_expr(Type const* const type, Expression* const lhs, Expression* const rhs) : Arithmetic_binaryexpr(Kind::b_xor_expr, type, lhs, rhs) {}


B_LSHIFT_expr::B_LSHIFT_expr(Type const* const type, Expression* const lhs, Expression* const rhs)
	: Arithmetic_binaryexpr(Kind::b_lshift_expr, type, lhs, rhs) {}


B_RSHIFT_expr::B_RSHIFT_expr(Type const* const type, Expression* const lhs, Expression* const rhs)
	: Arithmetic_binaryexpr(Kind::b_rshift_expr, type, lhs, rhs) {}


Addition_expr::Addition_expr(Type const* const type, Expression* const lhs, Expression* const rhs)
	: Arithmetic_binaryexpr(Kind::addition_expr, type, lhs, rhs) {}


Subtraction_expr::Subtraction_expr(Type const* const type, Expression* const lhs, Expression* const rhs)
	: Arithmetic_binaryexpr(Kind::subtraction_expr, type, lhs, rhs) {}


Multiplication_expr::Multiplication_expr(Type const* const type, Expression* const lhs, Expression* const rhs)
	: Arithmetic_binaryexpr(Kind::multiplication_expr, type, lhs, rhs) {}


Quotient_expr::Quotient_expr(Type const* const type, Expression* const lhs, Expression* const rhs)
	: Arithmetic_binaryexpr(Kind::quotient_expr, type, lhs, rhs) {}


Modulo_expr::Modulo_expr(Type const* const type, Expression* const lhs, Expression* const rhs) : Arithmetic_binaryexpr(Kind::modulo_expr, type, lhs, rhs) {}

/*****************************/

L_AND_expr::L_AND_expr(Type const* const type, Expression* const lhs, Expression* const rhs) : Logical_binaryexpr(Kind::l_and_expr, type, lhs, rhs) {}


L_OR_expr::L_OR_expr(Type const* const type, Expression* const lhs, Expression* const rhs) : Logical_binaryexpr(Kind::l_or_expr, type, lhs, rhs) {}


EQUAL_expr::EQUAL_expr(Type const* const type, Expression* const lhs, Expression* const rhs) : Comparison_binaryexpr(Kind::equal_expr, type, lhs, rhs) {}


NEQUAL_expr::NEQUAL_expr(Type const* const type, Expression* const lhs, Expression* const rhs) : Comparison_binaryexpr(Kind::nequal_expr, type, lhs, rhs) {}


LT_expr::LT_expr(Type const* const type, Expression* const lhs, Expression* const rhs) : Comparison_binaryexpr(Kind::lt_expr, type, lhs, rhs) {}


GT_expr::GT_expr(Type const* const type, Expression* const lhs, Expression* const rhs) : Comparison_binaryexpr(Kind::gt_expr, type, lhs, rhs) {}


LTOE_expr::LTOE_expr(Type const* const type, Expression* const lhs, Expression* const rhs) : Comparison_binaryexpr(Kind::ltoe_expr, type, lhs, rhs) {}


GTOE_expr::GTOE_expr(Type const* const type, Expression* const lhs, Expression* const rhs) : Comparison_binaryexpr(Kind::gtoe_expr, type, lhs, rhs) {}
