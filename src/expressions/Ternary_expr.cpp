#include "expressions/Ternary_expr.hpp"


Ternary_expr::Ternary_expr(Kind const kind, Type const* const type, Expression* const first, Expression* const second, Expression* const third)
	: Fixed_Arity_expr(kind, type, first, second, third) {}

/*************************************************************/

Conditional_expr::Conditional_expr(Type const* const type, Expression* const condition, Expression* const true_expr, Expression* const false_expr)
	: Ternary_expr(Kind::conditional_expr, type, condition, true_expr, false_expr) {}


Expression const* const Conditional_expr::get_condition() const {
	assert(get_operand(0) != nullptr);

	return get_operand(0);
}


Expression const* const Conditional_expr::get_true_expr() const {
	assert(get_operand(1) != nullptr);

	return get_operand(1);
}


Expression const* const Conditional_expr::get_false_expr() const {
	assert(get_operand(2) != nullptr);

	return get_operand(2);
}
