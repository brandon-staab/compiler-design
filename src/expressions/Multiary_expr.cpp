#include "expressions/Multiary_expr.hpp"

#include "expressions/Nullary_expr.hpp"


Multiary_expr::Multiary_expr(Kind const kind, Type const* const type, Expr_seq&& operands) : Expression(kind, type), Dynamic_arity_node(std::move(operands)) {}

/*************************************************************/

Call_expr::Call_expr(Type const* const type, Expr_seq&& operands) : Multiary_expr(Kind::call_expr, type, std::move(operands)) {}

Identifier_expr const& Call_expr::get_calle() const {
	assert(get_operand(0) != nullptr);

	return *static_cast<Identifier_expr const* const>(get_operand(0));
}


Expression const& Call_expr::get_parameter(std::size_t const i) const {
	assert(get_operand(i + 1) != nullptr);

	return *get_operand(i + 1);
}
