#include "expressions/expr_incl.hpp"

#include "utilities/insertIndent.hpp"

#include <iostream>


namespace {
	template <std::size_t N>
	void dump_debug_arity_operands(std::ostream& os, Fixed_Arity_expr<N> const* const expr, std::size_t const depth) {
		for (auto const& op : *expr) {
			dump_debug(os, op, depth);
		}
	}


	void dump_debug_Call_expr(std::ostream& os, Call_expr const* const expr, std::size_t const depth) {
		os << expr->get_calle().to_string() << " (";
		for (auto const& op : *expr) {
			dump_debug(os, op, depth);
		}
		os << ')';
	}
}  // namespace


void dump_debug(std::ostream& os, Expression const* const expr, std::size_t depth) {
	assert(expr != nullptr);
	assert(depth < 256);

	os << '\n' << &expr;
	insertIndent(os, ++depth);

	switch (expr->get_kind()) {
		case Expression::Kind::identifier_expr:
		case Expression::Kind::bool_expr:
		case Expression::Kind::char_expr:
		case Expression::Kind::int_expr:
		case Expression::Kind::uint_expr:
		case Expression::Kind::float_expr:
		case Expression::Kind::double_expr:
			os << static_cast<Nullary_expr const* const>(expr)->to_string();
			return;

		case Expression::Kind::convert_value_expr:
		case Expression::Kind::pre_increment_expr:
		case Expression::Kind::post_increment_expr:
		case Expression::Kind::pre_decrement_expr:
		case Expression::Kind::post_decrement_expr:
		case Expression::Kind::minus_expr:
		case Expression::Kind::b_not_expr:
		case Expression::Kind::reciprocal_expr:
		case Expression::Kind::l_not_expr:
			os << expr->get_kind();
			return dump_debug(os, static_cast<Fixed_Arity_expr<1> const* const>(expr)->get_operand(0), depth);

		case Expression::Kind::assignment_expr:
		case Expression::Kind::addition_assignment_expr:
		case Expression::Kind::subtraction_assignment_expr:
		case Expression::Kind::multiplication_assignment_expr:
		case Expression::Kind::quotient_assignment_expr:
		case Expression::Kind::modulo_assignment_expr:
		case Expression::Kind::b_and_assignment_expr:
		case Expression::Kind::b_or_assignment_expr:
		case Expression::Kind::b_xor_assignment_expr:
		case Expression::Kind::b_lshift_assignment_expr:
		case Expression::Kind::b_rshift_assignment_expr:
		case Expression::Kind::b_and_expr:
		case Expression::Kind::b_or_expr:
		case Expression::Kind::b_xor_expr:
		case Expression::Kind::b_lshift_expr:
		case Expression::Kind::b_rshift_expr:
		case Expression::Kind::addition_expr:
		case Expression::Kind::subtraction_expr:
		case Expression::Kind::multiplication_expr:
		case Expression::Kind::quotient_expr:
		case Expression::Kind::modulo_expr:
		case Expression::Kind::l_and_expr:
		case Expression::Kind::l_or_expr:
		case Expression::Kind::equal_expr:
		case Expression::Kind::nequal_expr:
		case Expression::Kind::lt_expr:
		case Expression::Kind::gt_expr:
		case Expression::Kind::ltoe_expr:
		case Expression::Kind::gtoe_expr:
			os << expr->get_kind();
			return dump_debug_arity_operands(os, static_cast<Fixed_Arity_expr<2> const* const>(expr), depth);

		case Expression::Kind::conditional_expr:
			os << expr->get_kind();
			return dump_debug_arity_operands(os, static_cast<Fixed_Arity_expr<3> const* const>(expr), depth);

		case Expression::Kind::call_expr:
			return dump_debug_Call_expr(os, static_cast<Call_expr const* const>(expr), depth);
	}

	assert(false);	// All expressions must be defined
}
