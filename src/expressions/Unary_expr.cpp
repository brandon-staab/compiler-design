#include "expressions/Unary_expr.hpp"


Unary_expr::Unary_expr(Kind const kind, Type const* const type, Expression* const operand) : Fixed_Arity_expr(kind, type, operand) {}


Incdec_unaryexpr::Incdec_unaryexpr(Kind const kind, Type const* const type, Expression* const operand) : Unary_expr(kind, type, operand) {}


Arithmetic_unaryexpr::Arithmetic_unaryexpr(Kind const kind, Type const* const type, Expression* const operand) : Unary_expr(kind, type, operand) {}


Logical_unaryexpr::Logical_unaryexpr(Kind const kind, Type const* const type, Expression* const operand) : Unary_expr(kind, type, operand) {}

/*************************************************************/

Convert_Value_expr::Convert_Value_expr(Type const* const type, Expression* const operand) : Unary_expr(Kind::convert_value_expr, type, operand) {}


Expression* const Convert_Value_expr::get_source() {
	assert(get_operand(0) != nullptr);

	return get_operand(0);
}


Expression const* const Convert_Value_expr::get_source() const {
	assert(get_operand(0) != nullptr);

	return get_operand(0);
}

/*****************************/

pre_Increment_expr::pre_Increment_expr(Type const* const type, Expression* const operand) : Incdec_unaryexpr(Kind::pre_increment_expr, type, operand) {}


post_Increment_expr::post_Increment_expr(Type const* const type, Expression* const operand) : Incdec_unaryexpr(Kind::post_increment_expr, type, operand) {}


pre_Decrement_expr::pre_Decrement_expr(Type const* const type, Expression* const operand) : Incdec_unaryexpr(Kind::pre_decrement_expr, type, operand) {}


post_Decrement_expr::post_Decrement_expr(Type const* const type, Expression* const operand) : Incdec_unaryexpr(Kind::post_decrement_expr, type, operand) {}

/*****************************/

Minus_expr::Minus_expr(Type const* const type, Expression* const operand) : Arithmetic_unaryexpr(Kind::minus_expr, type, operand) {}


B_NOT_expr::B_NOT_expr(Type const* const type, Expression* const operand) : Arithmetic_unaryexpr(Kind::b_not_expr, type, operand) {}


Reciprocal_expr::Reciprocal_expr(Type const* const type, Expression* const operand) : Arithmetic_unaryexpr(Kind::reciprocal_expr, type, operand) {}


L_NOT_expr::L_NOT_expr(Type const* const type, Expression* const operand) : Logical_unaryexpr(Kind::l_not_expr, type, operand) {}
