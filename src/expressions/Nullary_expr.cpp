#include "expressions/Nullary_expr.hpp"


Nullary_expr::Nullary_expr(Kind const kind, Type const* const type) : Fixed_Arity_expr(kind, type) {}

/*****************************/

Literal_expr::Literal_expr(Kind const kind, Type const* const type, Value&& value) : Nullary_expr(kind, type), m_value(std::move(value)) {}

/*************************************************************/

Identifier_expr::Identifier_expr(Type const* const type, Declaration const* const decl) : Nullary_expr(Kind::identifier_expr, type), m_decl(decl) {
	assert(decl != nullptr);
}


void Identifier_expr::set_decl(Declaration const* const decl) {
	assert(decl != nullptr);

	m_decl = decl;
}


std::string Identifier_expr::to_string() const {
	assert(m_decl != nullptr);

	return m_decl->get_name();
}

/*****************************/

Bool_expr::Bool_expr(Type const* const type, bool const val) : Literal_expr(Kind::bool_expr, type, Value(val)) {}


std::string Bool_expr::to_string() const {
	return m_value.get_bool() ? "true" : "false";
}

/*****************************/

Char_expr::Char_expr(Type const* const type, char const val) : Literal_expr(Kind::char_expr, type, Value(val)) {}


std::string Char_expr::to_string() const {
	return std::string(1, m_value.get_char());
}

/*****************************/

Int_expr::Int_expr(Type const* const type, int const val) : Literal_expr(Kind::int_expr, type, Value(val)) {}


std::string Int_expr::to_string() const {
	return std::to_string(m_value.get_int());
}

/*****************************/

UInt_expr::UInt_expr(Type const* const type, unsigned int const val) : Literal_expr(Kind::uint_expr, type, Value(val)) {}


std::string UInt_expr::to_string() const {
	return std::to_string(m_value.get_uint());
}

/*****************************/

Float_expr::Float_expr(Type const* const type, float const val) : Literal_expr(Kind::float_expr, type, Value(val)) {}


std::string Float_expr::to_string() const {
	return std::to_string(m_value.get_float());
}

/*****************************/

Double_expr::Double_expr(Type const* const type, double const val) : Literal_expr(Kind::double_expr, type, Value(val)) {}


std::string Double_expr::to_string() const {
	return std::to_string(m_value.get_double());
}
