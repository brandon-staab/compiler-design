#include "expressions/Expression.hpp"

#include <iostream>


Expression::Expression(Kind const kind, Type const* type) : m_kind(kind), m_type(type) {}


Expression::Kind Expression::get_kind() const {
	return m_kind;
}


Type const* const Expression::get_type() const {
	assert(m_type != nullptr);

	return m_type;
}

/*************************************************************/

std::ostream& operator<<(std::ostream& os, Expression const& expr) {
	print(os, &expr);
	return os;
}


std::ostream& operator<<(std::ostream& os, Expression::Kind const kind) {
	return os << kind_to_string(kind);
}

/*****************************/

char const* const kind_to_string(Expression::Kind const kind) {
	switch (kind) {
		case Expression::Kind::identifier_expr:
			return "identifier_expr";
		case Expression::Kind::bool_expr:
			return "bool_expr";
		case Expression::Kind::char_expr:
			return "char_expr";
		case Expression::Kind::int_expr:
			return "int_expr";
		case Expression::Kind::uint_expr:
			return "uint_expr";
		case Expression::Kind::float_expr:
			return "float_expr";
		case Expression::Kind::double_expr:
			return "double_expr";
		case Expression::Kind::convert_value_expr:
			return "convert_value_expr";
		case Expression::Kind::pre_increment_expr:
			return "pre_increment_expr";
		case Expression::Kind::post_increment_expr:
			return "post_increment_expr";
		case Expression::Kind::pre_decrement_expr:
			return "pre_decrement_expr";
		case Expression::Kind::post_decrement_expr:
			return "post_decrement_expr";
		case Expression::Kind::minus_expr:
			return "minus_expr";
		case Expression::Kind::b_not_expr:
			return "b_not_expr";
		case Expression::Kind::reciprocal_expr:
			return "reciprocal_expr";
		case Expression::Kind::l_not_expr:
			return "l_not_expr";
		case Expression::Kind::assignment_expr:
			return "assignment_expr";
		case Expression::Kind::addition_assignment_expr:
			return "addition_assignment_expr";
		case Expression::Kind::subtraction_assignment_expr:
			return "subtraction_assignment_expr";
		case Expression::Kind::multiplication_assignment_expr:
			return "multiplication_assignment_expr";
		case Expression::Kind::quotient_assignment_expr:
			return "quotient_assignment_expr";
		case Expression::Kind::modulo_assignment_expr:
			return "modulo_assignment_expr";
		case Expression::Kind::b_and_assignment_expr:
			return "b_and_assignment_expr";
		case Expression::Kind::b_or_assignment_expr:
			return "b_or_assignment_expr";
		case Expression::Kind::b_xor_assignment_expr:
			return "b_xor_assignment_expr";
		case Expression::Kind::b_lshift_assignment_expr:
			return "b_lshift_assignment_expr";
		case Expression::Kind::b_rshift_assignment_expr:
			return "b_rshift_assignment_expr";
		case Expression::Kind::b_and_expr:
			return "b_and_expr";
		case Expression::Kind::b_or_expr:
			return "b_or_expr";
		case Expression::Kind::b_xor_expr:
			return "b_xor_expr";
		case Expression::Kind::b_lshift_expr:
			return "b_lshift_expr";
		case Expression::Kind::b_rshift_expr:
			return "b_rshift_expr";
		case Expression::Kind::addition_expr:
			return "addition_expr";
		case Expression::Kind::subtraction_expr:
			return "subtraction_expr";
		case Expression::Kind::multiplication_expr:
			return "multiplication_expr";
		case Expression::Kind::quotient_expr:
			return "quotient_expr";
		case Expression::Kind::modulo_expr:
			return "modulo_expr";
		case Expression::Kind::l_and_expr:
			return "l_and_expr";
		case Expression::Kind::l_or_expr:
			return "l_or_expr";
		case Expression::Kind::equal_expr:
			return "equal_expr";
		case Expression::Kind::nequal_expr:
			return "nequal_expr";
		case Expression::Kind::lt_expr:
			return "lt_expr";
		case Expression::Kind::gt_expr:
			return "gt_expr";
		case Expression::Kind::ltoe_expr:
			return "ltoe_expr";
		case Expression::Kind::gtoe_expr:
			return "gtoe_expr";
		case Expression::Kind::conditional_expr:
			return "conditional_expr";
		case Expression::Kind::call_expr:
			return "call_expr";
	}

	assert(false);	// All expression kinds must be defined
}


char const* const kind_to_symbol(Expression::Kind const kind) {
	switch (kind) {
		case Expression::Kind::identifier_expr:
			return "iden:";
		case Expression::Kind::bool_expr:
			return "bool:";
		case Expression::Kind::char_expr:
			return "char:";
		case Expression::Kind::int_expr:
			return "int:";
		case Expression::Kind::uint_expr:
			return "uint:";
		case Expression::Kind::float_expr:
			return "float:";
		case Expression::Kind::double_expr:
			return "double:";
		case Expression::Kind::convert_value_expr:
			return "val:";
		case Expression::Kind::pre_increment_expr:
			return "++";
		case Expression::Kind::post_increment_expr:
			return "++";
		case Expression::Kind::pre_decrement_expr:
			return "--";
		case Expression::Kind::post_decrement_expr:
			return "--";
		case Expression::Kind::minus_expr:
			return "-";
		case Expression::Kind::b_not_expr:
			return "~";
		case Expression::Kind::reciprocal_expr:
			return "/";
		case Expression::Kind::l_not_expr:
			return "!";
		case Expression::Kind::assignment_expr:
			return "=";
		case Expression::Kind::addition_assignment_expr:
			return "+=";
		case Expression::Kind::subtraction_assignment_expr:
			return "-=";
		case Expression::Kind::multiplication_assignment_expr:
			return "*=";
		case Expression::Kind::quotient_assignment_expr:
			return "/=";
		case Expression::Kind::modulo_assignment_expr:
			return "%=";
		case Expression::Kind::b_and_assignment_expr:
			return "&=";
		case Expression::Kind::b_or_assignment_expr:
			return "|=";
		case Expression::Kind::b_xor_assignment_expr:
			return "^=";
		case Expression::Kind::b_lshift_assignment_expr:
			return "<<=";
		case Expression::Kind::b_rshift_assignment_expr:
			return ">>=";
		case Expression::Kind::b_and_expr:
			return "&";
		case Expression::Kind::b_or_expr:
			return "|";
		case Expression::Kind::b_xor_expr:
			return "^";
		case Expression::Kind::b_lshift_expr:
			return "<<";
		case Expression::Kind::b_rshift_expr:
			return ">>";
		case Expression::Kind::addition_expr:
			return "+";
		case Expression::Kind::subtraction_expr:
			return "-";
		case Expression::Kind::multiplication_expr:
			return "*";
		case Expression::Kind::quotient_expr:
			return "/";
		case Expression::Kind::modulo_expr:
			return "%";
		case Expression::Kind::l_and_expr:
			return "&&";
		case Expression::Kind::l_or_expr:
			return "||";
		case Expression::Kind::equal_expr:
			return "==";
		case Expression::Kind::nequal_expr:
			return "!=";
		case Expression::Kind::lt_expr:
			return "<";
		case Expression::Kind::gt_expr:
			return ">";
		case Expression::Kind::ltoe_expr:
			return "<=";
		case Expression::Kind::gtoe_expr:
			return ">=";
		case Expression::Kind::conditional_expr:
			return "?";
		case Expression::Kind::call_expr:
			return "call:";
	}

	assert(false);	// All expression kinds must be defined
}
