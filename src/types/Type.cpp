#include "types/Type.hpp"

#include "types/type_incl.hpp"

#include <iostream>


Type::Type(Kind const kind) : m_kind(kind) {}


Type::Kind Type::get_kind() const {
	return m_kind;
}


bool Type::is_bool() const {
	return get_kind() == Kind::bool_type;
}


bool Type::is_char() const {
	return get_kind() == Kind::char_type;
}


bool Type::is_int() const {
	return get_kind() == Kind::int_type;
}


bool Type::is_uint() const {
	return get_kind() == Kind::uint_type;
}


bool Type::is_float() const {
	return get_kind() == Kind::float_type;
}


bool Type::is_double() const {
	return get_kind() == Kind::double_type;
}


bool Type::is_reference() const {
	return get_kind() == Kind::ref_type;
}


bool Type::is_function() const {
	return get_kind() == Kind::fun_type;
}


bool Type::is_primitive() const {
	return is_variable() && is_object();  // FIXME: classes?
}


bool Type::is_object() const {
	return !is_reference();
}


bool Type::is_variable() const {
	return get_kind() != Kind::fun_type;
}


bool Type::is_integral() const {
	return is_bool() || is_char() || is_int() || is_uint();
}


bool Type::is_arithmetic() const {
	return is_char() || is_int() || is_uint() || is_float() || is_double();
}


bool Type::is_reference_to(Type const* const that) const {
	assert(that != nullptr);

	return is_reference() && (static_cast<Ref_type const* const>(this)->get_referent_type() == that);
}

/*************************************************************/

static bool is_equal_ref(Ref_type const* const a, Ref_type const* const b) {
	assert(a != nullptr);
	assert(b != nullptr);

	return is_equal(a->get_referent_type(), b->get_referent_type());
}


static bool is_equal_fun(Fun_type const* const a, Fun_type const* const b) {
	assert(a != nullptr);
	assert(b != nullptr);

	return std::equal(a->begin(), a->end(), b->begin(), b->end());
}

/*************************************************************/

std::ostream& operator<<(std::ostream& os, Type const& type) {
	print(os, &type);
	return os;
}


std::ostream& operator<<(std::ostream& os, Type::Kind const kind) {
	return os << kind_to_string(kind);
}


bool operator==(Type const& lhs, Type const& rhs) {
	return is_equal(&lhs, &rhs);
}


bool operator!=(Type const& lhs, Type const& rhs) {
	return !(lhs == rhs);
}


bool operator<(Type const& lhs, Type const& rhs) {
	return &lhs < &rhs;
}

/*****************************/

bool is_equal(Type const* const a, Type const* const b) {
	assert(a != nullptr);
	assert(b != nullptr);


	if (a->get_kind() != b->get_kind()) {
		return false;
	}

	switch (a->get_kind()) {
		case Type::Kind::bool_type:
		case Type::Kind::char_type:
		case Type::Kind::int_type:
		case Type::Kind::uint_type:
		case Type::Kind::float_type:
		case Type::Kind::double_type:
			return true;

		case Type::Kind::ref_type:
			return is_equal_ref(static_cast<Ref_type const* const>(a), static_cast<Ref_type const* const>(b));

		case Type::Kind::fun_type:
			return is_equal_fun(static_cast<Fun_type const* const>(a), static_cast<Fun_type const* const>(b));
	}

	assert(false);	// All types must be defined
}


char const* const kind_to_string(Type::Kind const kind) {
	switch (kind) {
		case Type::Kind::bool_type:
			return "bool";
		case Type::Kind::char_type:
			return "char";
		case Type::Kind::int_type:
			return "int";
		case Type::Kind::uint_type:
			return "uint";
		case Type::Kind::float_type:
			return "float";
		case Type::Kind::double_type:
			return "double";
		case Type::Kind::ref_type:
			return "ref";
		case Type::Kind::fun_type:
			return "fun";
	}

	assert(false);	// All type kinds must be defined
}
