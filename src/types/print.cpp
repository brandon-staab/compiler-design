#include "types/type_incl.hpp"

#include <iostream>


namespace {
	void print_ref(std::ostream& os, Ref_type const* const type) {
		os << "(ref ";
		print(os, type->get_referent_type());
		os << ')';
	}


	void print_fun(std::ostream& os, Fun_type const* const type) {
		os << "fun (";
		for (auto parm_type = type->begin(); parm_type != type->end() - 1;) {
			print(os, *parm_type);

			if (++parm_type != type->end() - 1) {
				os << ", ";
			}
		}
		os << ") -> " << *type->get_ret_type();
	}
}  // namespace

/*************************************************************/

void print(std::ostream& os, Type const* const type) {
	assert(type != nullptr);

	switch (type->get_kind()) {
		case Type::Kind::bool_type:
		case Type::Kind::char_type:
		case Type::Kind::int_type:
		case Type::Kind::uint_type:
		case Type::Kind::float_type:
		case Type::Kind::double_type:
			os << type->get_kind();
			return;

		case Type::Kind::ref_type:
			return print_ref(os, static_cast<Ref_type const* const>(type));

		case Type::Kind::fun_type:
			return print_fun(os, static_cast<Fun_type const* const>(type));
	}

	assert(false);	// All types must be defined
}
