#include "types/type_incl.hpp"

#include <iostream>


namespace {
	void to_sexpr_ref(std::ostream& os, Ref_type const* const type) { os << "ref (" << type->get_referent_type() << ')'; }


	void to_sexpr_fun(std::ostream& os, Fun_type const* const type) {
		os << "fun (";
		for (auto parm_type = type->begin(); parm_type != type->end();) {
			os << *parm_type;

			if (++parm_type != type->end()) {
				os << ' ';
			}
		}
		os << ") " << *type->get_ret_type();
	}
}  // namespace

/*************************************************************/

void to_sexpr(std::ostream& os, Type const* const type) {
	assert(type != nullptr);

	switch (type->get_kind()) {
		case Type::Kind::bool_type:
		case Type::Kind::char_type:
		case Type::Kind::int_type:
		case Type::Kind::uint_type:
		case Type::Kind::float_type:
		case Type::Kind::double_type:
			os << '(' << type->get_kind() << ')';
			return;

		case Type::Kind::ref_type:
			os << '(';
			to_sexpr_ref(os, static_cast<Ref_type const* const>(type));
			os << ')';
			return;

		case Type::Kind::fun_type:
			os << '(';
			to_sexpr_fun(os, static_cast<Fun_type const* const>(type));
			os << ')';
			return;
	}

	assert(false);	// All types must be defined
}
