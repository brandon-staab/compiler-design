#include "types/Multiary_type.hpp"


Multiary_type::Multiary_type(Kind const kind, Type_seq const& operands) : Type(kind), Dynamic_arity_node(operands) {}

/*************************************************************/

Fun_type::Fun_type(Type_seq const& operands) : Multiary_type(Kind::fun_type, operands) {}


Type const* const Fun_type::get_parm_type(std::size_t const i) const {
	assert(i < get_arity() - 1);
	assert(get_operand(i) != nullptr);

	return get_operand(i);
}


Type const* const Fun_type::get_ret_type() const {
	assert(get_operand(get_arity() - 1) != nullptr);

	return get_operand(get_arity() - 1);
}
