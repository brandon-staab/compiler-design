#include "types/Nullary_type.hpp"


Nullary_type::Nullary_type(Kind const kind) : Fixed_Arity_type(kind) {}

/*************************************************************/

Bool_type::Bool_type() : Nullary_type(Kind::bool_type) {}

/*****************************/

Char_type::Char_type() : Nullary_type(Kind::char_type) {}

/*****************************/

Int_type::Int_type() : Nullary_type(Kind::int_type) {}

/*****************************/

UInt_type::UInt_type() : Nullary_type(Kind::uint_type) {}

/*****************************/

Float_type::Float_type() : Nullary_type(Kind::float_type) {}

/*****************************/

Double_type::Double_type() : Nullary_type(Kind::double_type) {}
