#include "types/type_incl.hpp"

#include "utilities/insertIndent.hpp"

#include <iostream>


namespace {
	void dump_debug_ref(std::ostream& os, Ref_type const* const type, std::size_t const depth) {
		os << "ref";
		dump_debug(os, type->get_referent_type(), depth);
	}


	void dump_debug_fun(std::ostream& os, Fun_type const* const type, std::size_t const depth) {
		os << "fun";
		for (auto parm_type = type->begin(); parm_type != type->end(); parm_type++) {
			dump_debug(os, *parm_type, depth);
		}
		dump_debug(os, type->get_ret_type(), depth);
	}
}  // namespace

/*************************************************************/

void dump_debug(std::ostream& os, Type const* const type, std::size_t depth) {
	assert(type != nullptr);
	assert(depth < 256);

	os << '\n' << &type;
	insertIndent(os, ++depth);

	switch (type->get_kind()) {
		case Type::Kind::bool_type:
		case Type::Kind::char_type:
		case Type::Kind::int_type:
		case Type::Kind::uint_type:
		case Type::Kind::float_type:
		case Type::Kind::double_type:
			os << type->get_kind();
			return;

		case Type::Kind::ref_type:
			return dump_debug_ref(os, static_cast<Ref_type const* const>(type), depth);

		case Type::Kind::fun_type:
			return dump_debug_fun(os, static_cast<Fun_type const* const>(type), depth);
	}

	assert(false);	// All types must be defined
}
