#include "types/Unary_type.hpp"


Unary_type::Unary_type(Kind const kind, Type const* const type) : Fixed_Arity_type(kind, type) {}

/*************************************************************/

Ref_type::Ref_type(Type const* const type) : Unary_type(Kind::ref_type, type) {}


Type const* Ref_type::get_referent_type() const {
	assert(get_operand(0) != nullptr);

	return get_operand(0);
}
