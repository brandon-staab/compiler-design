#include "semantics/SemanticActions.hpp"

#include "builder/builder.hpp"
#include "types/type_incl.hpp"

#include <cassert>
#include <iostream>


Type const* SemanticActions::get_bool_type(Token tok) {
	return Builder::get_Bool_type();
}


Type const* SemanticActions::get_char_type(Token tok) {
	return Builder::get_Char_type();
}


Type const* SemanticActions::get_int_type(Token tok) {
	return Builder::get_Int_type();
}


Type const* SemanticActions::get_unsigned_type(Token tok) {
	return Builder::get_UInt_type();
}


Type const* SemanticActions::get_float_type(Token tok) {
	return Builder::get_Float_type();
}


Type const* SemanticActions::get_double_type(Token tok) {
	return Builder::get_Double_type();
}

Type const* SemanticActions::on_ref_type(Type const* type, Token ref) {
	assert(type != nullptr);

	return Builder::get_Ref_type(type);
}


Type const* SemanticActions::on_function_type(Type const* type) {
	assert(type != nullptr);

	assert(false);
}


Expression* SemanticActions::on_type_get_zero(Type const* type) {
	assert(type != nullptr);
	assert(type->is_primitive());

	switch (type->get_kind()) {
		case Type::Kind::bool_type:
			return Builder::make_Bool_expr();

		case Type::Kind::char_type:
			return Builder::make_Char_expr();

		case Type::Kind::int_type:
			return Builder::make_Int_expr();

		case Type::Kind::uint_type:
			return Builder::make_UInt_expr();

		case Type::Kind::float_type:
			return Builder::make_Float_expr();

		case Type::Kind::double_type:
			return Builder::make_Double_expr();
	}

	assert(false);
}
