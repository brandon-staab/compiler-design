#include "semantics/Scope.hpp"

#include "declarations/decl_incl.hpp"

#include <cassert>
#include <iostream>


/**** Scope **************************************************/

Declaration* Scope::lookup(Symbol const sym) {
	auto iter = find(sym);
	if (iter == end()) {
		return nullptr;
	}

	return iter->second;
}


Declaration const* Scope::lookup(Symbol const sym) const {
	auto iter = find(sym);
	if (iter == end()) {
		return nullptr;
	}

	return iter->second;
}


Declaration* Scope::declare(Symbol const sym, Declaration* decl) {
	assert(!sym.name().empty());
	assert(decl != nullptr);
	assert(sym.name() == decl->get_name());
	assert(count(sym) == 0);

	emplace(sym, decl);

	return decl;
}

/**** Scope_stack ********************************************/

Declaration* Scope_stack::lookup(Symbol const sym) {
	for (auto iter = rbegin(); iter != rend(); ++iter) {
		if (Declaration* decl = iter->lookup(sym)) {
			return decl;
		}
	}

	return nullptr;
}


Declaration const* Scope_stack::lookup(Symbol const sym) const {
	for (auto iter = rbegin(); iter != rend(); ++iter) {
		if (Declaration const* decl = iter->lookup(sym)) {
			return decl;
		}
	}

	return nullptr;
}


Declaration* Scope_stack::declare(Symbol const sym, Declaration* decl) {
	// std::cerr << "Scope_stack:: declare " << sym.name() << " on scope: " << (size() - 1) << '\n';
	return top().declare(sym, decl);
}


void Scope_stack::push() {
	emplace_back();
	// std::cerr << "Scope_stack:: new scope: " << (size() - 1) << '\n';
}


void Scope_stack::pop() {
	// std::cerr << "Scope_stack:: exited scope: " << (size() - 1) << '\n';
	pop_back();
}


Scope& Scope_stack::top() {
	return back();
}


Scope const& Scope_stack::top() const {
	return back();
}
