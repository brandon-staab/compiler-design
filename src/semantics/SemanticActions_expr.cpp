#include "semantics/SemanticActions.hpp"

#include "builder/builder.hpp"

#include <sstream>


Expression* SemanticActions::on_assignment_expression(Expression* lhs, Expression* rhs, Token const& op) {
	switch (op.get_kind()) {
		case Token::Kind::eq_tok:
			return Builder::make_Assignment_expr(lhs, rhs);

		case Token::Kind::star_eq_tok:
			return Builder::make_Multiplication_Assignment_expr(lhs, rhs);

		case Token::Kind::slash_eq_tok:
			return Builder::make_Quotient_Assignment_expr(lhs, rhs);

		case Token::Kind::percent_eq_tok:
			return Builder::make_Modulo_Assignment_expr(lhs, rhs);

		case Token::Kind::plus_eq_tok:
			return Builder::make_Addition_Assignment_expr(lhs, rhs);

		case Token::Kind::minus_eq_tok:
			return Builder::make_Subtraction_Assignment_expr(lhs, rhs);

		case Token::Kind::greater_greater_eq_tok:
			return Builder::make_B_RSHIFT_Assignment_expr(lhs, rhs);

		case Token::Kind::less_less_eq_tok:
			return Builder::make_B_LSHIFT_Assignment_expr(lhs, rhs);

		case Token::Kind::ampersand_eq_tok:
			return Builder::make_B_AND_Assignment_expr(lhs, rhs);

		case Token::Kind::caret_eq_tok:
			return Builder::make_B_XOR_Assignment_expr(lhs, rhs);

		case Token::Kind::bar_eq_tok:
			return Builder::make_B_OR_Assignment_expr(lhs, rhs);
	}

	assert(false);
}


Expression* SemanticActions::on_conditional_expression(Expression* e1, Expression* e2, Expression* e3, Token const& tok1, Token const& tok2) {
	switch (tok1.get_kind()) {
		case Token::Kind::question_tok:
			if (tok2.is(Token::Kind::colon_tok)) {
				return Builder::make_Conditional_expr(e1, e2, e3);
			}
	}

	assert(false);
}


Expression* SemanticActions::on_logical_expression(Expression* lhs, Expression* rhs, Token const& op) {
	switch (op.get_kind()) {
		case Token::Kind::bar_bar_tok:
			return Builder::make_L_OR_expr(lhs, rhs);

		case Token::Kind::ampersand_ampersand_tok:
			return Builder::make_L_AND_expr(lhs, rhs);
	}

	assert(false);
}


Expression* SemanticActions::on_bitwise_expression(Expression* lhs, Expression* rhs, Token const& op) {
	switch (op.get_kind()) {
		case Token::Kind::bar_tok:
			return Builder::make_B_OR_expr(lhs, rhs);

		case Token::Kind::caret_tok:
			return Builder::make_B_XOR_expr(lhs, rhs);

		case Token::Kind::ampersand_tok:
			return Builder::make_B_AND_expr(lhs, rhs);
	}

	assert(false);
}


Expression* SemanticActions::on_equality_expression(Expression* lhs, Expression* rhs, Token const& op) {
	switch (op.get_kind()) {
		case Token::Kind::eq_eq_tok:
			return Builder::make_EQUAL_expr(lhs, rhs);

		case Token::Kind::bang_eq_tok:
			return Builder::make_NEQUAL_expr(lhs, rhs);
	}

	assert(false);
}


Expression* SemanticActions::on_relational_expression(Expression* lhs, Expression* rhs, Token const& op) {
	switch (op.get_kind()) {
		case Token::Kind::less_tok:
			return Builder::make_LT_expr(lhs, rhs);

		case Token::Kind::greater_tok:
			return Builder::make_GT_expr(lhs, rhs);

		case Token::Kind::less_eq_tok:
			return Builder::make_LTOE_expr(lhs, rhs);

		case Token::Kind::greater_eq_tok:
			return Builder::make_GTOE_expr(lhs, rhs);
	}

	assert(false);
}


Expression* SemanticActions::on_shift_expression(Expression* lhs, Expression* rhs, Token const& op) {
	switch (op.get_kind()) {
		case Token::Kind::less_less_tok:
			return Builder::make_B_LSHIFT_Assignment_expr(lhs, rhs);

		case Token::Kind::greater_greater_tok:
			return Builder::make_B_RSHIFT_Assignment_expr(lhs, rhs);
	}

	assert(false);
}


Expression* SemanticActions::on_additive_expression(Expression* lhs, Expression* rhs, Token const& op) {
	switch (op.get_kind()) {
		case Token::Kind::plus_tok:
			return Builder::make_Addition_expr(lhs, rhs);

		case Token::Kind::minus_tok:
			return Builder::make_Subtraction_expr(lhs, rhs);
	}

	assert(false);
}


Expression* SemanticActions::on_multiplicative_expression(Expression* lhs, Expression* rhs, Token const& op) {
	switch (op.get_kind()) {
		case Token::Kind::star_tok:
			return Builder::make_Multiplication_expr(lhs, rhs);

		case Token::Kind::slash_tok:
			return Builder::make_Quotient_expr(lhs, rhs);

		case Token::Kind::percent_tok:
			return Builder::make_Modulo_expr(lhs, rhs);
	}

	assert(false);
}


Expression* SemanticActions::on_access_expression(Expression* expr, Token const& op) {
	switch (op.get_kind()) {
		case Token::Kind::dot_tok:
		case Token::Kind::rarrow_tok: {
			std::stringstream ss;
			ss << "Access not suppported: " << op;
			throw std::runtime_error(ss.str());
		}
	}

	assert(false);
}


Expression* SemanticActions::on_prefix_expression(Expression* expr, Token const& op) {
	switch (op.get_kind()) {
		case Token::Kind::plus_plus_tok:
			return Builder::make_pre_Increment_expr(expr);

		case Token::Kind::minus_minus_tok:
			return Builder::make_pre_Decrement_expr(expr);

		case Token::Kind::tilde_tok:
			return Builder::make_B_NOT_expr(expr);

		case Token::Kind::bang_tok:
			return Builder::make_L_NOT_expr(expr);

		case Token::Kind::plus_tok:
			return expr;

		case Token::Kind::minus_tok:
			return Builder::make_Minus_expr(expr);

		case Token::Kind::slash_tok:
			return Builder::make_Reciprocal_expr(expr);

		case Token::Kind::ampersand_tok:
		case Token::Kind::star_tok: {
			std::stringstream ss;
			ss << "Prefix not suppported: " << op;
			throw std::runtime_error(ss.str());
		}
	}

	assert(false);
}


Expression* SemanticActions::on_postfix_expression(Expression* expr, Token const& op) {
	switch (op.get_kind()) {
		case Token::Kind::plus_plus_tok:
			return Builder::make_post_Increment_expr(expr);

		case Token::Kind::minus_minus_tok:
			return Builder::make_post_Decrement_expr(expr);

		case Token::Kind::dot_tok:
		case Token::Kind::rarrow_tok: {
			std::stringstream ss;
			ss << "postfix not suppported: " << op;
			throw std::runtime_error(ss.str());
		}
	}

	assert(false);
}


Expression* SemanticActions::on_call_expression(Expression* id, Expr_seq args, Token const& lparen, Token const& rparen) {
	Expr_seq exprs;

	exprs.push_back(id);
	exprs.insert(exprs.end(), args.begin(), args.end());

	return Builder::make_Call_expr(exprs);
}


Expression* SemanticActions::on_boolean_literal(Token const& bool_lit) {
	return Builder::make_Bool_expr(bool_lit.is(Token::Kind::true_tok));
}


Expression* SemanticActions::on_integer_literal(Token const& int_lit) {
	int n = std::stoi(int_lit.get_lexeme().name());

	return Builder::make_Int_expr(n);
}


Expression* SemanticActions::on_float_literal(Token const& float_lit) {
	float n = std::stof(float_lit.get_lexeme().name());

	return Builder::make_Int_expr(n);
}


Expression* SemanticActions::on_paren_expression(Expression* expr, Token const& lparen, Token const& rparen) {
	return expr;
}


Expression* SemanticActions::on_identifier_expression(Token const& id_tok) {
	if (Declaration* decl = m_stack.lookup(id_tok.get_lexeme())) {
		return Builder::make_Identifier_expr(decl);
	}

	std::stringstream ss;
	ss << "No matching declaration for: " << id_tok;
	throw std::runtime_error(ss.str());
}
