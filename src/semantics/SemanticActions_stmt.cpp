#include "semantics/SemanticActions.hpp"

#include "builder/builder.hpp"

#include <cassert>
#include <sstream>


Statement* SemanticActions::on_empty_statement(Token semicolon) {
	return Builder::get_Skip_stmt();
}


Statement* SemanticActions::on_block_statement(Stmt_seq& stmts, Token lbrace, Token rbrace) {
	return Builder::make_Block_stmt(stmts);
}


Statement*
SemanticActions::on_if_statement(Expression* condition, Statement* true_stmt, Statement* false_stmt, Token if_kw, Token lparen, Token rparen, Token else_kw) {
	return Builder::make_If_stmt(condition, true_stmt, false_stmt);
}


Statement* SemanticActions::on_when_statement(Expression* condition, Statement* true_stmt, Token if_kw, Token lparen, Token rparen) {
	return Builder::make_When_stmt(condition, true_stmt);
}


Statement* SemanticActions::on_while_statement(Expression* condition, Statement* body, Token keyword, Token lparen, Token rparen) {
	return Builder::make_While_stmt(condition, body);
}


Statement* SemanticActions::on_break_statement(Token keyword, Token semicolon) {
	return Builder::get_Break_stmt();
}


Statement* SemanticActions::on_continue_statement(Token keyword, Token semicolon) {
	return Builder::get_Continue_stmt();
}


Statement* SemanticActions::on_return_statement(Expression* ret, Token keyword, Token semicolon) {
	return Builder::make_Return_stmt(ret);
}


Statement* SemanticActions::on_declaration_statement(Declaration* decl) {
	return Builder::make_Declaration_stmt(decl);
}


Statement* SemanticActions::on_expression_statement(Expression* expr, Token semicolon) {
	return Builder::make_Expression_stmt(expr);
}
