#include "semantics/SemanticActions.hpp"

#include "builder/builder.hpp"
#include "declarations/decl_incl.hpp"

#include <cassert>
#include <iostream>
#include <sstream>


Declaration* SemanticActions::on_program(Decl_seq& decls) {
	return Builder::make_Program_decl(decls);
}


Declaration* SemanticActions::on_object_declaration(Token id, Type const* type, Token var_tok, Token colon) {
	must_not_exist(id);
	Name name = id.get_lexeme().name();
	Declaration* var = Builder::make_Variable_decl(name, type);

	return declare(id, var);
}


Declaration* SemanticActions::on_reference_definintion(Token id, Type const* type, Token let, Token colon, Token ref, Token equal, Token semicolon) {
	must_not_exist(id);
	Declaration* target = must_exist(ref);
	if (target->get_type() != type) {
		std::stringstream ss;
		ss << id << " '" << *type << "' and " << ref << " '" << *target->get_type() << "' do not have the same type";
		throw std::runtime_error(ss.str());
	}

	Name name = id.get_lexeme().name();
	auto alias = static_cast<Variable_decl*>(Builder::make_Variable_decl(name, Builder::get_Ref_type(type)));

	Expression* target_expr = Builder::make_Identifier_expr(target);
	// Expression* ref_expr = Builder::make_Convert_Value_expr(target_expr);  // FIXME ?
	alias->set_initializer(target_expr);

	return declare(id, alias);
}


Declaration* SemanticActions::on_function_declaration(Token id, Decl_seq& parms, Type const* type) {
	must_not_exist(id);

	Type_seq types;
	for (auto parm : parms) {
		types.push_back(parm->get_type());
	}
	types.push_back(type);

	auto func_type = static_cast<Fun_type const*>(Builder::get_Fun_type(types));
	auto const name = id.get_lexeme().name();
	m_func = static_cast<Function_decl*>(Builder::make_Function_decl(name, func_type));

	for (auto parm : parms) {
		m_func->add_parameter(parm);
	}

	return declare(id, m_func);
}


Declaration* SemanticActions::on_parameter(Token id, Type const* type, Token colon) {
	must_not_exist(id);
	Name name = id.get_lexeme().name();
	Declaration* parm;

	if (type->is_reference()) {
		parm = Builder::make_Variable_decl(name, type);
	} else {
		parm = Builder::make_Variable_decl(name, type);
	}

	return declare(id, parm);
}


Declaration* SemanticActions::finish_object_definition(Declaration* var, Expression* init, Token semicolon) {
	Builder::copy_initialize(var, init);

	return var;
}


void SemanticActions::start_function_definition(Declaration* fn) {
	m_func = static_cast<Function_decl*>(fn);
}


void SemanticActions::finish_function_definition(Declaration* fn, Statement* body) {
	assert(m_func == fn);

	m_func->set_body(body);
	m_func = nullptr;
}
