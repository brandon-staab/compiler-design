#include "semantics/SemanticActions.hpp"

#include "declarations/decl_incl.hpp"
#include "expressions/expr_incl.hpp"
#include "statements/stmt_incl.hpp"
#include "types/type_incl.hpp"

#include <iostream>
#include <sstream>
#include <stdexcept>


void SemanticActions::enter_scope() {
	m_stack.push();
}


void SemanticActions::leave_scope() {
	m_stack.pop();
}


Declaration* SemanticActions::declare(Symbol sym, Declaration* decl) {
	assert(decl != nullptr);

	return m_stack.declare(sym, decl);
}


Declaration* SemanticActions::declare(Token token, Declaration* decl) {
	return declare(token.get_lexeme(), decl);
}


Declaration* SemanticActions::lookup(Symbol sym) {
	m_stack.lookup(sym);
}


Declaration* SemanticActions::lookup(Token token) {
	return lookup(token.get_lexeme());
}


Declaration* SemanticActions::must_exist(Token token) {
	Declaration* target = lookup(token);

	if (target == nullptr) {
		std::stringstream ss;
		ss << token << " has yet to be declared";
		throw std::runtime_error(ss.str());
	}

	return target;
}


void SemanticActions::must_not_exist(Token token) const {
	Symbol sym = token.get_lexeme();
	if (m_stack.top().lookup(sym)) {
		std::stringstream ss;
		ss << "redefinition error " << token;
		throw std::runtime_error(ss.str());
	}
}


Declaration* SemanticActions::declare_unique(Token token, Declaration* decl) {
	must_not_exist(token);

	return declare(token, decl);
}
