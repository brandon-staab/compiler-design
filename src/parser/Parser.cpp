#include "parser/Parser.hpp"


Parser::Parser(SymbolTable& syms, std::string const& input) : m_lex(syms, input) {
	while (auto tok = m_lex()) {
		m_toks.push_back(tok);
	}

	m_lookahead = m_toks.data();
	m_last = m_lookahead + m_toks.size();
}

/**** Lookahead **************/

Token const& Parser::peek() const {
	return *m_lookahead;
}


Token::Kind Parser::lookahead() const {
	return peek().get_kind();
}


bool Parser::is_eof() const {
	return m_lookahead == m_last;
}


bool Parser::next_token_is(Token::Kind const kind) const {
	return lookahead() == kind;
}


bool Parser::next_token_is_not(Token::Kind const kind) const {
	return lookahead() != kind;
}

/**** Scope ******************/

void Parser::enter_scope() {
	m_act.enter_scope();
}


void Parser::leave_scope() {
	m_act.leave_scope();
}


Declaration* Parser::declare(Symbol sym, Declaration* decl) {
	return m_act.declare(sym, decl);
}


Declaration* Parser::declare(Token token, Declaration* decl) {
	return m_act.declare(token, decl);
}


Declaration* Parser::lookup(Symbol sym) {
	return m_act.lookup(sym);
}


Declaration* Parser::lookup(Token token) {
	return m_act.lookup(token);
}
