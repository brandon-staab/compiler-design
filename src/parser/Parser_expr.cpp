#include "parser/Parser.hpp"

#include <iostream>
#include <sstream>
#include <stdexcept>


Expression* Parser::parse_expression() {
	return parse_assignment_expression();
}


Expression* Parser::parse_assignment_expression() {
	Expression* lhs = parse_conditional_expression();

	if (Token op = match(Token::Kind::eq_tok)) {
		Expression* rhs = parse_assignment_expression();
		return m_act.on_assignment_expression(lhs, rhs, op);
	}

	return lhs;
}


Expression* Parser::parse_conditional_expression() {
	Expression* e1 = parse_logical_or_expression();

	if (Token tok1 = match(Token::Kind::question_tok)) {
		Expression* e2 = parse_expression();
		Token tok2 = expect(Token::Kind::colon_tok);
		Expression* e3 = parse_conditional_expression();

		return m_act.on_conditional_expression(e1, e2, e3, tok1, tok2);
	}

	return e1;
}


Expression* Parser::parse_logical_or_expression() {
	Expression* lhs = parse_logical_and_expression();

	while (Token op = match(Token::Kind::bar_bar_tok)) {
		Expression* rhs = parse_logical_and_expression();
		lhs = m_act.on_logical_expression(lhs, rhs, op);
	}

	return lhs;
}


Expression* Parser::parse_logical_and_expression() {
	Expression* lhs = parse_bitwise_or_expression();

	while (Token op = match(Token::Kind::ampersand_ampersand_tok)) {
		Expression* rhs = parse_bitwise_or_expression();
		lhs = m_act.on_logical_expression(lhs, rhs, op);
	}

	return lhs;
}


Expression* Parser::parse_bitwise_or_expression() {
	Expression* lhs = parse_bitwise_xor_expression();

	while (Token op = match(Token::Kind::bar_tok)) {
		Expression* rhs = parse_bitwise_xor_expression();
		lhs = m_act.on_bitwise_expression(lhs, rhs, op);
	}

	return lhs;
}


Expression* Parser::parse_bitwise_xor_expression() {
	Expression* lhs = parse_bitwise_and_expression();

	while (Token op = match(Token::Kind::caret_tok)) {
		Expression* rhs = parse_bitwise_and_expression();
		lhs = m_act.on_bitwise_expression(lhs, rhs, op);
	}

	return lhs;
}


Expression* Parser::parse_bitwise_and_expression() {
	Expression* lhs = parse_equality_expression();

	while (Token op = match(Token::Kind::ampersand_tok)) {
		Expression* rhs = parse_equality_expression();
		lhs = m_act.on_bitwise_expression(lhs, rhs, op);
	}

	return lhs;
}


Expression* Parser::parse_equality_expression() {
	Expression* lhs = parse_relational_expression();

	while (Token op = match_equality_operator()) {
		Expression* rhs = parse_relational_expression();
		lhs = m_act.on_equality_expression(lhs, rhs, op);
	}

	return lhs;
}


Expression* Parser::parse_relational_expression() {
	Expression* lhs = parse_shift_expression();

	while (Token op = match_relational_operator()) {
		Expression* rhs = parse_shift_expression();
		lhs = m_act.on_relational_expression(lhs, rhs, op);
	}

	return lhs;
}


Expression* Parser::parse_shift_expression() {
	Expression* lhs = parse_additive_expression();

	while (Token op = match_shift_operator()) {
		Expression* rhs = parse_additive_expression();
		lhs = m_act.on_shift_expression(lhs, rhs, op);
	}

	return lhs;
}


Expression* Parser::parse_additive_expression() {
	Expression* lhs = parse_multiplicative_expression();

	while (Token op = match_additive_operator()) {
		Expression* rhs = parse_multiplicative_expression();
		lhs = m_act.on_additive_expression(lhs, rhs, op);
	}

	return lhs;
}


Expression* Parser::parse_multiplicative_expression() {
	Expression* lhs = parse_access_expression();

	while (Token op = match_multiplicative_operator()) {
		Expression* rhs = parse_access_expression();
		lhs = m_act.on_multiplicative_expression(lhs, rhs, op);
	}

	return lhs;
}


Expression* Parser::parse_access_expression() {
	if (Token op = match_access_operator()) {
		Expression* expr = parse_prefix_expression();
		return m_act.on_access_expression(expr, op);
	}

	return parse_prefix_expression();
}


Expression* Parser::parse_prefix_expression() {
	while (Token op = match_prefix_operator()) {
		switch (op.get_kind()) {
			case Token::Kind::new_tok:
			case Token::Kind::delete_tok:
			case Token::Kind::sizeof_tok: {
				std::stringstream ss;
				ss << "Prefix not suppported: " << peek();
				throw std::runtime_error(ss.str());
			}

			default: {
				Expression* expr = parse_prefix_expression();
				return m_act.on_prefix_expression(expr, op);
			}
		}
	}

	return parse_postfix_expression();
}


Expression* Parser::parse_postfix_expression() {
	Expression* expr = parse_primary_expression();

	while (Token op = match_postfix_operator()) {
		switch (op.get_kind()) {
			case Token::Kind::lparen_tok:
				return parse_call_expression(expr, op);

			case Token::Kind::lbrace_tok: {
				std::stringstream ss;
				ss << "postfix subscript-expression not suppported: " << peek();
				throw std::runtime_error(ss.str());
			}

			default:
				return m_act.on_postfix_expression(expr, op);
		}
	}

	return expr;
}


Expression* Parser::parse_call_expression(Expression* id, Token lparen) {
	Expr_seq args = parse_argument_list();
	Token rparen = expect(Token::Kind::rparen_tok);

	return m_act.on_call_expression(id, args, lparen, rparen);
}


Expr_seq Parser::parse_argument_list() {
	Expr_seq args;

	while (true) {
		Expression* arg = parse_argument();
		args.push_back(arg);

		if (match(Token::Kind::comma_tok)) {
			continue;
		}

		if (next_token_is(Token::Kind::rparen_tok)) {
			break;
		}
	}

	return args;
}


Expression* Parser::parse_argument() {
	return parse_expression();
}


Expression* Parser::parse_primary_expression() {
	switch (lookahead()) {
		case Token::Kind::true_tok:
		case Token::Kind::false_tok:
			return m_act.on_boolean_literal(consume());

		case Token::Kind::integer_literal_tok:
			return m_act.on_integer_literal(consume());

		case Token::Kind::float_literal_tok:
			return m_act.on_float_literal(consume());

		case Token::Kind::identifier_tok:
			return parse_identifier_expression();

		case Token::Kind::lparen_tok: {
			Token lparen = expect(Token::Kind::lparen_tok);
			Expression* expr = parse_expression();
			Token rparen = expect(Token::Kind::rparen_tok);

			return m_act.on_paren_expression(expr, lparen, rparen);
		}

		default:
			break;
	}

	std::stringstream ss;
	ss << "expected primary-expression, but got: " << peek();
	throw std::runtime_error(ss.str());
}


Expression* Parser::parse_identifier_expression() {
	Token id = require(Token::Kind::identifier_tok);
	return m_act.on_identifier_expression(id);
}
