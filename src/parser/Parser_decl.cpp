#include "parser/Parser.hpp"

#include <cassert>
#include <iostream>
#include <sstream>
#include <stdexcept>


// program → declaration-seq?
Declaration* Parser::parse_program() {
	enter_scope();
	Decl_seq decls = parse_declaration_seq();
	leave_scope();

	return m_act.on_program(decls);
}

// declaration-seq → declaration-seq declaration
//                 | declaration
Decl_seq Parser::parse_declaration_seq() {
	Decl_seq decls;

	while (peek()) {
		Declaration* decl = parse_declaration();
		decls.push_back(decl);
	}

	return decls;
}


// declaration → function-definition
//             | variable-definition
Declaration* Parser::parse_declaration() {
	if (peek().is(Token::Kind::fun_tok)) {
		return parse_function_definition();
	}

	return parse_variable_definition();
}


// local-declaration → variable-definition
Declaration* Parser::parse_local_declaration() {
	return parse_variable_definition();
}


// variable-definition → object-definition
//                      | reference-definition
Declaration* Parser::parse_variable_definition() {
	Declaration* decl;

	switch (lookahead()) {
		case Token::Kind::var_tok:
			decl = parse_object_definition();
			break;
		case Token::Kind::let_tok:
			decl = parse_reference_definition();
			break;
		case Token::Kind::ref_tok: {
			std::stringstream ss;
			ss << "variable definition is malformed, try 'let' instead of " << peek();
			throw std::runtime_error(ss.str());
		}
		default: {
			std::stringstream ss;
			ss << "variable definition is malformed: " << peek();
			throw std::runtime_error(ss.str());
		}
	}

	return decl;
}


// object-definition → var identifier: type-specifier;
//                   | var identifier: type-specifier = expression;
Declaration* Parser::parse_object_definition() {
	Token var_tok = require(Token::Kind::var_tok);
	Token id = expect(Token::Kind::identifier_tok);
	Token colon = expect(Token::Kind::colon_tok);
	Type const* type = parse_type_specifier();

	Declaration* var = m_act.on_object_declaration(id, type, var_tok, colon);

	Expression* init;
	if (Token equal = match(Token::Kind::eq_tok)) {
		init = parse_expression();
	} else {
		init = m_act.on_type_get_zero(type);
	}

	Token semicolon = expect(Token::Kind::semicolon_tok);

	return m_act.finish_object_definition(var, init, semicolon);
}


// reference-definition → let identifier: type-specifier = expression;
Declaration* Parser::parse_reference_definition() {
	Token let = require(Token::Kind::let_tok);
	Token id = expect(Token::Kind::identifier_tok);
	Token colon = expect(Token::Kind::colon_tok);
	Type const* type = parse_type_specifier();
	Token equal = expect(Token::Kind::eq_tok);
	Token ref = expect(Token::Kind::identifier_tok);
	Token semicolon = expect(Token::Kind::semicolon_tok);

	return m_act.on_reference_definintion(id, type, let, colon, ref, equal, semicolon);
}


// function-definition → fun identier (parameter-list?) -> type-specier block-statement
Declaration* Parser::parse_function_definition() {
	Token fun = require(Token::Kind::fun_tok);
	Token id = expect(Token::Kind::identifier_tok);

	Token lparen = expect(Token::Kind::lparen_tok);
	m_act.enter_scope();
	Decl_seq decls;
	if (next_token_is_not(Token::Kind::rparen_tok)) {
		decls = parse_parameter_list();
	}
	Token rparen = expect(Token::Kind::rparen_tok);

	Token rarrow = expect(Token::Kind::rarrow_tok);
	Type const* type = parse_type_specifier();
	Declaration* fn = m_act.on_function_declaration(id, decls, type);

	m_act.start_function_definition(fn);
	Statement* body = parse_block_statement();
	m_act.finish_function_definition(fn, body);
	m_act.leave_scope();

	return declare(id, fn);
}


Decl_seq Parser::parse_parameter_list() {
	assert(next_token_is_not(Token::Kind::rparen_tok));
	Decl_seq decls;

	do {
		Declaration* decl = parse_parameter();
		decls.push_back(decl);
	} while (match(Token::Kind::comma_tok));

	std::cerr << "parmlist size = " << decls.size() << '\n';

	return decls;
}


Declaration* Parser::parse_parameter() {
	Token id = expect(Token::Kind::identifier_tok);
	Token colon = expect(Token::Kind::colon_tok);
	Type const* type = parse_type_specifier();

	return m_act.on_parameter(id, type, colon);
}
