#include "parser/Parser.hpp"

#include <cassert>


Statement* Parser::parse_statement() {
	switch (lookahead()) {
		case Token::Kind::semicolon_tok:
			return parse_empty_statement();

		case Token::Kind::lbrace_tok:
			return parse_block_statement();

		case Token::Kind::if_tok:
			return parse_if_statement();

		case Token::Kind::while_tok:
			return parse_while_statement();

		case Token::Kind::break_tok:
			return parse_break_statement();

		case Token::Kind::continue_tok:
			return parse_continue_statement();

		case Token::Kind::return_tok:
			return parse_return_statement();

		case Token::Kind::var_tok:
		case Token::Kind::ref_tok:
			return parse_declaration_statement();

		default:
			return parse_expression_statement();
	}

	assert(false);
}


Statement* Parser::parse_empty_statement() {
	Token semicolon = require(Token::Kind::semicolon_tok);

	return m_act.on_empty_statement(semicolon);
}


Statement* Parser::parse_block_statement() {
	Stmt_seq stmts;

	Token lbrace = require(Token::Kind::lbrace_tok);
	enter_scope();

	while (next_token_is_not(Token::Kind::rbrace_tok)) {
		Statement* stmt = parse_statement();
		stmts.push_back(stmt);
	}

	leave_scope();
	Token rbrace = expect(Token::Kind::rbrace_tok);

	return m_act.on_block_statement(stmts, lbrace, rbrace);
}


Statement* Parser::parse_if_statement() {
	Token if_kw = require(Token::Kind::if_tok);
	Token lparen = expect(Token::Kind::lparen_tok);
	Expression* condition = parse_expression();
	Token rparen = expect(Token::Kind::rparen_tok);
	Statement* true_stmt = parse_statement();

	if (Token else_kw = match(Token::Kind::else_tok)) {
		Statement* false_stmt = parse_statement();

		return m_act.on_if_statement(condition, true_stmt, false_stmt, if_kw, lparen, rparen, else_kw);
	} else {
		return m_act.on_when_statement(condition, true_stmt, if_kw, lparen, rparen);
	}
}


Statement* Parser::parse_while_statement() {
	Token keyword = require(Token::Kind::while_tok);
	Token lparen = expect(Token::Kind::lparen_tok);
	Expression* condition = parse_expression();
	Token rparen = expect(Token::Kind::rparen_tok);
	Statement* body = parse_statement();

	return m_act.on_while_statement(condition, body, keyword, lparen, rparen);
}


Statement* Parser::parse_break_statement() {
	Token keyword = require(Token::Kind::break_tok);
	Token semicolon = expect(Token::Kind::semicolon_tok);

	return m_act.on_break_statement(keyword, semicolon);
}


Statement* Parser::parse_continue_statement() {
	Token keyword = require(Token::Kind::continue_tok);
	Token semicolon = expect(Token::Kind::semicolon_tok);

	return m_act.on_continue_statement(keyword, semicolon);
}


Statement* Parser::parse_return_statement() {
	Token keyword = require(Token::Kind::return_tok);
	Expression* ret = parse_expression();
	Token semicolon = expect(Token::Kind::semicolon_tok);

	return m_act.on_return_statement(ret, keyword, semicolon);
}


Statement* Parser::parse_declaration_statement() {
	Declaration* decl = parse_local_declaration();

	return m_act.on_declaration_statement(decl);
}


Statement* Parser::parse_expression_statement() {
	Expression* expr = parse_expression();
	Token semicolon = expect(Token::Kind::semicolon_tok);

	return m_act.on_expression_statement(expr, semicolon);
}
