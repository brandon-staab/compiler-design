#include "parser/Parser.hpp"

#include <iostream>
#include <sstream>
#include <stdexcept>

Type const* Parser::parse_type_specifier() {
	switch (peek().get_kind()) {
		case Token::Kind::bool_tok: {
			Token tok = consume();
			return m_act.get_bool_type(tok);
		}
		case Token::Kind::char_tok: {
			Token tok = consume();
			return m_act.get_char_type(tok);
		}
		case Token::Kind::int_tok: {
			Token tok = consume();
			return m_act.get_int_type(tok);
		}
		case Token::Kind::unsigned_tok: {
			Token tok = consume();
			return m_act.get_unsigned_type(tok);
		}
		case Token::Kind::float_tok: {
			Token tok = consume();
			return m_act.get_float_type(tok);
		}
		case Token::Kind::double_tok: {
			Token tok = consume();
			return m_act.get_double_type(tok);
		}
		case Token::Kind::ref_tok:
			return parse_ref_type();
	}

	std::stringstream ss;
	ss << "Expected type specifier, but got: " << peek();
	throw std::runtime_error(ss.str());
}


Type const* Parser::parse_ref_type() {
	Token ref = require(Token::Kind::ref_tok);
	Type const* type = parse_type_specifier();
	m_act.on_ref_type(type, ref);

	while (ref = match(Token::Kind::ref_tok)) {
		type = parse_type_specifier();
		m_act.on_ref_type(type, ref);
	}

	return type;
}
