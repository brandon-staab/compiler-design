#include "parser/Parser.hpp"

#include <cassert>
#include <iostream>
#include <sstream>


Token Parser::consume() {
	assert(!is_eof());

	Token ret = *m_lookahead;
	++m_lookahead;

	return ret;
}


Token Parser::match(Token::Kind const kind) {
	if (next_token_is(kind)) {
		return consume();
	}

	return Token{};
}


Token Parser::expect(Token::Kind const kind) {
	if (next_token_is(kind)) {
		return consume();
	}

	std::stringstream ss;
	ss << "Syntax error: expected '" << kind << "', but got '" << lookahead() << "'\n\tInfo: " << peek();
	throw std::runtime_error(ss.str());
}


Token Parser::require(Token::Kind const kind) {
	if (!next_token_is(kind)) {
		std::cerr << "compiler error: require '" << kind << "', but got '" << lookahead() << "'\n\tInfo: " << peek() << '\n';
	}

	assert(next_token_is(kind));

	return consume();
}


Token Parser::match_equality_operator() {
	switch (lookahead()) {
		case Token::Kind::eq_eq_tok:
		case Token::Kind::bang_eq_tok:
			return consume();

		default:
			return Token{};
	}
}


Token Parser::match_relational_operator() {
	switch (lookahead()) {
		case Token::Kind::less_tok:
		case Token::Kind::greater_tok:
		case Token::Kind::less_eq_tok:
		case Token::Kind::greater_eq_tok:
			return consume();

		default:
			return Token{};
	}
}


Token Parser::match_shift_operator() {
	switch (lookahead()) {
		case Token::Kind::less_less_tok:
		case Token::Kind::greater_greater_tok:
			return consume();

		default:
			return Token{};
	}
}


Token Parser::match_additive_operator() {
	switch (lookahead()) {
		case Token::Kind::plus_tok:
		case Token::Kind::minus_tok:
			return consume();

		default:
			return Token{};
	}
}


Token Parser::match_multiplicative_operator() {
	switch (lookahead()) {
		case Token::Kind::star_tok:
		case Token::Kind::slash_tok:
		case Token::Kind::percent_tok:
			return consume();

		default:
			return Token{};
	}
}


Token Parser::match_access_operator() {
	switch (lookahead()) {
		case Token::Kind::dot_tok:
		case Token::Kind::rarrow_tok:
			return consume();

		default:
			return Token{};
	}
}


Token Parser::match_prefix_operator() {
	switch (lookahead()) {
		case Token::Kind::plus_plus_tok:
		case Token::Kind::minus_minus_tok:
		case Token::Kind::tilde_tok:
		case Token::Kind::bang_tok:
		case Token::Kind::plus_tok:
		case Token::Kind::minus_tok:
		case Token::Kind::slash_tok:
		case Token::Kind::ampersand_tok:
		case Token::Kind::star_tok:
		case Token::Kind::new_tok:
		case Token::Kind::delete_tok:
		case Token::Kind::sizeof_tok:
			return consume();

		default:
			return Token{};
	}
}


Token Parser::match_postfix_operator() {
	switch (lookahead()) {
		case Token::Kind::plus_plus_tok:
		case Token::Kind::minus_minus_tok:
		case Token::Kind::lparen_tok:
		case Token::Kind::lbrace_tok:
		case Token::Kind::dot_tok:
		case Token::Kind::rarrow_tok:
			return consume();

		default:
			return Token{};
	}
}
