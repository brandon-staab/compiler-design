#include "lexer/Token.hpp"

#include <cassert>
#include <iostream>


Token::Token() : Token(Kind::eof_tok, Symbol(), Location()) {}


Token::Token(Kind const kind, Symbol const& sym, Location const& loc) : m_kind(kind), m_lex(sym), m_loc(loc) {}


Token::operator bool() const {
	return m_kind != Kind::eof_tok;
}


Token::Kind Token::get_kind() const {
	return m_kind;
}


Symbol Token::get_lexeme() const {
	return m_lex;
}


Location Token::get_location() const {
	return m_loc;
}


bool Token::is(Kind const kind) const {
	return kind == get_kind();
}


bool Token::is_identifier() const {
	return get_kind() == Kind::identifier_tok;
}


bool Token::is_keyword() const {
	switch (get_kind()) {
		case Kind::bool_tok:
		case Kind::break_tok:
		case Kind::case_tok:
		case Kind::catch_tok:
		case Kind::char_tok:
		case Kind::class_tok:
		case Kind::const_tok:
		case Kind::continue_tok:
		case Kind::delete_tok:
		case Kind::do_tok:
		case Kind::double_tok:
		case Kind::else_tok:
		case Kind::enum_tok:
		case Kind::explicit_tok:
		case Kind::false_tok:
		case Kind::float_tok:
		case Kind::fun_tok:
		case Kind::for_tok:
		case Kind::goto_tok:
		case Kind::if_tok:
		case Kind::int_tok:
		case Kind::let_tok:
		case Kind::long_tok:
		case Kind::new_tok:
		case Kind::namespace_tok:
		case Kind::nullptr_tok:
		case Kind::operator_tok:
		case Kind::private_tok:
		case Kind::protected_tok:
		case Kind::public_tok:
		case Kind::reinterpret_cast_tok:
		case Kind::ref_tok:
		case Kind::return_tok:
		case Kind::short_tok:
		case Kind::signed_tok:
		case Kind::sizeof_tok:
		case Kind::static_assert_tok:
		case Kind::static_cast_tok:
		case Kind::struct_tok:
		case Kind::switch_tok:
		case Kind::throw_tok:
		case Kind::true_tok:
		case Kind::try_tok:
		case Kind::union_tok:
		case Kind::unsigned_tok:
		case Kind::using_tok:
		case Kind::var_tok:
		case Kind::void_tok:
		case Kind::while_tok:
			return true;
		default:
			return false;
	}
}


bool Token::is_literal() const {
	switch (get_kind()) {
		case Kind::integer_literal_tok:
		case Kind::float_literal_tok:
			return true;
		default:
			return false;
	}
}


bool Token::is_punctuator() const {
	switch (get_kind()) {
		case Kind::lbrace_tok:
		case Kind::rbrace_tok:
		case Kind::lbracket_tok:
		case Kind::rbracket_tok:
		case Kind::lparen_tok:
		case Kind::rparen_tok:
		case Kind::semicolon_tok:
		case Kind::colon_tok:
		case Kind::ellipses_tok:
		case Kind::question_tok:
		case Kind::scope_tok:
		case Kind::dot_tok:
		case Kind::dot_star_tok:
		case Kind::rarrow_tok:
		case Kind::rarrow_star_tok:
		case Kind::comma_tok:
			return true;
		default:
			return false;
	}
}


bool Token::is_operator() const {
	switch (get_kind()) {
		case Kind::tilde_tok:
		case Kind::bang_tok:
		case Kind::plus_tok:
		case Kind::minus_tok:
		case Kind::star_tok:
		case Kind::slash_tok:
		case Kind::percent_tok:
		case Kind::caret_tok:
		case Kind::ampersand_tok:
		case Kind::bar_tok:
		case Kind::eq_tok:
		case Kind::plus_eq_tok:
		case Kind::minus_eq_tok:
		case Kind::star_eq_tok:
		case Kind::slash_eq_tok:
		case Kind::percent_eq_tok:
		case Kind::caret_eq_tok:
		case Kind::ampersand_eq_tok:
		case Kind::bar_eq_tok:
		case Kind::eq_eq_tok:
		case Kind::bang_eq_tok:
		case Kind::less_tok:
		case Kind::greater_tok:
		case Kind::less_eq_tok:
		case Kind::greater_eq_tok:
		case Kind::spaceship_tok:
		case Kind::ampersand_ampersand_tok:
		case Kind::bar_bar_tok:
		case Kind::less_less_tok:
		case Kind::greater_greater_tok:
		case Kind::less_less_eq_tok:
		case Kind::greater_greater_eq_tok:
		case Kind::plus_plus_tok:
		case Kind::minus_minus_tok:
		case Kind::and_tok:
		case Kind::or_tok:
		case Kind::xor_tok:
		case Kind::not_tok:
		case Kind::bitand_tok:
		case Kind::bitor_tok:
		case Kind::and_eq_tok:
		case Kind::or_eq_tok:
		case Kind::xor_eq_tok:
		case Kind::not_eq_tok:
			return true;
		default:
			return false;
	}
}

/*************************************************************/

std::ostream& operator<<(std::ostream& os, Token const& token) {
	os << token.get_kind();

	if (token.is_identifier() || token.is_literal()) {
		os << ": " << token.get_lexeme();
	}

	return os << ' ' << token.get_location();
}


std::ostream& operator<<(std::ostream& os, Token::Kind const kind) {
	return os << kind_to_string(kind);
}

/*****************************/

char const* const kind_to_string(Token::Kind const kind) {
	switch (kind) {
		case Token::Kind::eof_tok:
			return "eof";
		case Token::Kind::identifier_tok:
			return "identifier";
		case Token::Kind::bool_tok:
			return "bool";
		case Token::Kind::break_tok:
			return "break";
		case Token::Kind::case_tok:
			return "case";
		case Token::Kind::catch_tok:
			return "catch";
		case Token::Kind::char_tok:
			return "char";
		case Token::Kind::class_tok:
			return "class";
		case Token::Kind::const_tok:
			return "const";
		case Token::Kind::continue_tok:
			return "continue";
		case Token::Kind::delete_tok:
			return "delete";
		case Token::Kind::do_tok:
			return "do";
		case Token::Kind::double_tok:
			return "double";
		case Token::Kind::else_tok:
			return "else";
		case Token::Kind::enum_tok:
			return "enum";
		case Token::Kind::explicit_tok:
			return "explicit";
		case Token::Kind::false_tok:
			return "false";
		case Token::Kind::float_tok:
			return "float";
		case Token::Kind::for_tok:
			return "for";
		case Token::Kind::fun_tok:
			return "fun";
		case Token::Kind::goto_tok:
			return "goto";
		case Token::Kind::if_tok:
			return "if";
		case Token::Kind::int_tok:
			return "int";
		case Token::Kind::let_tok:
			return "let";
		case Token::Kind::long_tok:
			return "long";
		case Token::Kind::new_tok:
			return "new";
		case Token::Kind::namespace_tok:
			return "namespace";
		case Token::Kind::nullptr_tok:
			return "nullptr";
		case Token::Kind::operator_tok:
			return "operator";
		case Token::Kind::private_tok:
			return "private";
		case Token::Kind::protected_tok:
			return "protected";
		case Token::Kind::public_tok:
			return "public";
		case Token::Kind::reinterpret_cast_tok:
			return "reinterpret_cast";
		case Token::Kind::ref_tok:
			return "ref";
		case Token::Kind::return_tok:
			return "return";
		case Token::Kind::short_tok:
			return "short";
		case Token::Kind::signed_tok:
			return "signed";
		case Token::Kind::sizeof_tok:
			return "sizeof";
		case Token::Kind::static_assert_tok:
			return "static_assert";
		case Token::Kind::static_cast_tok:
			return "static_cast";
		case Token::Kind::struct_tok:
			return "struct";
		case Token::Kind::switch_tok:
			return "switch";
		case Token::Kind::throw_tok:
			return "throw";
		case Token::Kind::true_tok:
			return "true";
		case Token::Kind::try_tok:
			return "try";
		case Token::Kind::union_tok:
			return "union";
		case Token::Kind::unsigned_tok:
			return "unsigned";
		case Token::Kind::using_tok:
			return "using";
		case Token::Kind::var_tok:
			return "var";
		case Token::Kind::void_tok:
			return "void";
		case Token::Kind::while_tok:
			return "while";
		case Token::Kind::integer_literal_tok:
			return "integer_literal";
		case Token::Kind::float_literal_tok:
			return "float_literal";
		case Token::Kind::lbrace_tok:
			return "{";
		case Token::Kind::rbrace_tok:
			return "}";
		case Token::Kind::lbracket_tok:
			return "[";
		case Token::Kind::rbracket_tok:
			return "]";
		case Token::Kind::lparen_tok:
			return "(";
		case Token::Kind::rparen_tok:
			return ")";
		case Token::Kind::semicolon_tok:
			return ";";
		case Token::Kind::colon_tok:
			return ":";
		case Token::Kind::ellipses_tok:
			return "...";
		case Token::Kind::question_tok:
			return "?";
		case Token::Kind::scope_tok:
			return "::";
		case Token::Kind::dot_tok:
			return ".";
		case Token::Kind::dot_star_tok:
			return ".*";
		case Token::Kind::rarrow_tok:
			return "->";
		case Token::Kind::rarrow_star_tok:
			return "->*";
		case Token::Kind::comma_tok:
			return ",";
		case Token::Kind::tilde_tok:
			return "~";
		case Token::Kind::bang_tok:
			return "!";
		case Token::Kind::plus_tok:
			return "+";
		case Token::Kind::minus_tok:
			return "-";
		case Token::Kind::star_tok:
			return "*";
		case Token::Kind::slash_tok:
			return "/";
		case Token::Kind::percent_tok:
			return "%";
		case Token::Kind::caret_tok:
			return "^";
		case Token::Kind::ampersand_tok:
			return "&";
		case Token::Kind::bar_tok:
			return "|";
		case Token::Kind::eq_tok:
			return "=";
		case Token::Kind::plus_eq_tok:
			return "+=";
		case Token::Kind::minus_eq_tok:
			return "-=";
		case Token::Kind::star_eq_tok:
			return "*=";
		case Token::Kind::slash_eq_tok:
			return "/=";
		case Token::Kind::percent_eq_tok:
			return "%=";
		case Token::Kind::caret_eq_tok:
			return "^=";
		case Token::Kind::ampersand_eq_tok:
			return "&=";
		case Token::Kind::bar_eq_tok:
			return "|=";
		case Token::Kind::eq_eq_tok:
			return "==";
		case Token::Kind::bang_eq_tok:
			return "!=";
		case Token::Kind::less_tok:
			return "<";
		case Token::Kind::greater_tok:
			return ">";
		case Token::Kind::less_eq_tok:
			return "<=";
		case Token::Kind::greater_eq_tok:
			return ">=";
		case Token::Kind::spaceship_tok:
			return "<=>";
		case Token::Kind::ampersand_ampersand_tok:
			return "&&";
		case Token::Kind::bar_bar_tok:
			return "||";
		case Token::Kind::less_less_tok:
			return "<<";
		case Token::Kind::greater_greater_tok:
			return ">>";
		case Token::Kind::less_less_eq_tok:
			return "<<=";
		case Token::Kind::greater_greater_eq_tok:
			return ">>=";
		case Token::Kind::plus_plus_tok:
			return "++";
		case Token::Kind::minus_minus_tok:
			return "--";
		case Token::Kind::and_tok:
			return "and";
		case Token::Kind::or_tok:
			return "or";
		case Token::Kind::xor_tok:
			return "xor";
		case Token::Kind::not_tok:
			return "not";
		case Token::Kind::bitand_tok:
			return "bitand";
		case Token::Kind::bitor_tok:
			return "bitor";
		case Token::Kind::and_eq_tok:
			return "and_eq";
		case Token::Kind::or_eq_tok:
			return "or_eq";
		case Token::Kind::xor_eq_tok:
			return "xor_eq";
		case Token::Kind::not_eq_tok:
			return "not_eq";
	}

	assert(false);	// All token kinds must be defined
}
