#include "lexer/Location.hpp"

#include <iostream>


Location::Location() : m_line_number(0), m_column_number(0) {}


Location::Location(std::size_t const line_number, std::size_t const column_number) : m_line_number(line_number), m_column_number(column_number) {}


std::size_t Location::get_line_num() const {
	return m_line_number;
}


std::size_t Location::get_column_num() const {
	return m_column_number;
}

/*************************************************************/

std::ostream& operator<<(std::ostream& os, Location const& location) {
	os << '[' << location.get_line_num() << ':' << location.get_column_num() << ']';
}
