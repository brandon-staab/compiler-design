#include "lexer/SymbolTable.hpp"

#include <cassert>


Symbol SymbolTable::get(Name const& name) {
	assert(!name.empty());

	return &*emplace(name).first;
}
