#include "lexer/Lexer.hpp"

#include <cassert>
#include <cctype>
#include <iostream>


namespace {
	bool is_nondigit(char c) { return std::isalpha(c) || c == '_'; }


	bool is_digit(char c) { return std::isdigit(c); }


	bool is_nondigit_or_digit(char c) { return is_nondigit(c) || is_digit(c); }


	bool is_bindigit(char c) { return c == '0' || c == '1'; }


	bool is_hexdigit(char c) { return std::isxdigit(c); }
}  // namespace


Lexer::Lexer(SymbolTable& syms, char const* first, char const* limit) : m_syms(&syms), m_cursor(first), m_limit(limit), m_line_number(1), m_column_number(0) {
	// Keywords
	m_keywords.emplace("bool", Token::Kind::bool_tok);
	m_keywords.emplace("break", Token::Kind::break_tok);
	m_keywords.emplace("case", Token::Kind::case_tok);
	m_keywords.emplace("catch", Token::Kind::catch_tok);
	m_keywords.emplace("char", Token::Kind::char_tok);
	m_keywords.emplace("class", Token::Kind::class_tok);
	m_keywords.emplace("const", Token::Kind::const_tok);
	m_keywords.emplace("continue", Token::Kind::continue_tok);
	m_keywords.emplace("delete", Token::Kind::delete_tok);
	m_keywords.emplace("do", Token::Kind::do_tok);
	m_keywords.emplace("double", Token::Kind::double_tok);
	m_keywords.emplace("else", Token::Kind::else_tok);
	m_keywords.emplace("enum", Token::Kind::enum_tok);
	m_keywords.emplace("explicit", Token::Kind::explicit_tok);
	m_keywords.emplace("false", Token::Kind::false_tok);
	m_keywords.emplace("float", Token::Kind::float_tok);
	m_keywords.emplace("for", Token::Kind::for_tok);
	m_keywords.emplace("fun", Token::Kind::fun_tok);
	m_keywords.emplace("goto", Token::Kind::goto_tok);
	m_keywords.emplace("if", Token::Kind::if_tok);
	m_keywords.emplace("int", Token::Kind::int_tok);
	m_keywords.emplace("let", Token::Kind::let_tok);
	m_keywords.emplace("long", Token::Kind::long_tok);
	m_keywords.emplace("new", Token::Kind::new_tok);
	m_keywords.emplace("namespace", Token::Kind::namespace_tok);
	m_keywords.emplace("nullptr", Token::Kind::nullptr_tok);
	m_keywords.emplace("operator", Token::Kind::operator_tok);
	m_keywords.emplace("private", Token::Kind::private_tok);
	m_keywords.emplace("protected", Token::Kind::protected_tok);
	m_keywords.emplace("public", Token::Kind::public_tok);
	m_keywords.emplace("ref", Token::Kind::ref_tok);
	m_keywords.emplace("reinterpret_cast", Token::Kind::reinterpret_cast_tok);
	m_keywords.emplace("return", Token::Kind::return_tok);
	m_keywords.emplace("short", Token::Kind::short_tok);
	m_keywords.emplace("signed", Token::Kind::signed_tok);
	m_keywords.emplace("sizeof", Token::Kind::sizeof_tok);
	m_keywords.emplace("static_assert", Token::Kind::static_assert_tok);
	m_keywords.emplace("static_cast", Token::Kind::static_cast_tok);
	m_keywords.emplace("struct", Token::Kind::struct_tok);
	m_keywords.emplace("switch", Token::Kind::switch_tok);
	m_keywords.emplace("throw", Token::Kind::throw_tok);
	m_keywords.emplace("true", Token::Kind::true_tok);
	m_keywords.emplace("try", Token::Kind::try_tok);
	m_keywords.emplace("union", Token::Kind::union_tok);
	m_keywords.emplace("unsigned", Token::Kind::unsigned_tok);
	m_keywords.emplace("using", Token::Kind::using_tok);
	m_keywords.emplace("var", Token::Kind::var_tok);
	m_keywords.emplace("void", Token::Kind::void_tok);
	m_keywords.emplace("while", Token::Kind::while_tok);

	// Operators
	m_keywords.emplace("and", Token::Kind::and_tok);
	m_keywords.emplace("and_eq", Token::Kind::and_eq_tok);
	m_keywords.emplace("bitand", Token::Kind::bitand_tok);
	m_keywords.emplace("bitor", Token::Kind::bitor_tok);
	m_keywords.emplace("compl", Token::Kind::tilde_tok);
	m_keywords.emplace("not", Token::Kind::not_tok);
	m_keywords.emplace("not_eq", Token::Kind::not_eq_tok);
	m_keywords.emplace("or", Token::Kind::or_tok);
	m_keywords.emplace("or_eq", Token::Kind::or_eq_tok);
	m_keywords.emplace("xor", Token::Kind::xor_tok);
	m_keywords.emplace("xor_eq", Token::Kind::xor_eq_tok);
}


Lexer::Lexer(SymbolTable& syms, std::string const& str) : Lexer(syms, str.data(), str.data() + str.size()) {}


Token Lexer::operator()() {
	return get_next_token();
}

/*****************************/

Token Lexer::get_next_token() {
	while (true) {
		++m_column_number;	// FIXME: in the case of tokens longer than one symbol
		switch (peek()) {
			case ' ':
			case '\r':
			case '\t':
				consume();
				continue;

			case '\n':
				++m_line_number;
				m_column_number = 0;
				consume();
				continue;

			case '#':
				consume_comment();
				continue;

			case '\0':
				return match(Token::Kind::eof_tok, 1);

			case '{':
				return match(Token::Kind::lbrace_tok, 1);
			case '}':
				return match(Token::Kind::rbrace_tok, 1);
			case '[':
				return match(Token::Kind::lbracket_tok, 1);
			case ']':
				return match(Token::Kind::rbracket_tok, 1);
			case '(':
				return match(Token::Kind::lparen_tok, 1);
			case ')':
				return match(Token::Kind::rparen_tok, 1);
			case ';':
				return match(Token::Kind::semicolon_tok, 1);
			case ':':
				if (peek(1) == ':') {
					return match(Token::Kind::scope_tok, 2);
				} else {
					return match(Token::Kind::colon_tok, 1);
				}
			case '?':
				return match(Token::Kind::question_tok, 1);
			case '.':
				if (peek(1) == '.' && peek(2) == '.') {
					return match(Token::Kind::ellipses_tok, 3);
				} else if (peek(1) == '*') {
					return match(Token::Kind::dot_star_tok, 2);
				} else {
					return match(Token::Kind::dot_tok, 1);
				}
			case ',':
				return match(Token::Kind::comma_tok, 1);
			case '~':
				return match(Token::Kind::tilde_tok, 1);
			case '!':
				if (peek(1) == '=') {
					return match(Token::Kind::bang_eq_tok, 2);
				} else {
					return match(Token::Kind::bang_tok, 1);
				}
			case '+':
				if (peek(1) == '=') {
					return match(Token::Kind::plus_eq_tok, 2);
				} else if (peek(1) == '+') {
					return match(Token::Kind::plus_plus_tok, 2);
				} else {
					return match(Token::Kind::plus_tok, 1);
				}
			case '-':
				if (peek(1) == '>') {
					if (peek(2) == '*') {
						return match(Token::Kind::rarrow_star_tok, 3);
					} else {
						return match(Token::Kind::rarrow_tok, 2);
					}
				} else if (peek(1) == '=') {
					return match(Token::Kind::minus_eq_tok, 2);
				} else if (peek(1) == '-') {
					return match(Token::Kind::minus_minus_tok, 2);
				} else {
					return match(Token::Kind::minus_tok, 1);
				}
			case '*':
				if (peek(1) == '=') {
					return match(Token::Kind::star_eq_tok, 2);
				} else {
					return match(Token::Kind::star_tok, 1);
				}
			case '/':
				if (peek(1) == '=') {
					return match(Token::Kind::slash_eq_tok, 2);
				} else {
					return match(Token::Kind::slash_tok, 1);
				}
			case '%':
				if (peek(1) == '=') {
					return match(Token::Kind::percent_eq_tok, 2);
				} else {
					return match(Token::Kind::percent_tok, 1);
				}
			case '^':
				if (peek(1) == '=') {
					return match(Token::Kind::caret_eq_tok, 2);
				} else {
					return match(Token::Kind::caret_tok, 1);
				}
			case '&':
				if (peek(1) == '=') {
					return match(Token::Kind::ampersand_eq_tok, 2);
				} else if (peek(1) == '&') {
					return match(Token::Kind::ampersand_ampersand_tok, 2);
				} else {
					return match(Token::Kind::ampersand_tok, 1);
				}
			case '|':
				if (peek(1) == '=') {
					return match(Token::Kind::bar_eq_tok, 2);
				}
				if (peek(1) == '|') {
					return match(Token::Kind::bar_bar_tok, 2);
				} else {
					return match(Token::Kind::bar_tok, 1);
				}
			case '=':
				if (peek(1) == '=') {
					return match(Token::Kind::eq_eq_tok, 2);
				} else {
					return match(Token::Kind::eq_tok, 1);
				}
			case '<':
				if (peek(1) == '=') {
					if (peek(2) == '>') {
						return match(Token::Kind::spaceship_tok, 3);
					} else {
						return match(Token::Kind::less_eq_tok, 2);
					}
				} else if (peek(1) == '<') {
					if (peek(2) == '=') {
						return match(Token::Kind::less_less_eq_tok, 3);
					} else {
						return match(Token::Kind::less_less_tok, 2);
					}
				} else {
					return match(Token::Kind::less_tok, 1);
				}
			case '>':
				if (peek(1) == '=') {
					return match(Token::Kind::greater_eq_tok, 2);
				} else if (peek(1) == '>') {
					if (peek(2) == '=') {
						return match(Token::Kind::greater_greater_eq_tok, 3);
					} else {
						return match(Token::Kind::greater_greater_tok, 2);
					}
				} else {
					return match(Token::Kind::greater_tok, 1);
				}
			default:
				if (is_nondigit(*m_cursor)) {
					return match_word();
				} else if (is_digit(*m_cursor)) {
					return match_number();
				} else {
					error();
					consume();
					continue;
				}
		}
	}
}


bool Lexer::is_eof() const {
	return is_eof(m_cursor);
}


bool Lexer::is_eof(char const* ptr) const {
	return ptr == m_limit;
}


char Lexer::peek() const {
	if (is_eof()) {
		return '\0';
	}

	return *m_cursor;
}


char Lexer::peek(std::size_t const n) const {
	if (static_cast<std::size_t>(m_limit - m_cursor) <= n) {
		return '\0';
	}

	return *(m_cursor + n);
}


char Lexer::consume() {
	assert(m_cursor != m_limit);

	return *m_cursor++;
}


void Lexer::consume_comment() {
	while (!is_eof(m_cursor) && *m_cursor != '\n') {
		++m_cursor;
	}
}


Token Lexer::match(Token::Kind const kind, std::size_t const len) {
	auto str = std::string{m_cursor, m_cursor + len};
	m_cursor += len;

	return Token{kind, m_syms->get(str), Location{m_line_number, m_column_number}};
}


Token Lexer::match_number() {
	char const* cursor = m_cursor;
	auto kind = Token::Kind::integer_literal_tok;

	number(cursor, kind);

	auto number = std::string{m_cursor, cursor};
	m_cursor = cursor;

	return Token{kind, m_syms->get(number), Location{m_line_number, m_column_number}};
}


Token Lexer::match_word() {
	char const* cursor = m_cursor + 1;
	while (!is_eof(cursor) && is_nondigit_or_digit(*cursor)) {
		++cursor;
	}

	auto id = std::string{m_cursor, cursor};
	m_cursor = cursor;

	auto kind = Token::Kind::identifier_tok;
	auto iter = m_keywords.find(id);
	if (iter != m_keywords.end()) {
		kind = iter->second;
	}

	return Token{kind, m_syms->get(id), Location{m_line_number, m_column_number}};
}


void Lexer::error() {
	std::cerr << "lexing error: invalid character on line " << m_line_number << ", column " << m_column_number << ", found: " << *m_cursor << '\n';
}

/*****************************/

void Lexer::number(char const*& cursor, Token::Kind& kind) {
	if (*cursor == '0') {
		++cursor;
		if (!is_eof(cursor) && (*cursor == 'x' || *cursor == 'X')) {
			hexadecimal_digits(++cursor);
			return;
		} else if (!is_eof(cursor) && (*cursor == 'b' || *cursor == 'B')) {
			binary_digits(++cursor);
			return;
		}
	}

	digits(cursor);

	if (!is_eof(cursor) && *cursor == '.' && !is_eof(cursor + 1) && is_digit(*(cursor + 1))) {
		cursor += 2;
		float_literal_rest(cursor, kind);
	}
}


void Lexer::binary_digits(char const*& cursor) {
	while (!is_eof(cursor) && is_bindigit(*cursor)) {
		++cursor;
	}
}


void Lexer::digits(char const*& cursor) {
	while (!is_eof(cursor) && is_digit(*cursor)) {
		++cursor;
	}
}


void Lexer::hexadecimal_digits(char const*& cursor) {
	while (!is_eof(cursor) && is_hexdigit(*cursor)) {
		++cursor;
	}
}


void Lexer::float_literal_rest(char const*& cursor, Token::Kind& kind) {
	kind = Token::Kind::float_literal_tok;

	digits(cursor);

	if (*cursor == 'e' || *cursor == 'E') {
		++cursor;
		exponent_rest(cursor);
	}
}


void Lexer::exponent_rest(char const*& cursor) {
	if (*cursor == '+' || *cursor == '-') {
		++cursor;
	}

	digits(cursor);
}
