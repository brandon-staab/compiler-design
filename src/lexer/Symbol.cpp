#include "lexer/Symbol.hpp"

#include <cassert>

Symbol::Symbol() : m_name() {}


Symbol::Symbol(Name const* name) : m_name(name) {
	assert(m_name != nullptr);
}


Name const& Symbol::name() const {
	assert(m_name != nullptr);

	return *m_name;
}

/*************************************************************/

bool operator==(Symbol const& lhs, Symbol const& rhs) {
	return lhs.m_name == rhs.m_name;
}


bool operator!=(Symbol const& lhs, Symbol const& rhs) {
	return lhs.m_name != rhs.m_name;
}

/*****************************/

std::ostream& operator<<(std::ostream& os, Symbol const& symbol) {
	os << symbol.name();
}
