#include "statements/Unary_stmt.hpp"

#include "expressions/expr_incl.hpp"


Unary_stmt::Unary_stmt(Kind const kind, Statement* const stmt) : Fixed_Arity_stmt(kind, stmt) {}

/*************************************************************/

When_stmt::When_stmt(Expression const* condition, Statement* const stmt) : Unary_stmt(Kind::when_stmt, stmt), m_condition(condition) {
	assert(condition != nullptr);
}


Expression const* const When_stmt::get_condition() const {
	assert(m_condition != nullptr);

	return m_condition;
}


Statement const* const When_stmt::get_stmt() const {
	assert(get_operand(0) != nullptr);

	return get_operand(0);
}

/*****************************/

While_stmt::While_stmt(Expression const* condition, Statement* const stmt) : Unary_stmt(Kind::while_stmt, stmt), m_condition(condition) {
	assert(condition != nullptr);
}


Expression const* const While_stmt::get_condition() const {
	assert(m_condition != nullptr);

	return m_condition;
}


Statement const* const While_stmt::get_stmt() const {
	assert(get_operand(0) != nullptr);

	return get_operand(0);
}
