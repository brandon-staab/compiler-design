#include "statements/Binary_stmt.hpp"

#include "expressions/expr_incl.hpp"


Binary_stmt::Binary_stmt(Kind const kind, Statement* const s1, Statement* const s2) : Fixed_Arity_stmt(kind, s1, s2) {}

/*************************************************************/

If_stmt::If_stmt(Expression* const condition, Statement* const true_stmt, Statement* const false_stmt)
	: Binary_stmt(Kind::if_stmt, true_stmt, false_stmt), m_condition(condition) {
	assert(condition != nullptr);
}


Expression const* const If_stmt::get_condition() const {
	assert(m_condition != nullptr);

	return m_condition;
}


Statement const* const If_stmt::get_true_stmt() const {
	assert(get_operand(0) != nullptr);

	return get_operand(0);
}


Statement const* const If_stmt::get_false_stmt() const {
	assert(get_operand(1) != nullptr);

	return get_operand(1);
}
