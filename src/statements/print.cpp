#include "statements/stmt_incl.hpp"

#include "expressions/expr_incl.hpp"
#include "utilities/insertIndent.hpp"

#include <iostream>


namespace {
	void print_Expression_stmt(std::ostream& os, Expression_stmt const* const stmt, std::size_t const depth) {
		insertIndent(os, depth);
		print(os, stmt->get_expr(), depth + 1);
		os << ';';
	}


	void print_Declaration_stmt(std::ostream& os, Declaration_stmt const* const stmt, std::size_t const depth) {
		insertIndent(os, depth);
		print(os, stmt->get_decl(), depth + 1);
		os << ';';
	}


	void print_Return_stmt(std::ostream& os, Return_stmt const* const stmt, std::size_t const depth) {
		insertIndent(os, depth);
		os << "return ";
		print(os, stmt->get_expr(), depth + 1);
		os << ';';
	}


	void print_When_stmt(std::ostream& os, When_stmt const* const stmt, std::size_t const depth) {
		insertIndent(os, depth);
		os << "if (";
		print(os, stmt->get_condition(), depth);
		os << ") {\n";
		print(os, stmt->get_stmt(), depth + 1);
		os << '\n';
		insertIndent(os, depth);
		os << "}\n";
	}


	void print_While_stmt(std::ostream& os, While_stmt const* const stmt, std::size_t const depth) {
		insertIndent(os, depth);
		os << "while (";
		print(os, stmt->get_condition(), depth);
		os << ") do {\n";
		print(os, stmt->get_stmt(), depth + 1);
		os << '\n';
		insertIndent(os, depth);
		os << "}\n";
	}


	void print_If_stmt(std::ostream& os, If_stmt const* const stmt, std::size_t const depth) {
		insertIndent(os, depth);
		os << "if (";
		print(os, stmt->get_condition(), depth + 1);
		os << ") {\n";
		print(os, stmt->get_true_stmt(), depth + 1);
		os << '\n';
		insertIndent(os, depth);
		os << "} else {\n";
		print(os, stmt->get_false_stmt(), depth + 1);
		os << '\n';
		insertIndent(os, depth);
		os << "}\n";
	}


	void print_Block_stmt(std::ostream& os, Block_stmt const* const stmt, std::size_t const depth) {
		insertIndent(os, depth);
		os << "{\n";
		auto const indent = depth + 1;
		for (auto s = stmt->begin(); s != stmt->end(); s++) {
			print(os, *s, indent);
			os << '\n';
		}
		insertIndent(os, depth);
		os << "}\n";
	}
}  // namespace


void print(std::ostream& os, Statement const* const stmt, std::size_t depth) {
	assert(stmt != nullptr);
	assert(depth < 256);

	switch (stmt->get_kind()) {
		case Statement::Kind::skip_stmt:
			insertIndent(os, depth);
			os << "skip;";
			return;
		case Statement::Kind::break_stmt:
			insertIndent(os, depth);
			os << "break;";
			return;
		case Statement::Kind::continue_stmt:
			insertIndent(os, depth);
			os << "continue;";
			return;
		case Statement::Kind::declaration_stmt:
			return print_Declaration_stmt(os, static_cast<Declaration_stmt const* const>(stmt), depth);
		case Statement::Kind::expression_stmt:
			return print_Expression_stmt(os, static_cast<Expression_stmt const* const>(stmt), depth);
		case Statement::Kind::return_stmt:
			return print_Return_stmt(os, static_cast<Return_stmt const* const>(stmt), depth);
		case Statement::Kind::when_stmt:
			return print_When_stmt(os, static_cast<When_stmt const* const>(stmt), depth);
		case Statement::Kind::while_stmt:
			return print_While_stmt(os, static_cast<While_stmt const* const>(stmt), depth);
		case Statement::Kind::if_stmt:
			return print_If_stmt(os, static_cast<If_stmt const* const>(stmt), depth);
		case Statement::Kind::block_stmt:
			return print_Block_stmt(os, static_cast<Block_stmt const* const>(stmt), depth);
	}

	assert(false);	// All statements must be defined
}
