#include "statements/Nullary_stmt.hpp"

#include "expressions/expr_incl.hpp"


Nullary_stmt::Nullary_stmt(Kind const kind) : Fixed_Arity_stmt(kind) {}

/*************************************************************/

Skip_stmt::Skip_stmt() : Nullary_stmt(Kind::skip_stmt) {}

/*****************************/

Break_stmt::Break_stmt() : Nullary_stmt(Kind::break_stmt) {}

/*****************************/

Continue_stmt::Continue_stmt() : Nullary_stmt(Kind::continue_stmt) {}

/*****************************/

Return_stmt::Return_stmt(Expression const* const expr) : Nullary_stmt(Kind::return_stmt), m_expr(expr) {
	assert(m_expr != nullptr);
}


Expression const* Return_stmt::get_expr() const {
	assert(m_expr != nullptr);

	return m_expr;
}

/*****************************/

Expression_stmt::Expression_stmt(Expression const* const expr) : Nullary_stmt(Kind::expression_stmt), m_expr(expr) {
	assert(m_expr != nullptr);
}


Expression const* Expression_stmt::get_expr() const {
	assert(m_expr != nullptr);

	return m_expr;
}

/*****************************/

Declaration_stmt::Declaration_stmt(Declaration const* const decl) : Nullary_stmt(Kind::declaration_stmt), m_decl(decl) {
	assert(m_decl != nullptr);
}


Declaration const* Declaration_stmt::get_decl() const {
	assert(m_decl != nullptr);

	return m_decl;
}
