#include "statements/Statement.hpp"

#include <cassert>
#include <iostream>


Statement::Statement(Kind const kind) : m_kind(kind) {}


Statement::Kind Statement::get_kind() const {
	return m_kind;
}

/*************************************************************/

std::ostream& operator<<(std::ostream& os, Statement const& stmt) {
	print(os, &stmt);
	return os;
}


std::ostream& operator<<(std::ostream& os, Statement::Kind const kind) {
	return os << kind_to_string(kind);
}

/*****************************/

char const* const kind_to_string(Statement::Kind const kind) {
	switch (kind) {
		case Statement::Kind::skip_stmt:
			return "skip_stmt";
		case Statement::Kind::break_stmt:
			return "break_stmt";
		case Statement::Kind::continue_stmt:
			return "continue_stmt";
		case Statement::Kind::declaration_stmt:
			return "declaration_stmt";
		case Statement::Kind::expression_stmt:
			return "expression_stmt";
		case Statement::Kind::return_stmt:
			return "return_stmt";
		case Statement::Kind::when_stmt:
			return "when_stmt";
		case Statement::Kind::while_stmt:
			return "while_stmt";
		case Statement::Kind::if_stmt:
			return "if_stmt";
		case Statement::Kind::block_stmt:
			return "block_stmt";
	}

	assert(false);	// All statement kinds must be defined
}
