#include "statements/Multiary_stmt.hpp"


Statement const* Multiary_stmt::get_stmt(std::size_t const i) const {
	return get_operand(i);
}

Multiary_stmt::Multiary_stmt(Kind const kind, Stmt_seq& operands) : Statement(kind), Dynamic_arity_node(operands) {}

/*************************************************************/

Block_stmt::Block_stmt(Stmt_seq& operands) : Multiary_stmt(Kind::block_stmt, operands) {}
