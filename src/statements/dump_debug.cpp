#include "statements/stmt_incl.hpp"

#include "expressions/expr_incl.hpp"
#include "utilities/insertIndent.hpp"

#include <iostream>


namespace {
	void dump_debug_Expression_stmt(std::ostream& os, Expression_stmt const* const stmt, std::size_t const depth) {
		os << kind_to_string(stmt->get_kind());
		dump_debug(os, stmt->get_expr(), depth);
		os << ';';
	}


	void dump_debug_Declaration_stmt(std::ostream& os, Declaration_stmt const* const stmt, std::size_t const depth) {
		os << kind_to_string(stmt->get_kind());
		dump_debug(os, stmt->get_decl(), depth);
		os << ';';
	}


	void dump_debug_Return_stmt(std::ostream& os, Return_stmt const* const stmt, std::size_t const depth) {
		os << kind_to_string(stmt->get_kind());
		dump_debug(os, stmt->get_expr(), depth);
	}


	void dump_debug_When_stmt(std::ostream& os, When_stmt const* const stmt, std::size_t const depth) {
		os << "if (";
		dump_debug(os, stmt->get_condition(), depth);
		os << ") {\n";
		dump_debug(os, stmt->get_stmt(), depth);
		os << '}';
	}


	void dump_debug_While_stmt(std::ostream& os, While_stmt const* const stmt, std::size_t const depth) {
		os << "while (";
		dump_debug(os, stmt->get_condition(), depth);
		os << ") do {\n";
		dump_debug(os, stmt->get_stmt(), depth);
		os << '}';
	}


	void dump_debug_If_stmt(std::ostream& os, If_stmt const* const stmt, std::size_t const depth) {
		os << "if (";
		dump_debug(os, stmt->get_condition(), depth);
		os << ") then {\n";
		dump_debug(os, stmt->get_true_stmt(), depth);
		os << " } else {\n";
		dump_debug(os, stmt->get_false_stmt(), depth);
		os << '}';
	}


	void dump_debug_Block_stmt(std::ostream& os, Block_stmt const* const stmt, std::size_t const depth) {
		os << kind_to_string(stmt->get_kind()) << '\n';
		for (auto s = stmt->begin(); s != stmt->end();) {
			dump_debug(os, *s, depth);

			if (++s != stmt->end()) {
				os << '\n';
			}
		}
	}
}  // namespace


void dump_debug(std::ostream& os, Statement const* const stmt, std::size_t depth) {
	assert(stmt != nullptr);
	assert(depth < 256);

	os << &stmt;
	insertIndent(os, ++depth);

	switch (stmt->get_kind()) {
		case Statement::Kind::skip_stmt:
			os << "skip;";
			return;
		case Statement::Kind::break_stmt:
			os << "break;";
			return;
		case Statement::Kind::continue_stmt:
			os << "continue;";
			return;
		case Statement::Kind::declaration_stmt:
			return dump_debug_Declaration_stmt(os, static_cast<Declaration_stmt const* const>(stmt), depth);
		case Statement::Kind::expression_stmt:
			return dump_debug_Expression_stmt(os, static_cast<Expression_stmt const* const>(stmt), depth);
		case Statement::Kind::return_stmt:
			return dump_debug_Return_stmt(os, static_cast<Return_stmt const* const>(stmt), depth);
		case Statement::Kind::when_stmt:
			return dump_debug_When_stmt(os, static_cast<When_stmt const* const>(stmt), depth);
		case Statement::Kind::while_stmt:
			return dump_debug_While_stmt(os, static_cast<While_stmt const* const>(stmt), depth);
		case Statement::Kind::if_stmt:
			return dump_debug_If_stmt(os, static_cast<If_stmt const* const>(stmt), depth);
		case Statement::Kind::block_stmt:
			return dump_debug_Block_stmt(os, static_cast<Block_stmt const* const>(stmt), depth);
	}

	assert(false);	// All statements must be defined
}
