#include "statements/stmt_incl.hpp"

#include "expressions/expr_incl.hpp"

#include <iostream>


namespace {
	void to_sexpr_Expression_stmt(std::ostream& os, Expression_stmt const* const stmt) {
		os << '(';
		to_sexpr(os, stmt->get_expr());
		os << ")";
	}


	void to_sexpr_Declaration_stmt(std::ostream& os, Declaration_stmt const* const stmt) {
		os << '(';
		to_sexpr(os, stmt->get_decl());
		os << ")";
	}


	void to_sexpr_Return_stmt(std::ostream& os, Return_stmt const* const stmt) {
		os << "(return ";
		to_sexpr(os, stmt->get_expr());
		os << ")";
	}


	void to_sexpr_When_stmt(std::ostream& os, When_stmt const* const stmt) {
		os << "(if ";
		to_sexpr(os, stmt->get_condition());
		os << ' ';
		to_sexpr(os, stmt->get_stmt());
		os << ')';
	}


	void to_sexpr_While_stmt(std::ostream& os, While_stmt const* const stmt) {
		os << "(while ";
		to_sexpr(os, stmt->get_condition());
		os << ' ';
		to_sexpr(os, stmt->get_stmt());
		os << ')';
	}


	void to_sexpr_If_stmt(std::ostream& os, If_stmt const* const stmt) {
		os << "(if ";
		to_sexpr(os, stmt->get_condition());
		os << ' ';
		to_sexpr(os, stmt->get_true_stmt());
		os << ' ';
		to_sexpr(os, stmt->get_false_stmt());
		os << ')';
	}


	void to_sexpr_Block_stmt(std::ostream& os, Block_stmt const* const stmt) {
		for (auto s = stmt->begin(); s != stmt->end();) {
			to_sexpr(os, *s);

			if (++s != stmt->end()) {
				os << ' ';
			}
		}
	}
}  // namespace


void to_sexpr(std::ostream& os, Statement const* const stmt) {
	assert(stmt != nullptr);

	switch (stmt->get_kind()) {
		case Statement::Kind::skip_stmt:
			os << "(skip)";
			return;
		case Statement::Kind::break_stmt:
			os << "(break)";
			return;
		case Statement::Kind::continue_stmt:
			os << "(continue)";
			return;
		case Statement::Kind::expression_stmt:
			return to_sexpr_Expression_stmt(os, static_cast<Expression_stmt const* const>(stmt));
		case Statement::Kind::declaration_stmt:
			return to_sexpr_Declaration_stmt(os, static_cast<Declaration_stmt const* const>(stmt));
		case Statement::Kind::return_stmt:
			return to_sexpr_Return_stmt(os, static_cast<Return_stmt const* const>(stmt));
		case Statement::Kind::when_stmt:
			return to_sexpr_When_stmt(os, static_cast<When_stmt const* const>(stmt));
		case Statement::Kind::while_stmt:
			return to_sexpr_While_stmt(os, static_cast<While_stmt const* const>(stmt));
		case Statement::Kind::if_stmt:
			return to_sexpr_If_stmt(os, static_cast<If_stmt const* const>(stmt));
		case Statement::Kind::block_stmt:
			return to_sexpr_Block_stmt(os, static_cast<Block_stmt const* const>(stmt));
	}

	assert(false);	// All statements must be defined
}
