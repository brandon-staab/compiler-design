set(
	"CORE_SOURCE"

	#########################
	#        General        #
	#########################

	# Lexer
	"src/lexer/Lexer.cpp"
	"src/lexer/Location.cpp"
	"src/lexer/Symbol.cpp"
	"src/lexer/SymbolTable.cpp"
	"src/lexer/Token.cpp"

	# Parser
	"src/parser/Parser.cpp"
	"src/parser/Parser_consumption.cpp"
	"src/parser/Parser_decl.cpp"
	"src/parser/Parser_expr.cpp"
	"src/parser/Parser_stmt.cpp"
	"src/parser/Parser_type.cpp"

	# Semantic Actions
	"src/semantics/SemanticActions.cpp"
	"src/semantics/SemanticActions_decl.cpp"
	"src/semantics/SemanticActions_expr.cpp"
	"src/semantics/SemanticActions_stmt.cpp"
	"src/semantics/SemanticActions_type.cpp"
	"src/semantics/Scope.cpp"

	# Builder
	"src/builder/builder.cpp"
	"src/builder/builder_check.cpp"
	"src/builder/builder_convert.cpp"
	"src/builder/builder_init.cpp"
	"src/builder/builder_decl.cpp"
	"src/builder/builder_expr.cpp"
	"src/builder/builder_stmt.cpp"
	"src/builder/builder_type.cpp"

	# Call Stack
	"src/call_stack/Call_stack.cpp"
	"src/call_stack/Frame.cpp"

	# Components
	"src/components/Value.cpp"

	# Evaluation
	"src/evaluation/Evaluation.cpp"
	"src/evaluation/eval_decl.cpp"
	"src/evaluation/eval_expr.cpp"
	"src/evaluation/eval_stmt.cpp"

	# Store
	"src/store/Monotonic_store.cpp"

	#########################################
	#        Abstract Semantic Graph        #
	#########################################

	# Declarations
	"src/declarations/Declaration.cpp"
	"src/declarations/Nullary_decl.cpp"
	"src/declarations/Multiary_decl.cpp"
	"src/declarations/dump_debug.cpp"
	"src/declarations/print.cpp"
	"src/declarations/to_sexpr.cpp"

	# Expressions
	"src/expressions/Expression.cpp"
	"src/expressions/Nullary_expr.cpp"
	"src/expressions/Unary_expr.cpp"
	"src/expressions/Binary_expr.cpp"
	"src/expressions/Ternary_expr.cpp"
	"src/expressions/Multiary_expr.cpp"
	"src/expressions/dump_debug.cpp"
	"src/expressions/print.cpp"
	"src/expressions/to_sexpr.cpp"

	# Statements
	"src/statements/Statement.cpp"
	"src/statements/Nullary_stmt.cpp"
	"src/statements/Unary_stmt.cpp"
	"src/statements/Binary_stmt.cpp"
	"src/statements/Multiary_stmt.cpp"
	"src/statements/dump_debug.cpp"
	"src/statements/print.cpp"
	"src/statements/to_sexpr.cpp"

	# Types
	"src/types/Type.cpp"
	"src/types/Nullary_type.cpp"
	"src/types/Unary_type.cpp"
	"src/types/Multiary_type.cpp"
	"src/types/dump_debug.cpp"
	"src/types/print.cpp"
	"src/types/to_sexpr.cpp"

	###########################
	#        Utilities        #
	###########################
	"src/utilities/insertIndent.cpp"
	"src/utilities/load_file.cpp"
)
