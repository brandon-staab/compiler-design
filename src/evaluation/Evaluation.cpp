#include "evaluation/Evaluation.hpp"

#include "declarations/decl_incl.hpp"
#include "types/type_incl.hpp"


namespace {
	Monotonic_store& get_store(Evaluation& eval, int store) {
		if (store == -1) {
			return eval.get_globals();
		} else {
			return eval.get_stack().get_frame(store)->get_locals();
		}
	}
}  // namespace


Monotonic_store& Evaluation::get_globals() {
	return m_globals;
}


Object* Evaluation::allocate_static(Declaration const* decl) {
	return m_globals.allocate(decl);
}


Object* Evaluation::allocate_automatic(Declaration const* decl) {
	return get_current_frame()->allocate_local(decl);
}


Object* Evaluation::locate_object(Value const& val) {
	Address addr = val.get_address();
	Monotonic_store& store = get_store(*this, addr.store);
	return store.locate(addr.def);
}


Call_stack& Evaluation::get_stack() {
	return m_stack;
}


Declaration const* Evaluation::get_current_function() const {
	assert(get_current_frame() != nullptr);

	return get_current_frame()->get_function();
}


Frame* Evaluation::get_current_frame() const {
	return m_stack.get_top();
}


Frame* Evaluation::push_frame(Declaration const* func) {
	assert(func != nullptr);
	assert(func->is_function());

	auto fn = static_cast<Function_decl const*>(func);
	Frame* frame = m_stack.push(fn);

	for (Declaration const* local : *fn) {
		if (!local->is_variable()) {
			continue;
		}

		auto var = static_cast<Variable_decl const*>(local);
		frame->allocate_local(var);
	}

	return frame;
}


void Evaluation::pop_frame() {
	assert(!m_stack.empty());

	m_stack.pop();
}
