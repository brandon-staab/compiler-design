#include "utilities/load_file.hpp"

#include <fstream>
#include <string>


std::string load_file(char const* filename) {
	auto ifs = std::ifstream{filename};
	auto first = std::istreambuf_iterator<char>{ifs};
	auto limit = std::istreambuf_iterator<char>{};

	return std::string{first, limit};
}
