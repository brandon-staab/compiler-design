#include "utilities/insertIndent.hpp"

#include <iomanip>


void insertIndent(std::ostream& os, std::size_t const amount) {
	if (amount != 0) {
		os << std::setw(2 * amount) << ' ';
	}
}
