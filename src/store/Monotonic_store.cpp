#include "store/Monotonic_store.hpp"

#include "declarations/decl_incl.hpp"


static inline bool contains_object(std::vector<Object> const& store, Object const* obj) {
	assert(obj != nullptr);

	auto iter = std::find_if(store.begin(), store.end(), [obj](Object const& x) { return &x == obj; });
	return iter != store.end();
}

/*************************************************************/

Object* const Monotonic_store::allocate(Declaration const* const decl) {
	assert(decl != nullptr);
	assert(m_lookup.count(decl) == 0);
	assert(decl->is_object());

	m_storage.emplace_back(decl->get_type());
	m_lookup.emplace(decl, m_storage.size() - 1);
	return &m_storage.back();
}


Object* const Monotonic_store::locate(Declaration const* const decl) {
	assert(decl != nullptr);
	assert(m_lookup.count(decl) != 0);

	return &m_storage[m_lookup.find(decl)->second];
}


void Monotonic_store::alias(Declaration const* const decl, Object const* obj) {
	assert(decl != nullptr);
	assert(obj != nullptr);
	assert(m_lookup.count(decl) == 0);
	assert(contains_object(m_storage, obj));

	m_lookup.emplace(decl, obj - m_storage.data());
}
