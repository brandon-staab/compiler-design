#include "call_stack/Frame.hpp"

Frame::Frame(Frame* const prev, Declaration const* const func) : m_prev(prev), m_func(func), m_index((prev == nullptr) ? (prev->m_index + 1) : 0) {
	assert(m_func != nullptr);
}


Frame* const Frame::get_caller() const {
	// NOTE: nullptr if root frame

	return m_prev;
}


Declaration const* Frame::get_function() const {
	assert(m_func != nullptr);

	return m_func;
}


std::size_t Frame::get_index() const {
	return m_index;
}


Monotonic_store& Frame::get_locals() {
	return m_locals;
}


Object* const Frame::allocate_local(Declaration const* const decl) {
	assert(decl != nullptr);

	return m_locals.allocate(decl);
}


Object* const Frame::locate_local(Declaration const* const decl) {
	assert(decl != nullptr);
	assert(m_locals.locate(decl) != nullptr);

	return m_locals.locate(decl);
}


void Frame::alias_local(Declaration const* const decl, Object* const obj) {
	assert(decl != nullptr);
	assert(obj != nullptr);

	m_locals.alias(decl, obj);
}
