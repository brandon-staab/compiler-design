#include "call_stack/Call_stack.hpp"


Call_stack::Call_stack() : m_top(), m_frames() {}


std::size_t Call_stack::empty() const {
	return m_frames.empty();
}


std::size_t Call_stack::size() const {
	return m_frames.size();
}


Frame* const Call_stack::push(Declaration const* const func) {
	assert(func != nullptr);

	m_top = new Frame(m_top, func);
	m_frames.push_back(m_top);

	return m_top;
}


void Call_stack::pop() {
	m_top = m_top->get_caller();
	delete m_frames.back();
	m_frames.pop_back();
}


Frame* const Call_stack::get_top() const {
	// NOTE: nullptr if root frame

	return m_top;
}


Frame* const Call_stack::get_frame(std::size_t n) const {
	assert(n < m_frames.size());

	return m_frames[n];
}
