#include "declarations/Declaration.hpp"

#include <iostream>


void dump_debug(std::ostream& os, Expression const* const expr, std::size_t depth);
void print(std::ostream& os, Expression const* const expr, std::size_t depth);
void to_sexpr(std::ostream& os, Expression const* const expr, std::size_t depth);
std::ostream& operator<<(std::ostream& os, Expression const& expr);
std::ostream& operator<<(std::ostream& os, Type const& type);

/*************************************************************/

Declaration::Declaration(Kind const kind) : m_kind(kind) {}


Declaration::Kind Declaration::get_kind() const {
	return m_kind;
}


bool Declaration::is_object() const {
	return is_variable() && get_type()->is_object();
}


bool Declaration::is_variable() const {
	return get_kind() == Kind::variable_decl;
}


bool Declaration::is_reference() const {
	return is_variable() && get_type()->is_reference();
}


bool Declaration::is_function() const {
	return get_kind() == Kind::function_decl;
}

/*************************************************************/

Value_decl::Value_decl(Name const& name, Type const* const type) : m_name(name), m_type(type) {
	assert(name.length() > 0);
	assert(type != nullptr);
}


Name const& Value_decl::get_name() const {
	return m_name;
}


Type const* const Value_decl::get_type() const {
	assert(m_type != nullptr);

	return m_type;
}

/*************************************************************/

std::ostream& operator<<(std::ostream& os, Declaration const& decl) {
	print(os, &decl);
	return os;
}


std::ostream& operator<<(std::ostream& os, Declaration::Kind const kind) {
	return os << kind_to_string(kind);
}

/*****************************/

char const* const kind_to_string(Declaration::Kind const kind) {
	switch (kind) {
		case Declaration::Kind::variable_decl:
			return "variable_decl";
		case Declaration::Kind::function_decl:
			return "function_decl";
	}

	assert(false);	// All declaration kinds must be defined
}
