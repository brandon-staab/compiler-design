#include "declarations/decl_incl.hpp"

#include "expressions/expr_incl.hpp"
#include "statements/stmt_incl.hpp"
#include "types/type_incl.hpp"
#include "utilities/insertIndent.hpp"

#include <iostream>


namespace {
	void print_Variable_decl(std::ostream& os, Variable_decl const* const decl, std::size_t const depth) {
		os << (decl->is_object() ? "var " : "ref ") << decl->get_name() << ':' << *decl->get_type();
		if (decl->has_initializer()) {
			os << " = ";
			print(os, decl->get_initializer(), depth);
		}
	}


	void print_Function_decl(std::ostream& os, Function_decl const* const decl, std::size_t const depth) {
		os << "fun " << decl->get_name() << '(';
		for (auto arg_decl = decl->begin(); arg_decl != decl->end();) {
			print(os, *arg_decl);

			if (++arg_decl != decl->end()) {
				os << ", ";
			}
		}
		os << ") -> ";
		print(os, decl->get_ret_type());
		os << ' ';
		print(os, decl->get_stmt(), depth);
		os << "\n";
	}


	void print_Program_decl(std::ostream& os, Program_decl const* const decl, std::size_t const depth) {
		os << "# program:\n";

		for (auto d : *decl) {
			print(os, d);
		}
	}
}  // namespace


void print(std::ostream& os, Declaration const* const decl, std::size_t depth) {
	assert(decl != nullptr);
	assert(depth < 256);

	// insertIndent(os, depth++);

	switch (decl->get_kind()) {
		case Declaration::Kind::variable_decl:
			return print_Variable_decl(os, static_cast<Variable_decl const* const>(decl), depth);
		case Declaration::Kind::function_decl:
			return print_Function_decl(os, static_cast<Function_decl const* const>(decl), depth);
		case Declaration::Kind::program_decl:
			return print_Program_decl(os, static_cast<Program_decl const* const>(decl), depth);
	}

	assert(false);	// All declarations must be defined
}
