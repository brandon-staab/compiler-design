#include "declarations/Nullary_decl.hpp"

#include "expressions/expr_incl.hpp"


Nullary_decl::Nullary_decl(Kind const kind) : Fixed_Arity_decl(kind) {}

/*************************************************************/

Variable_decl::Variable_decl(Name const& name, Type const* const type) : Nullary_decl(Kind::variable_decl), Value_decl(name, type), m_init() {}


Name const& Variable_decl::get_name() const {
	return Value_decl::get_name();
}


Type const* const Variable_decl::get_type() const {
	assert(Value_decl::get_type() != nullptr);

	return Value_decl::get_type();
}

bool Variable_decl::has_initializer() const {
	return m_init != nullptr;
}


void Variable_decl::set_initializer(Expression* const expr) {
	assert(expr != nullptr);
	assert(expr->get_type() == get_type());

	m_init = expr;
}


Expression* const Variable_decl::get_initializer() {
	assert(m_init != nullptr);

	return m_init;
}


Expression const* const Variable_decl::get_initializer() const {
	assert(m_init != nullptr);

	return m_init;
}
