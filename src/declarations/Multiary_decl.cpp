#include "declarations/Multiary_decl.hpp"

#include <stdexcept>


Multiary_decl::Multiary_decl(Kind const kind, Decl_seq operands) : Declaration(kind), Dynamic_arity_node(operands) {}

/*************************************************************/

Function_decl::Function_decl(Name const& name, Fun_type const* const type) : Multiary_decl(Kind::function_decl, Decl_seq()), Value_decl(name, type), m_stmt() {}


Name const& Function_decl::get_name() const {
	return Value_decl::get_name();
}


Type const* const Function_decl::get_type() const {
	assert(Value_decl::get_type() != nullptr);

	return Value_decl::get_type();
}


Fun_type const* const Function_decl::get_fun_type() const {
	assert(get_type() != nullptr);

	return static_cast<Fun_type const*>(get_type());
}


void Function_decl::set_parameters(Decl_seq& parms) {
	assert(parms.size() <= get_num_parameters());

	for (Declaration* p : parms) {
		assert(p != nullptr);
		add_parameter(p);
	}
}


void Function_decl::add_parameter(Declaration* const decl) {
	assert(decl != nullptr);
	assert(decl->is_variable());
	assert(Multiary_decl::get_arity() <= get_num_parameters());

	get_operands().emplace_back(decl);
}


void Function_decl::set_body(Statement const* const stmt) {
	assert(stmt != nullptr);

	m_stmt = stmt;
}


Statement const* const Function_decl::get_stmt() const {
	assert(m_stmt != nullptr);

	return m_stmt;
}


std::size_t Function_decl::get_num_parameters() const {
	return get_fun_type()->get_num_parameters();
}


Type const* const Function_decl::get_parm_type(std::size_t const i) {
	return get_fun_type()->get_operand(i);
}


Type const* const Function_decl::get_ret_type() const {
	return get_fun_type()->get_ret_type();
}

/*****************************/

Program_decl::Program_decl(Decl_seq& decls) : Multiary_decl(Kind::program_decl, decls) {}


Name const& Program_decl::get_name() const {
	assert(false);
	throw std::logic_error("Program_decl.get_name()");
}


Type const* const Program_decl::get_type() const {
	assert(false);
	throw std::logic_error("Program_decl.get_type()");
}
