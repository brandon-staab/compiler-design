#include "declarations/decl_incl.hpp"

#include "expressions/expr_incl.hpp"
#include "statements/stmt_incl.hpp"
#include "types/type_incl.hpp"
#include "utilities/insertIndent.hpp"

#include <iostream>


namespace {
	void dump_debug_Variable_decl(std::ostream& os, Variable_decl const* const decl, std::size_t const depth) {
		os << (decl->is_object() ? "var " : "ref ") << decl->get_name() << ':';
		dump_debug(os, decl->get_type(), depth);

		if (decl->has_initializer()) {
			os << " = ";
			dump_debug(os, decl->get_initializer(), depth);
		}
	}


	void dump_debug_Function_decl(std::ostream& os, Function_decl const* const decl, std::size_t const depth) {
		os << "fun " << decl->get_name() << "(";
		for (auto arg_decl = decl->begin(); arg_decl != decl->end(); ++arg_decl) {
			dump_debug(os, *arg_decl, depth);
		}
		os << ") -> ";
		dump_debug(os, decl->get_ret_type(), depth);
		os << " {\n";
		dump_debug(os, decl->get_stmt(), depth);
		os << "}";
	}


	void dump_debug_Program_decl(std::ostream& os, Program_decl const* const decl, std::size_t const depth) {
		os << "program:";

		for (auto d : *decl) {
			dump_debug(os, d);
		}
	}
}  // namespace


void dump_debug(std::ostream& os, Declaration const* const decl, std::size_t depth) {
	assert(decl != nullptr);
	assert(depth < 256);

	os << '\n' << &decl;
	insertIndent(os, ++depth);

	switch (decl->get_kind()) {
		case Declaration::Kind::variable_decl:
			return dump_debug_Variable_decl(os, static_cast<Variable_decl const* const>(decl), depth);
		case Declaration::Kind::function_decl:
			return dump_debug_Function_decl(os, static_cast<Function_decl const* const>(decl), depth);
		case Declaration::Kind::program_decl:
			return dump_debug_Program_decl(os, static_cast<Program_decl const* const>(decl), depth);
	}

	assert(false);	// All declarations must be defined
}
