#include "declarations/decl_incl.hpp"

#include "expressions/expr_incl.hpp"
#include "statements/stmt_incl.hpp"
#include "types/type_incl.hpp"

#include <iostream>


namespace {
	void to_sexpr_Variable_decl(std::ostream& os, Variable_decl const* const decl) {
		os << '(' << (decl->is_object() ? "var " : "ref ") << decl->get_name() << ' ';
		to_sexpr(os, decl->get_type());

		if (decl->has_initializer()) {
			os << ' ';
			to_sexpr(os, decl->get_initializer());
		}
		os << ')';
	}

	void to_sexpr_Function_decl(std::ostream& os, Function_decl const* const decl) {
		os << "(fun " << decl->get_name() << '(';
		for (auto arg_decl = decl->begin(); arg_decl != decl->end();) {
			to_sexpr(os, *arg_decl);

			if (++arg_decl != decl->end()) {
				os << ' ';
			}
		}

		os << ')';
		to_sexpr(os, decl->get_ret_type());
		os << ' ';
		to_sexpr(os, decl->get_stmt());
		os << ')';
	}

	void to_sexpr_Program_decl(std::ostream& os, Program_decl const* const decl) {
		os << "(program ";

		for (auto d : *decl) {
			to_sexpr(os, d);
		}
		os << ')';
	}
}  // namespace


void to_sexpr(std::ostream& os, Declaration const* const decl) {
	assert(decl != nullptr);

	switch (decl->get_kind()) {
		case Declaration::Kind::variable_decl:
			return to_sexpr_Variable_decl(os, static_cast<Variable_decl const* const>(decl));
		case Declaration::Kind::function_decl:
			return to_sexpr_Function_decl(os, static_cast<Function_decl const* const>(decl));
		case Declaration::Kind::program_decl:
			return to_sexpr_Program_decl(os, static_cast<Program_decl const* const>(decl));
	}

	assert(false);	// All declarations must be defined
}
