# FIXME: pass by ref
fun fib(x:int, calls:ref int) -> int {
	calls++;

	if (x <= 1) return 1;
	return x * fib(--x, calls);
}


fun abc(a:int, b:int, c:int) -> int {
	return a + b + c;
}


fun main(argc:int) -> int {
	var calls:int = 0;
	var f:int = fib(42, calls);
	var a:int = abc(calls, f, argc);

	return a;
}
