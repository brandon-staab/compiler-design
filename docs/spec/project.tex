
% Document settings
\documentclass[11pt]{article}
\usepackage{amsmath}
\usepackage{stmaryrd}
\usepackage{bm}
\usepackage[margin=1in]{geometry}
\usepackage[pdftex]{graphicx}
\usepackage{color}
\usepackage{listings}
\usepackage{hyperref}
\usepackage{multirow}
\usepackage{multicol}
\usepackage{setspace}
\usepackage{xspace}
\usepackage{listings}

\pagestyle{plain}
\setlength\parindent{0pt}
\setlength\parskip{4pt}
\setlength{\columnseprule}{0.2pt}

% Define the speculative C++1z language.
\lstdefinelanguage{C++1z}{alsolanguage=C++,
                          escapechar=@,
                          breakatwhitespace=true,
                          breaklines=true
                          morekeywords={alignof, 
                                        decltype, 
                                        concept, 
                                        requires}}

% Default formatting for listings.
\lstset{language=C++1z,
        basicstyle=\ttfamily,
        keywordstyle=\bfseries,
        stringstyle=,
        xleftmargin=1em,
        showstringspaces=false,
        commentstyle=\rmfamily\itshape,
        columns=flexible,
        keepspaces=true,
        texcl=true}

\newcommand{\code}[1]{\lstinline @#1@}
\lstnewenvironment{codeblock}{\lstset{language=C++1z}}{}

\newcommand{\mathsc}[1]{\bm{\mathsf{#1}}}
% \newcommand{\mathsc}[1]{\mathsf{#1}}

% Types
\newcommand{\type}[1]{\ensuremath{t_{#1}}\xspace}
\newcommand{\typebool}{\ensuremath{\mathsc{bool}}\xspace}
\newcommand{\typeint}{\ensuremath{\mathsc{int}}\xspace}
\newcommand{\typefloat}{\ensuremath{\mathsc{float}}\xspace}
\newcommand{\typeref}[1]{\ensuremath{\mathsc{ref}~#1}\xspace}
\newcommand{\typefn}[2]{\ensuremath{(#1)\rightarrow #2}\xspace}

% Expressions
\newcommand{\expr}[1]{\ensuremath{e_{#1}}\xspace}
\newcommand{\exprtrue}{\ensuremath{\mathsc{true}}\xspace}
\newcommand{\exprfalse}{\ensuremath{\mathsc{false}}\xspace}
\newcommand{\exprand}[2]{\ensuremath{#1 \mathbin{\mathsc{and}} #2}\xspace}
\newcommand{\expror}[2]{\ensuremath{#1 \mathbin{\mathsc{or}} #2}\xspace}
\newcommand{\exprnot}[1]{\ensuremath{\mathsc{not}\;#1}\xspace}
\newcommand{\exprcond}[3]{\ensuremath{#1 \mathbin{?} #2 : #3}\xspace}
\newcommand{\expreq}[2]{\ensuremath{#1 = #2}\xspace}
\newcommand{\exprne}[2]{\ensuremath{#1 \neq #2}\xspace}
\newcommand{\exprlt}[2]{\ensuremath{#1 < #2}\xspace}
\newcommand{\exprgt}[2]{\ensuremath{#1 > #2}\xspace}
\newcommand{\exprle}[2]{\ensuremath{#1 \leq #2}\xspace}
\newcommand{\exprge}[2]{\ensuremath{#1 \geq #2}\xspace}
\newcommand{\expradd}[2]{\ensuremath{#1 + #2}\xspace}
\newcommand{\exprsub}[2]{\ensuremath{#1 - #2}\xspace}
\newcommand{\exprmul}[2]{\ensuremath{#1 * #2}\xspace}
\newcommand{\exprdiv}[2]{\ensuremath{#1 \mathbin{\mathsc{div}} #2}\xspace}
\newcommand{\exprrem}[2]{\ensuremath{#1 \mathbin{\mathsc{rem}} #2}\xspace}
\newcommand{\exprneg}[1]{\ensuremath{-#1}\xspace}
\newcommand{\exprrec}[1]{\ensuremath{/#1}\xspace}
\newcommand{\exprass}[2]{\ensuremath{#1 \leftarrow #2}\xspace}
\newcommand{\exprcall}[2]{\ensuremath{#1(#2)}\xspace}
\newcommand{\convval}[1]{\ensuremath{\mathsc{val}~#1}\xspace}

% Statements
\newcommand{\stmt}[1]{\ensuremath{s_{#1}}\xspace}
\newcommand{\stmtbreak}{\ensuremath{\mathsc{break}}\xspace}
\newcommand{\stmtcont}{\ensuremath{\mathsc{continue}}\xspace}
\newcommand{\stmtblock}[1]{\ensuremath{\{#1\}}\xspace}
\newcommand{\stmtwhile}[2]{\ensuremath{%
  \mathsc{while}~#1~\mathsc{do}~#2%
  }\xspace}
\newcommand{\stmtif}[3]{\ensuremath{%
  \mathsc{if}~#1~\mathsc{then}~#2~\mathsc{else}~#3%
  }\xspace}
\newcommand{\stmtret}[1]{\ensuremath{%
  \mathsc{return}~#1%
  }\xspace}
\newcommand{\stmtexpr}[1]{\ensuremath{#1}\xspace}
\newcommand{\stmtdecl}[1]{\ensuremath{#1}\xspace}

\newcommand{\decl}[1]{\ensuremath{d_{#1}}\xspace}
\newcommand{\decllet}[4]{\ensuremath{%
  \mathsc{#1}~#2\mathbin{:}#3\mathbin{=}#4%
  }\xspace}
\newcommand{\declvar}[3]{\ensuremath{\decllet{var}{#1}{#2}{#3}}\xspace}
\newcommand{\declref}[3]{\ensuremath{\decllet{ref}{#1}{#2}{#3}}\xspace}
\newcommand{\declfn}[4]{\ensuremath{%
  \mathsc{fun}~#1(#2) \rightarrow #3~#4%
  }\xspace}

\newcommand{\terminal}[1]{\text{\code{#1}}}
\newcommand{\nonterminal}[1]{\text{\textit{#1}}}
\newcommand{\alt}[0]{\mathbin{|}}
\newcommand{\opt}{\mathrm{?}}

\title{MiniC Language Specification}

\begin{document}
\maketitle

This document describes the abstract and concrete syntax and semantics of a small C-like programming language (albeit with a very different syntax for declarations).

\section{Basics}

The fundamental storage unit is the \emph{byte}, which is a sequence of 8 bits.
The storage (or memory) available to a program consists of one or more sequences of contiguous bytes. 
Every byte has a unique address.

The following paragraphs describe the entities of the language. An \emph{entity} is an abstract element of a program that is described by, created by, or
referred to in a program.

A \emph{value} is element of a set.
For example the value zero is an element in the set of integers and the set of real numbers.
Similarly, the value true is an element in the set boolean values; the other value is false.
A value can also be an address.
When this document refers to specific values, they are spelled in English using the normal font.

A \emph{type} describes objects, references, functions, and expressions.
The meaning of the type is determined by the the entity it describes.

An \emph{object} is region of storage, comprised of a fixed length sequence
of bytes.
Note that every object has an address.
Every object has a type, which determines the size of the object and how its stored bits can be interpreted as a value (i.e., an object stores a value).
Objects are created by definitions or when a temporary object is created.

A \emph{reference} can be thought of as an alias (name) for an object or alternatively as an address for the object.
Every reference has a type, which determines the set of objects to which the reference can bind.
It is unspecified whether a reference requires storage.
References are created by definitions.

A \emph{variable} is the definition of an object or reference.

A \emph{function} maps a sequence of input values, called \emph{arguments},
to an output value, called its \emph{return value}.
Every function has a type, which determines the arguments that it can accept as inputs the type of value that it returns as an output.
Functions are created by definitions.

\section{Abstract syntax and semantics}

This section describes the \emph{abstract syntax} of the language, which is comprised of four sub-languages: types ($t$), expressions ($e$), statements ($s$), and definitions $(d)$.
Each sub-language is defined (recursively) as a set of strings belonging to that set.
Note that these are ``strings'' in the abstract sense; they will be represented as abstract syntax trees.

The meaning of each string is determined by the semantic specifications in this document. 
The semantic specification determines several properties of the language:
\begin{itemize}
\item the \emph{static semantics} of the language define properties of
strings that can be used at compile-time (e.g., the type of expressions).
\item the \emph{dynamic semantics} of the language define properties of
strings that are meaningful at runtime (e.g., the value of expression).
\end{itemize}
Static semantics are often used as an additional ``filter'' on syntactically 
valid strings that would result in misbehaving programs.
Dynamic semantics are used to define when the execution of a program is
defined.
For example, the expression \code{1 / n} is valid if and only if \code{n} refers 
to an object of type \code{int} (statically). 
The evaluation of that expression is defined if the value of that object is 
never 0 (dynamically).

A \emph{program} is a sequence of definitions (substrings of $d$); that is,
it is also a string.


\subsection{Types}

Types describe objects, references and functions.

\[
\begin{array}{lcll}
t &::=& \typebool  & \text{boolean values} \\
  &   & \typeint   & \text{integer values} \\
  &   & \typefloat & \text{floating point values} \\
  &   & \typeref{t_1}   & \text{references to objects} \\
  &   & \typefn{t_1, t_2, \ldots, t_n}{t_r}
                   & \text{functions}
\end{array}
\]

The type \typebool describes the values true and false.

The type \typeint describes integer values in the left-open range $[-2^{32-1}, 2^{32-1})$.

The type \typefloat describes single precision IEEE 754 floating point values.

The reference types \typeref{t} describes references to objects. 
These value can be represented as the address of the object in memory. 

The set of function types \typefn{t_1, t_2, \ldots, t_n}{t_r} describes
functions taking $n$ arguments, whose corresponding types are $t_1$, $t_2$, $\ldots$, and $t_n$, and returning a value of type $t_r$.
These values can be represented as the address of the function in memory.

The types \typebool, \typeint, and \typefloat are collectively called the 
\emph{object types}, meaning they can be used to define objects.

The types \typeint and \typefloat are called the \emph{arithmetic types}.

\subsection{Expressions}

The language has the following kinds of expressions:

\begin{multicols}{2}
\[
\begin{array}{lcll}
e &::=& \exprtrue & \\
  &   & \exprfalse & \\
  &   & n & \text{integer literals}\\
  &   & r & \text{floating literals}\\
  &   & x & \text{identifiers}\\
  &   & \exprand{e_1}{e_2} & \text{logical and}\\
  &   & \expror{e_1}{e_2} &\text{logical or}\\
  &   & \exprnot{e_1} & \text{logical negation}\\
  &   & \exprcond{e_1}{e_2}{e_3} & \text{conditional}\\
  &   & \expreq{e_1}{e_2} & \text{equal to}\\
  &   & \exprne{e_1}{e_2} & \text{not equal to}\\
  &   & \exprlt{e_1}{e_2} & \text{less than}\\
  &   & \exprgt{e_1}{e_2} & \text{greater than}\\
  % &   & \ldots\\
\end{array}
\]
\vfill
\columnbreak
\[
\begin{array}{lll}
        % \ldots\\
        \exprle{e_1}{e_2} & \text{less or equal}\\
        \exprge{e_1}{e_2} & \text{greater or equal}\\
        \expradd{e_1}{e_2} & \text{addition}\\
        \exprsub{e_1}{e_2} & \text{subtraction}\\
        \exprmul{e_1}{e_2} & \text{multiplication}\\
        \exprdiv{e_1}{e_2} & \text{quotient of division}\\
        \exprrem{e_1}{e_2} & \text{remainder of division}\\
        \exprneg{e_1} & \text{negation}\\
        \exprrec{e_1} & \text{reciprocal}\\
        \exprass{e_1}{e_2} & \text{assignment}\\
        \exprcall{e_f}{e_1, e_2, \ldots, e_n} & \text{function call}\\
        \convval{e_1} & \text{conversion to value}
\end{array}
\]
\end{multicols}

An expression is a sequence of operands and operators that specifies a value
computation. The evaluation of an expression results in a value. The type of an 
expression determines a) how expressions can be combined to produce complex 
computations and b) the kind of value it produces. The following paragraphs define
the requirements on operands and the result types of each expression as well 
the values they produce.

Some operators require conversions of their operands. A \emph{value-conversion}
\convval{\expr{1}} converts an expression of type \typeref{t} to a value of 
type $t$.

The order in which an expression's operands are evaluated is unspecified unless 
otherwise noted.

The expressions \exprtrue and \exprfalse have type \typebool and the
values true and false, respectively.

Integer literals have type \typeint. The value of an integer literal is the
one indicated by its spelling.

Floating point literals have type \typefloat. The vale of a floating point
literal is the one indicated by its spelling.

After value-conversion, the operands of \exprand{\expr{1}}{\expr{2}} and 
\expror{\expr{1}}{\expr{2}} shall have type \typebool. The result these expressions 
is \typebool.
The value of \exprand{\expr{1}}{\expr{2}} is true if both operands are true and
false otherwise. \expr{1} is evaluated before \expr{2}, and \expr{2} is not evaluated
if \expr{1} is false.
The value of \expror{\expr{1}}{\expr{2}} is true if either operand is true, or both
are true, and false otherwise. \expr{1} is evaluated before \expr{2}, and 
\expr{2} is not evaluated if \expr{1} is true.

After value-conversion, the operand of \exprnot{\expr{1}} shall have type 
\typebool. The type of the expression is \typebool. 
The value of the expression is true when the \expr{1} is false and false otherwise.

In the expression \exprcond{e_1}{e_2}{e_3}, the type of \expr{1} shall have
type \typebool after value conversion. The type requirements for \expr{2} and
\expr{3} are as follows: If both \expr{1} and \expr{2} have reference 
type, they shall have the same type. Otherwise, \expr{1} and \expr{2} shall 
have the same type after value-conversion.
The type of the expression is that of \expr{1} and \expr{2}.
If the value of \expr{1} is true, then the value of the expression is that of
\expr{2}. Otherwise, it is that of \expr{3}. Only one of \expr{2} and \expr{3}
is evaluated.

After value-conversion, the operands of the expressions 
\expreq{\expr{1}}{\expr{2}} and 
\exprne{\expr{1}}{\expr{2}}
shall have the same type. 
The type of the expression is \typebool.
The value of \expreq{\expr{1}}{\expr{2}} is true if \expr{1} and \expr{2} are 
equal and false otherwise.
The value of \exprne{\expr{1}}{\expr{2}} is true if \expr{1} and \expr{2} are 
different and false otherwise.

After value-conversion, the operands of the expressions 
\exprlt{\expr{1}}{\expr{2}}, 
\exprgt{\expr{1}}{\expr{2}}, 
\exprle{\expr{1}}{\expr{2}}, and 
\exprge{\expr{1}}{\expr{2}} 
shall have the same arithmetic type. 
The result type is \typebool.
The value of \exprlt{\expr{1}}{\expr{2}} is true if \expr{1} is less than 
\expr{2} and false otherwise.
The value of \exprgt{\expr{1}}{\expr{2}} is true if \expr{1} is greater 
than \expr{2} and false otherwise.
The value of \exprle{\expr{1}}{\expr{2}} is true if \expr{1} is less than 
or equal to \expr{2} and false otherwise.
The value of \exprge{\expr{1}}{\expr{2}} is true if \expr{1} is greater 
than or equal to \expr{2} and false otherwise.

After value-conversion, the operands of the expressions 
\expradd{\expr{1}}{\expr{2}} and 
\exprsub{\expr{1}}{\expr{2}}
shall have the same arithmetic type.
The type of the expression is that of the converted operands.
The value of \expradd{\expr{1}}{\expr{2}} is the sum of the operands. 
For values of type \typeint, if the sum is greater than the maximum value of 
\typeint, the result is undefined.
The value of \exprsub{\expr{1}}{\expr{2}} is the difference resulting from the 
subtraction of the \expr{2} from \expr{1}. 
For values of type \typeint, if the difference is less than 
the minimum value of \typeint, the result is undefined.

After value-conversion, the operands of the expressions 
\exprmul{\expr{1}}{\expr{2}}, 
\exprdiv{\expr{1}}{\expr{2}}, and 
\exprrem{\expr{1}}{\expr{2}} 
shall have the same arithmetic type. 
The type of the expression is that of the converted operands.
The value of \exprmul{\expr{1}}{\expr{2}} is the product of the operands. 
For values of type \typeint, iff the product is greater than the maximum value 
of \typeint, the result is undefined.
The values of \exprdiv{\expr{1}}{\expr{2}} and \exprrem{\expr{1}}{\expr{2}} are 
the quotient and remainder of dividing \expr{1} by \expr{2}, respectively.
For values of type \typeint, if \expr{2} is zero, the result is undefined. 
Additionally, uf \expr{2} is the minimum value of \typeint,
the result is undefined. For division, the fractional part of the value is
discarded (the value is truncated towards zero). 
If the expression \exprdiv{a}{b} is defined,
\expradd{\exprmul{(\exprdiv{a}{b})}{b}}{\exprrem{a}{b}} is equal to $a$.

After value-conversion, the operand of the expression \exprneg{\expr{1}}
shall have arithmetic type.
The type of the expression is that of the converted operand.
The value of the expression is equivalent to subtracting the value of \expr{1}
from zero.

After value-conversion, the operand of the expression \exprrec{\expr{1}}
shall have arithmetic type.
The type of the expression is that of the converted operand.
The value of the expression is equivalent to dividing one by the value of 
\expr{1}. 
For operands of type \typeint, if the alue of \expr{1} is zero, the result is 
undefined.

In the expression \exprcall{\expr{0}}{\expr{1}, \expr{2}, \ldots, \expr{n}},
\expr{0} shall have function type 
\typefn{\type{1}, \type{2}, \ldots, \type{n}}{\type{0}} after value 
conversion. 
Each argument \expr{i} is converted as needed to copy initialize a variable of its
corresponding parameter type \type{j}. (Note: This can also include reference 
binding). The type of the expression is \type{0}.
When a function is called, each parameter is initialized by its corresponding
argument. 
The value of the expression is that of the operand of the return statement
in the called function.


In the expression \exprass{\expr{1}}{\expr{2}}, the type of \expr{1} shall
be \typeref{t}. After value conversion, the type of \expr{2} shall be $t$.
The type of the expression is \typeref{t}.
The effect of the expression is to set the value of the object referred to
be \expr{1} to that of \expr{2}.
The value of the expression is that of \expr{1}.


\subsection{Statements}

The language has the following kinds of statements:

\[
\begin{array}{lcll}
s &::=& \stmtblock{\stmt{1}, \stmt{2}, \ldots, \stmt{n}} & \text{block statements} \\
  &   & \stmtif{e}{\stmt{1}}{\stmt{2}} & \text{if statements} \\
  &   & \stmtwhile{e}{\stmt{1}}        & \text{while statements} \\
  &   & \stmtbreak      & \text{break statements} \\
  &   & \stmtcont       & \text{continue statement} \\ 
  &   & \stmtret{e}     & \text{return statements} \\
  &   & \stmtexpr{e}    & \text{expressions} \\
  &   & \stmtdecl{d}    & \text{local definitions} \\
\end{array}
\]

\subsection{Declarations}

A statement is executed for its side effects.

The statement \stmtblock{\stmt{1}, \stmt{2}, \ldots, \stmt{n}} is a sequence
of statements executed in lexical order.

In the statement \stmtif{e}{\stmt{1}}{\stmt{2}}, $e$ shall have
type \typebool after value conversion. 
If the value of $e$ is true, only \stmt{1} is executed. Otherwise,
only \stmt{2} is executed.

In the statement \stmtwhile{e}{\stmt{1}}, the $e$ shall have
type \typebool after value conversion. 
The statement \stmt{1} is executed repeatedly until the value of $e$ is
false. This test occurs before each execution of \stmt{1}.

When the \stmtbreak statement is executed, control is passed to the
statement following the innermost while loop, if any.

When the \stmtcont statement is executed, control is passed to the 
test of the condition of the innermost while loop.

In the statement \stmtret{e}, the return value is converted as needed to
copy initialize a variable whose type is that of the enclosing function.
When executed, the return object is initialized by the converted value
and control is passed to the calling function.
Memory allocated for local variables is released after returning control
to the caller.

The language has the following kinds of declarations:

\[
\begin{array}{lcll}
d &::=& \declvar{x}{t}{e}       & \text{object definitions} \\
  &   & \declref{x}{t}{e}       & \text{reference definitions} \\
  &   & \declfn{x}{d_1, d_2, \ldots, d_n}{t}{s}
                                & \text{function definitions} \\
\end{array}
\]

A definition \declvar{x}{t}{e} defines an object whose type is $t$, which can 
be accessed using the name $x$. $t$ shall not be a reference type. 
The expression $e$ is converted as needed to copy initialize the object.

A definition \declref{x}{t}{e} defines a reference whose type is $t$, which can 
be accessed using the name $x$. $t$ shall not be a reference type.
The reference is reference initialized by $e$.

A definition \declfn{x}{\decl{1}, \decl{2}, \ldots, \decl{n}}{t}{s} defines a
function. The type of the function is formed from the parameters and
return type and is \typefn{\type{1}, \type{2}, \ldots, \type{n}}{t}.
The statement $s$ defines the body of the function.


\subsection{Initialization}

Copy initialization is performed when initializing variables, during function
calls and returning values. Note that in each case, there is a variable
being initialized (parameters and return values are variables).
Copy initialization is a procedure that initializes a variable $v$ with
declared type $t$ from an expression $e$.

If $v$ declares a reference, \emph{reference initialization} is performed. 
Otherwise, \emph{object initialization} is performed.

For object initialization, $e$ shall have type $t$ after
value conversion. The converted value is stored in the object designated by
$v$.

For reference initialization, $e$ shall have type \typeref{t}, where $t$ is
the declared type of the reference. The reference is bound to the object
designated by $e$. 


\section{Lexical conventions}

The basic character set is comprised of the whitespace characters
space, (horizontal) tab, and newline, and the following graphical
characters:

\begin{codeblock}
a b c d e f g h i j k l m n o p q r s t u v w x y z
A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
0 1 2 3 4 5 6 7 8 9
_ { } [ ] ( ) < > # % : ; ? * + - / ^ & | ~ ! = , \
\end{codeblock}

A source file is translated in the following stages
\begin{enumerate}
\item The source file is decomposed into tokens (see below) and sequences
of whitespace characters. Comments are replaced by a single space. 
(Note: Removing comments during lexical analysis is equivalent to replacing
them by a single character.)
\item Whitespace characters separating tokens are no longer significant.
The resulting tokens are syntactically and semantically analyzed and
translated as a translation unit.
\item Separate translation units are combined, and all external entity names
being resolved. The output 
\end{enumerate}

A \emph{comment} has the following form:
\[
\begin{array}{lll}
\nonterminal{comment} &\rightarrow& \terminal{#} ~ \nonterminal{comment-character}*\\
\nonterminal{comment-character} &\rightarrow&\ \text{any basic character except newline}\\
\end{array}
\]
Each comment is terminated by a newline or the end-of-input. The newline (if 
present) is not part of the comment.

There are several kinds of tokens:
\[
\begin{array}{lcl}
\nonterminal{token} &\rightarrow& 
      \nonterminal{identifier}  \\
  & \alt & \nonterminal{keyword}  \\
  & \alt & \nonterminal{operator}  \\
  & \alt & \nonterminal{punctuator}  \\
  & \alt & \nonterminal{literal} 
\end{array}
\]
Tokens are produced during the first phase of translation.

An \emph{identifier} denotes a user-defined entity (variable or function).
\[
\begin{array}{lll}
\nonterminal{digit} &\rightarrow& [\terminal{0-9}]\\
\nonterminal{nondigit} &\rightarrow& [\terminal{a-zA-Z_}]\\
\nonterminal{identifier} &\rightarrow& 
    \nonterminal{nondigit} ~ (\nonterminal{nondigit} \alt \nonterminal{digit})*
\end{array}
\]
An identifier is a token that matches has many spellings.

The following identifiers are reserved as keywords.

\begin{center}
\begin{tabular}{ll}
\terminal{bool}     & \terminal{let} \\
\terminal{break}    & \terminal{not} \\
\terminal{continue} & \terminal{or} \\
\terminal{fun}      & \terminal{ref} \\
\terminal{else}     & \terminal{return} \\
\terminal{false}    & \terminal{true} \\
\terminal{float}    & \terminal{var} \\
\terminal{if}       & \terminal{while} \\
\terminal{int}      & \\
\end{tabular}
\end{center}

Each keyword corresponds to a distinct token.

A punctuator is a symbol that marks of divides a term in order to make the
meaning more clear. 
\[
\begin{array}{lcl}
\nonterminal{punctuator} &\rightarrow& 
  \terminal{\{} \alt \terminal{\}} \alt \terminal{(} \alt \terminal{)} \alt
    \terminal{,} \alt \terminal{;} \alt \terminal{:} \alt \terminal{->} \\
\end{array}
\]
Each punctuator corresponds to a distinct token.

An operator is a symbol that denotes the application of an operation. There
are several kinds of operators.
\[
\begin{array}{lcl}
\nonterminal{operator} &\rightarrow& 
           \nonterminal{relational-operator}  \\
  & \alt & \nonterminal{arithmetic-operator}  \\
  & \alt & \nonterminal{logical-operator}  \\
  & \alt & \nonterminal{conditional-operator}  \\
  & \alt & \nonterminal{assignment-operator} 
\end{array}
\]
Specific kinds of operators are:
\[
\begin{array}{lll}
\nonterminal{relational-operator} &\rightarrow& 
  \terminal{==} \alt
  \terminal{!=} \alt
  \terminal{<} \alt
  \terminal{>} \alt
  \terminal{<=} \alt
  \terminal{>=} \\
\nonterminal{arithmetic-operator} &\rightarrow& 
  \terminal{+} \alt
  \terminal{-} \alt
  \terminal{*} \alt
  \terminal{/} \alt
  \terminal{\%} \\
\nonterminal{logical-operator} &\rightarrow& 
  \terminal{and} \alt
  \terminal{or} \alt
  \terminal{not}\\
\nonterminal{conditional-operator} &\rightarrow& \terminal{?} \\
\nonterminal{assignment-operator} &\rightarrow& \terminal{=} \\
\end{array}
\]
Each operator corresponds to a distinct token. The \emph{logical-operator}s
are also keywords with distinct meaning.

A \emph{literal} denotes a fixed value. The literals are:
\[
\begin{array}{lcl}
\nonterminal{literal} &\rightarrow& 
           \nonterminal{boolean-literal}  \\
  & \alt & \nonterminal{integer-literal}  \\
  & \alt & \nonterminal{float-literal}  \\
\end{array}
\]

The boolean literals are:
\[
\begin{array}{lll}
\nonterminal{boolean-literal} &\rightarrow& \terminal{true} \alt \terminal{false}
\end{array}
\]
The boolean literals are also keywords, but have specialized meaning.

There are integer literals are:
\[
\begin{array}{lcl}
\nonterminal{integer-literal} &\rightarrow& 
           \nonterminal{decimal-integer-literal}  \\
  & \alt & \nonterminal{hexadecimal-integer-literal}  \\
  & \alt & \nonterminal{binary-integer-literal}  \\
\nonterminal{decimal-integer-literal} &\rightarrow& 
    \nonterminal{digit}+\\
\nonterminal{hexadecimal-digit} &\rightarrow& 
    [\terminal{0-9a-fA-F}]\\
\nonterminal{hexadecimal-integer-literal} &\rightarrow& 
    (\terminal{0} ~ (\terminal{x} \alt \terminal{X}) ~ 
      \nonterminal{hexadecimal-digit}+) \\
\nonterminal{binary-digit} &\rightarrow& 
    [\terminal{01}]\\
\nonterminal{binary-integer-literal} &\rightarrow& 
    (\terminal{0} ~[\terminal{bB}] ~ \nonterminal{binary-digit}+) \\
\end{array}
\]
The patterns 
$\nonterminal{decimal-integer-literal}$,
$\nonterminal{hexadecimal-integer-literal}$, and
$\nonterminal{binary-integer-literal}$
correspond to distinct tokens.

The floating point literal is:
\[
\begin{array}{lll}
\nonterminal{float-literal} &\rightarrow&
    \nonterminal{fraction} ~ \nonterminal{exponent}?\\
\nonterminal{fraction} &\rightarrow&
    \nonterminal{digit}+ ~ \terminal{.} ~ \nonterminal{digit}+\\
\nonterminal{exponent} &\rightarrow& [\terminal{eE}] ~ [\terminal{+-}]? ~ \nonterminal{digit}+
\end{array}
\]

\end{document}



