
\section{Concrete syntax}

This section defines the concrete syntax of the language and the correspondence
between strings of tokens and abstract syntax trees. For a concrete
expression \code{e} to correspond to an abstract expression $e$, all 
subexpressions of \code{e} must correspond to abstract subexpressions of
$e$, all identifiers in \code{e} must be introduced by declarations, and 
$e$ is well-typed. For example, the expression \code{3 + 4} corresponds to
\expradd{3}{4} because \code{3} corresponds to $3$, \code{4} corresponds to
$4$, and the addition of integers is well-typed according to the rules above.

\subsection{Scope and lookup}

The \emph{scope of a name} is the (non-contiguous) region of source
text where the name can be used to denote an entity. The scope of a name
begins at its point of declaration and ends at a fixed point determined by
the of text in which the declaration appears.

The point of declaration of a function is immediately after its return type
specifier.

The point of declaration of an object is immediately after its return type.

A name declared within a \emph{block-statement} has \emph{block scope};
its scope ends with the closing brace of the statement.

A name declared within a function \emph{parameter-list} has \emph{parameter scope};
its scope ends with the closing brace of the function definition.

A name declared in the outermost region of source text (i.e., outside of a
\emph{block-statement} or \emph{parameter-list}) has \emph{global scope}; its
scope ends at the end of the source file.

When an \emph{identifier} is used in an expression, it must be looked up to
determine which entity it refers to. That entity is found by searching outwards
from the current scope until a declaration of the identifier is found.
If no such declaration is found, the program is ill-formed.

\subsection{Expressions}

A \emph{primary-expression} is a literal (token), identifier (token), or grouped
subexpression.
\[
\begin{array}{lll}
\nonterminal{primary-expression} 
  &\rightarrow& \nonterminal{literal}\\
  &~~\alt& \nonterminal{identifier}\\
  &~~\alt& \terminal{(} ~ \nonterminal{expression} ~ \terminal{)}
\end{array}
\]
A literal corresponds to an abstract expression of similar kind. That is:
\begin{itemize}
  \item \emph{boolean-literal}s correspond to \exprtrue and \exprfalse.
  \item \emph{integer-literal}s correspond to the integers $n$.
  \item \emph{flat-literal}s correspond to the floating pointers $r$.
\end{itemize}

An \emph{identifier} \code{N} corresponds to an abstract identifier $x$ only 
if the \code{N} is visible in the scope in which it appears.

A \emph{postfix-expression} is an expression whose operator occurs after it's
operand. Additional operands may be specified as part of the operator (e.g.,
function call).
\[
\begin{array}{lll}
\nonterminal{postfix-expression} 
  &\rightarrow& 
    \nonterminal{postfix-expression} ~ 
    \terminal{(} ~ \nonterminal{argument-list}\opt ~ \terminal{)}\\
  &~~\alt&
    \nonterminal{primary-expression}
\end{array}
\]
\[
\begin{array}{lll}
\nonterminal{argument-list} 
  &\rightarrow& 
    \nonterminal{argument-list} ~ \terminal{,} ~ argument\\
  &~~\alt&
    \nonterminal{argument}
\end{array}
\]
\[
\begin{array}{lll}
\nonterminal{argument} 
  &\rightarrow& 
    \nonterminal{expression}
\end{array}
\]

An \emph{postfix-expression} of the form \code{e0(e1, e2, ..., en)} is a
\emph{function call} and corresponds to an abstract call expression of
the form \exprcall{e_0}{e_1, e_2, \ldots, e_n}.

A \emph{unary-expression} is an operator followed by an operand.
\[
\begin{array}{lll}
\nonterminal{unary-expression} 
  &\rightarrow& 
    \terminal{-} ~ \nonterminal{unary-expression}\\
  &~~\alt& 
    \terminal{/} ~ \nonterminal{unary-expression}\\
  &~~\alt& 
    \terminal{!} ~ \nonterminal{unary-expression}\\
  \end{array}
\]
A \emph{unary-expression} of the form \code{-e} corresponds \exprneg{e}.
A \emph{unary-expression} of the form \code{/e} corresponds \exprrec{e}.
A \emph{unary-expression} of the form \code{!e} corresponds \exprnot{e}.

A \emph{multiplicative-expression} denotes multiplication or the quotient
or remainder of division. 
\[
\begin{array}{lll}
\nonterminal{multiplicative-expression} 
  &\rightarrow& 
    \nonterminal{multiplicative-expression} ~ \terminal{*} ~ \nonterminal{unary-expression}\\
  &~~\alt& 
    \nonterminal{multiplicative-expression} ~ \terminal{/} ~ \nonterminal{unary-expression}\\
  &~~\alt& 
    \nonterminal{multiplicative-expression} ~ \terminal{\%} ~ \nonterminal{unary-expression}\\
  &~~\alt& 
    \nonterminal{unary-expression}\\
  \end{array}
\]
A \emph{multiplicative-expression} of the form \code{e1 * e2} corresponds \exprmul{e_1}{e_2}.
A \emph{multiplicative-expression} of the form \code{e1 / e2} corresponds \exprdiv{e_1}{e_2}.
A \emph{multiplicative-expression} of the form \code{e1 \% e2} corresponds \exprrem{e_1}{e_2}.

An \emph{additive-expression} denotes addition or subtraction.
\[
\begin{array}{lll}
\nonterminal{additive-expression} 
  &\rightarrow& 
    \nonterminal{additive-expression} ~ \terminal{+} ~ \nonterminal{multiplicative-expression}\\
  &~~\alt& 
    \nonterminal{additive-expression} ~ \terminal{-} ~ \nonterminal{multiplicative-expression}\\
  &~~\alt& 
    \nonterminal{multiplicative-expression}
  \end{array}
\]
An \emph{additive-expression} of the form \code{e1 + e2} corresponds \expradd{e_1}{e_2}.
An \emph{additive-expression} of the form \code{e1 - e2} corresponds \exprsub{e_1}{e_2}.

A \emph{relational-expression} denotes a comparison of two values to determine
respective operands.
\[
\begin{array}{lll}
\nonterminal{relational-expression} 
  &\rightarrow& 
    \nonterminal{relational-expression} ~ \terminal{<} ~ \nonterminal{additive-expression}\\
  &~~\alt& 
    \nonterminal{relational-expression} ~ \terminal{>} ~ \nonterminal{additive-expression}\\
  &~~\alt& 
    \nonterminal{relational-expression} ~ \terminal{<=} ~ \nonterminal{additive-expression}\\
  &~~\alt& 
    \nonterminal{relational-expression} ~ \terminal{>=} ~ \nonterminal{additive-expression}\\
  &~~\alt& 
    \nonterminal{additive-expression}
\end{array}
\]
A \emph{relational-expression} of the form \code{e1 < e2} corresponds \exprlt{e_1}{e_2}.
A \emph{relational-expression} of the form \code{e1 > e2} corresponds \exprgt{e_1}{e_2}.
A \emph{relational-expression} of the form \code{e1 <= e2} corresponds \exprle{e_1}{e_2}.
A \emph{relational-expression} of the form \code{e1 >= e2} corresponds \exprge{e_1}{e_2}.

An \emph{equality-expression} denotes a comparison of two values to determine
sameness or difference of operands.
\[
\begin{array}{lll}
\nonterminal{equality-expression} 
  &\rightarrow& 
    \nonterminal{equality-expression} ~ \terminal{==} ~ \nonterminal{relational-expression}\\
  &~~\alt& 
    \nonterminal{equality-expression} ~ \terminal{!=} ~ \nonterminal{relational-expression}\\
  &~~\alt& 
    \nonterminal{relational-expression}
\end{array}
\]
An \emph{equality-expression} of the form \code{e1 == e2} corresponds to \expreq{e_1}{e_2}.
An \emph{equality-expression} of the form \code{e1 != e2} corresponds to \exprne{e_1}{e_2}.

An \emph{and-expression} represents the logical and of two boolean 
operands.
\[
\begin{array}{lll}
\nonterminal{and-expression} 
  &\rightarrow& 
    \nonterminal{and-expression} ~ \terminal{and} ~ \nonterminal{equality-expression}\\
  &~~\alt& 
    \nonterminal{equality-expression}
\end{array}
\]
An \emph{and-expression} of the form \code{e1 && e2} corresponds to the abstract
expression \exprand{e_1}{e_2}.

An \emph{or-expression} represents the inclusive logical or of two boolean 
operands.
\[
\begin{array}{lll}
\nonterminal{or-expression} 
  &\rightarrow& 
    \nonterminal{or-expression} ~ \terminal{or} ~ \nonterminal{and-expression}\\
  &~~\alt& 
    \nonterminal{and-expression}
\end{array}
\]
An \emph{and-expression} of the form \code{e1 || e2} corresponds to \expror{e_1}{e_2}.

A \emph{conditional-expression} selectively evaluates one of its operands based
on the truth value of its first.
\[
\begin{array}{lll}
\nonterminal{conditional-expression} 
  &\rightarrow& 
    \nonterminal{logical-or-expression} ~ 
      \terminal{?} ~ \nonterminal{expression} ~ 
      \terminal{:} ~ \nonterminal{conditional-expression} \\
  &~~\alt& 
    \nonterminal{logical-or-expression}
\end{array}
\]
A \emph{conditional-expression} of the form \code{e1 ? e2 : e3} corresponds to 
\exprcond{e_1}{e_2}{e_3}.


% TODO: Discuss this parse in class.
% a ? b ? c : d : e ? f : g
% a ? (b ? c : d) : (e ? f : g)

An \emph{assignment-expression} denotes the storage of a computed value in an
object.
\[
\begin{array}{lll}
\nonterminal{assignment-expression} 
  &\rightarrow& 
    \nonterminal{conditional-expression} ~ \terminal{=} ~ \nonterminal{assignment-expression}\\
  &~~\alt& 
    \nonterminal{conditional-expression}
\end{array}
\]
An \emph{assignment-expression} of the form \code{e1 = e2} corresponds to 
\exprass{e_1}{e_2}.

An \emph{expression} is a sequence of operators and operands that denotes
a value computation.
\[
\begin{array}{lll}
\nonterminal{expression} 
  &\rightarrow& 
    \nonterminal{assignment-expression}\\
\end{array}
\]

\subsection{Statements}

A \emph{statement} can be executed and has some effect (storing a value
in an object, transferring the flow of control, etc.).
\[
\begin{array}{lll}
\nonterminal{statement} 
  &\rightarrow& 
    \nonterminal{block-statement}\\
  &~~\alt& 
    \nonterminal{if-statement}\\
  &~~\alt& 
    \nonterminal{while-statement}\\
  &~~\alt& 
    \nonterminal{break-statement}\\
  &~~\alt& 
    \nonterminal{continue-statement}\\
  &~~\alt& 
    \nonterminal{return-statement}\\
  &~~\alt& 
    \nonterminal{declaration-statement}\\
  &~~\alt& 
    \nonterminal{expression-statement}\\
  &~~\alt& 
    \nonterminal{empty-statement}\\
\end{array}
\]

A \emph{block-statement} allows a sequence of statements to be used wherever one is
expected.
\[
\begin{array}{lll}
\nonterminal{block-statement} 
  &\rightarrow& 
    \terminal{\{} ~ \nonterminal{statement-seq}\opt ~ \terminal{\}}\\
\end{array}
\]

\[
\begin{array}{lll}
\nonterminal{statement-seq} 
  &\rightarrow& \nonterminal{statement-seq} ~ \nonterminal{statement}\\
  &~~\alt& \nonterminal{statement}\\
\end{array}
\]
A \emph{block-statement} \code{\{s1 s2 ... sn\}} corresponds to \stmtblock{s_1, s_2, \ldots, s_n}.
\
An \emph{if-statement} can be used to selectively execute a statement based on
the value of a boolean expression.
\[
\begin{array}{lll}
\nonterminal{if-statement} 
  % &\rightarrow& 
  %   \terminal{if} ~ \terminal{(} ~ \nonterminal{expression} ~ \terminal{)} ~
  %     \nonterminal{statement}\\
  &\rightarrow& 
    \terminal{if} ~ \terminal{(} ~ \nonterminal{expression} ~ \terminal{)} ~
      \nonterminal{statement} ~
      \terminal{else} ~ \nonterminal{statement}\\
\end{array}
\]
An \emph{if-statement} \code{if (e) s1 else s2} corresponds to \stmtif{e_1}{s_1}{s_2}.

A \emph{while-statement} defines a loop, which allows repeated execution of a 
statement. 
\[
\begin{array}{lll}
\nonterminal{while-statement} 
  &\rightarrow& 
    \terminal{while} ~ \terminal{(} ~ \nonterminal{expression} ~ \terminal{)} ~
      \nonterminal{statement}\\
\end{array}
\]
A \emph{while-statement} \code{while (e) s} corresponds to \stmtwhile{e_1}{s_1}.

A \emph{break-statement} transfers control to the statement following the
innermost \emph{while-statement}.
\[
\begin{array}{lll}
\nonterminal{break-statement} 
  &\rightarrow& \terminal{break} ~ \terminal{;}\\
\end{array}
\]
A \emph{break-statement} \code{break;} corresponds to \stmtbreak.

A \emph{continue-statement} transfers control to the first statement in the body
of the innermost \emph{while-statement}.
\[
\begin{array}{lll}
\nonterminal{continue-statement} 
  &\rightarrow& \terminal{continue} ~ \terminal{;}\\
\end{array}
\]
A \emph{continue-statement} \code{break;} corresponds to \stmtcont.

A \emph{return-statement} returns control to the calling function.
\[
\begin{array}{lll}
\nonterminal{return-statement} 
  &\rightarrow& \terminal{return} ~ \nonterminal{expression} ~ \terminal{;}\\
  % &~~\alt& \terminal{return} ~ \terminal{;}
\end{array}
\]
A \emph{return-statement} \code{return e;} corresponds to \stmtret{e_1}.

A \emph{declaration-statement} introduces a declaration. In the case where the
declaration is a variable or constant, initializes the object.
\[
\begin{array}{lll}
\nonterminal{declaration-statement} 
  &\rightarrow& \nonterminal{local-declaration}\\
\end{array}
\]
A \emph{declaration-statement} \code{d} corresponds to \stmtdecl{d}.

An \emph{expression-statement} denotes a computation whose value is discarded.
This is typically used for assignment.
\[
\begin{array}{lll}
\nonterminal{expression-statement} 
  &\rightarrow& \nonterminal{expression} ~ \terminal{;} \\
\end{array}
\]
An \emph{expression-statement} \code{e;} corresponds to \stmtexpr{e}.

A \emph{empty-statement} has no effect.
\[
\begin{array}{lll}
\nonterminal{empty-statement} 
  &\rightarrow& \terminal{;}\\
\end{array}
\]
An \emph{empty-statement} \code{;} corresponds to \stmtskip.

\subsection{Declarations}

A \emph{program} is defined by a sequence of top-level declarations.
\[
\begin{array}{lll}
\nonterminal{program} 
  &\rightarrow& \nonterminal{declaration-seq}\opt\\
\end{array}
\]
\[
\begin{array}{lll}
\nonterminal{declaration-seq} 
  &\rightarrow& \nonterminal{declaration-seq} ~ \nonterminal{declaration}\\
  &~~\alt& \nonterminal{declaration}\\
\end{array}
\]

A \emph{declaration} specifies how a name is to be interpreted (by the compiler) 
and used (by a programmer). In general, a declaration introduces an entity
(variable or function) and associates it with a name.
\[
\begin{array}{lll}
\nonterminal{declaration} 
  &\rightarrow& \nonterminal{function-definition}\\
  &~~\alt& \nonterminal{variable-definition}\\
\end{array}
\]

A \emph{local-declaration} is the subset of declarations that can appear only
in local scope.
\[
\begin{array}{lll}
\nonterminal{local-declaration} 
  &\rightarrow& \nonterminal{variable-definition}
\end{array}
\]

An \emph{variable-definition} associates a name with a value, and possibly
storage.
\[
\begin{array}{lll}
\nonterminal{variable-definition} 
  &\rightarrow& \nonterminal{object-definition}\\
  &~~\alt& \nonterminal{reference-definition}\\
\end{array}
\]

A \emph{variable-definition} defines an object whose value can be modified.
\[
\begin{array}{lll}
\nonterminal{variable-definition} 
  &\rightarrow& \terminal{var} ~ \nonterminal{identifier} ~ 
    \terminal{:} \nonterminal{type-specifier} ~ \nonterminal{;}\\
  &~~\alt& \terminal{var} ~ \nonterminal{identifier} ~ 
    \terminal{:} ~ \nonterminal{type-specifier} ~
    \terminal{=} ~ \nonterminal{expression} ~ \nonterminal{;}
\end{array}
\]
A \emph{variable-definition} of the form \code{var x : t = e} corresponds
to \declvar{x}{t}{e}.
A \emph{variable-definition} of the form \code{var x : t;} corresponds
to \declvar{x}{t}{\text{\emph{zero}}}. Here, \emph{zero} is the value zero
with type $t$.


A \emph{reference-definition} defines an alias to an existing object.
\[
\begin{array}{lll}
\nonterminal{reference-definition} 
  &\rightarrow& \terminal{let} ~ \nonterminal{identifier} ~ 
    \terminal{:} ~ \nonterminal{type-specifier} ~
    \terminal{=} ~ \nonterminal{expression} ~ \nonterminal{;}
\end{array}
\]
A \emph{reference-definition} of the form \code{var x : t = e} corresponds
to \declref{x}{t}{e}.


A \emph{function-definition} defines a function as a sequence of statements.
\[
\begin{array}{lll}
\nonterminal{function-definition} 
  &\rightarrow& \terminal{fun} ~ \nonterminal{identifier} ~ 
    \terminal{(} ~ \nonterminal{parameter-list}\opt ~ \terminal{)} ~
    \terminal{->} ~ \nonterminal{type-specifier} ~
    \nonterminal{block-statement}
\end{array}
\]
\[
\begin{array}{lll}
\nonterminal{parameter-list} 
  &\rightarrow& \nonterminal{parameter-list} ~ \terminal{,} ~ \nonterminal{parameter}\\
  &~~\alt& \nonterminal{parameter}
\end{array}
\]
\[
\begin{array}{lll}
\nonterminal{parameter} 
  &\rightarrow& \nonterminal{identifier} ~ \terminal{:} ~ \nonterminal{type-specifier}
\end{array}
\]
A \emph{function-definition} of the form \code{fun f(p1, p2, ..., pn) -> t s}
corresponds to \declfn{f}{p_1, p_2, \ldots, p_n}{t}{s}.
